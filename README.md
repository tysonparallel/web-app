# Web App

### Test Accounts

#### Select a user from the list below

- demo@getparallel.com
- test+write@getparallel.com
- test+underbudget@getparallel.com
- test+growth@getparallel.com
- test+emptystate@getparallel.com

### End-to-end Testing (E2E)

We utilize E2E tests to maintain application quality. New features are expected to have corresponding E2E
tests and existing E2E are expected to pass with any new deploy. See a more detailed look into E2E testing [here](./tests/README.md).

### Questions & Patterns

zIndex levels
