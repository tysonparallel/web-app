# Use the official Node.js 20 image as the base image
FROM node:20

# Create and set the working directory
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install project dependencies
RUN npm install

# Copy the rest of the application files to the container
COPY . .

# Build the React application (adjust this as needed)
RUN npm run build

# Expose the port your application runs on
EXPOSE 8000

# Define the command to start your application (adjust this as needed)
CMD ["npm", "start"]