import { Page } from "@playwright/test";

export default async (page: Page, email?: string, userRole?: string) => {
  await page.goto(`/auth/login?method=standard`);

  const emailInput = await page.getByTestId("email-input");
  const passwordInput = await page.getByTestId("password-input");

  await emailInput.fill(email || "test+emptystate@getparallel.com");
  await passwordInput.fill("Password123!");

  await page.getByRole("button", { name: "Log In" }).click();

  if (userRole === "user") {
    await page.waitForURL(`/dashboard`);
    return;
  }
  await page.waitForURL(`/onboarding`);
};
