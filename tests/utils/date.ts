import dayjs from "dayjs";
// eslint-disable-next-line import/extensions
import utc from "dayjs/plugin/utc.js";
// eslint-disable-next-line import/extensions
import isSameOrAfter from "dayjs/plugin/isSameOrAfter.js";

dayjs.extend(isSameOrAfter);
dayjs.extend(utc);

export default (date?: string) => {
  if (date) return dayjs.utc(date);
  const VITE_MOCK_DATE = process.env.VITE_MOCK_DATE;
  if (VITE_MOCK_DATE) return dayjs.utc(VITE_MOCK_DATE);
  return dayjs().utc();
};
