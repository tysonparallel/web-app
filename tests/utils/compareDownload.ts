import { expect, Page } from "@playwright/test";
import fs from "fs";

export default async (page: Page, testId: string, expectedContents: string) => {
  const downloadPromise = page.waitForEvent("download");
  await page.getByTestId(testId).click();
  const download = await downloadPromise;
  const path = await download.path();
  const fileBuffer = fs.readFileSync(path);
  const contents = fileBuffer
    .toString()
    .replace(/^\uFEFF/, "")
    .replace(/\\/g, "")
    .replace(/"/g, "")
    .replace(/\s+/g, "")
    .trim(); // trim contents and remove all whitespaces and line breaks so strings are consistent from csv

  const trimmedContents = expectedContents
    .replace(/^\uFEFF/, "")
    .replace(/\\/g, "")
    .replace(/"/g, "")
    .replace(/\s+/g, "")
    .trim(); // do the same to expected contents so string can be on more than one line

  expect(contents).toContain(trimmedContents);
};
