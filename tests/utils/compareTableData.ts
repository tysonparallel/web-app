import { Locator, expect } from "@playwright/test";

export default async (rows: Locator, expectedData: string[][]) => {
  const rowCount = await rows.count();
  expect(rowCount).toBe(expectedData.length);
  for (let i = 0; i < rowCount; i++) {
    const row = rows.nth(i);
    const tds = row.locator("td");
    const columnCount = await tds.count();
    for (let j = 0; j < columnCount; j++) {
      const td = tds.nth(j);
      const text = await td.textContent();
      expect(text, `Expected data at index (${i},${j}) does not match`).toBe(
        expectedData[i][j],
      );
    }
  }
};
