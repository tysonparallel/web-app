import { Page } from "@playwright/test";
import { expect } from "@playwright/test";

const monthNumberToText = (monthNumber: number) => {
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  return months[monthNumber - 1];
};

export default async ({
  page,
  id,
  date,
}: {
  page: Page;
  id?: string;
  date?: string;
}) => {
  if (!date) throw new Error("No date provided");

  const [month, day, year] = date.split("/").map(Number);
  const monthText = monthNumberToText(month);
  const nextMonth = monthNumberToText((month % 12) + 1);
  const currentYear = new Date().getFullYear();
  const datePickerWrapper = `div[data-testid="${id}-datepicker-wrapper"]`;
  const selectedMonthButton = await page.locator(
    `${datePickerWrapper} >> div >> div >> div:nth-child(2) >> div >> div >> div >> div >> div:nth-child(2) >> div:nth-child(1) >> button`,
  );
  const selectedYearButton = await page.locator(
    `${datePickerWrapper} >> div >> div >> div:nth-child(2) >> div >> div >> div >> div >> div:nth-child(2) >> div:nth-child(2) >> button`,
  );

  const isYearPriorToCurrentYear = year < currentYear;

  const yearToCheckForVisibility = isYearPriorToCurrentYear
    ? year - 1
    : year + 1;

  const dateSelectorButton = await page.getByTestId(`${id}-datepicker-wrapper`);
  await dateSelectorButton.click();

  await selectedMonthButton.click();
  const monthToClick = page.locator(
    `${datePickerWrapper} >> button:text-is("${monthText}")`,
  );
  const nextMonthToClick = page.locator(
    `${datePickerWrapper} >> button:text-is("${nextMonth}")`,
  );
  await monthToClick.first().click();
  await expect(nextMonthToClick).not.toBeVisible();

  await selectedYearButton.click();
  if (isYearPriorToCurrentYear) {
    const previousYearsButton = await page.locator(
      `${datePickerWrapper} >> div >> div >> div:nth-child(2) >> div >> div >> div >> div:nth-child(1) >> div:nth-child(1) >> button`,
    );
    await previousYearsButton.first().click();
  }

  const yearToClick = page.locator(
    `${datePickerWrapper} >> button:text-is("${year}")`,
  );
  await yearToClick.first().click();
  await expect(
    page.locator(
      `${datePickerWrapper} >> button:text-is("${yearToCheckForVisibility}")`,
    ),
  ).not.toBeVisible();
  await expect(yearToClick).toBeVisible();

  await page
    .locator(`${datePickerWrapper} >> button:text-is("${day}")`)
    .first()
    .click();
};
