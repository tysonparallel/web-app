import { Page, expect } from "@playwright/test";

type BudgetCardFieldValuesToAssert = {
  total: string;
  planTitle: string;
  planValue: string;
  actualValue: string;
  varianceValue: string;
  budgetTag: string;
};

export default async (page: Page, values: BudgetCardFieldValuesToAssert) => {
  const { total, planTitle, planValue, actualValue, varianceValue, budgetTag } =
    values;

  await expect(page.getByTestId("budget-card-tag")).toContainText(budgetTag);

  await expect(page.getByTestId("budget-card-total")).toContainText(total);
  await expect(page.getByTestId("budget-card-plan-title")).toContainText(
    planTitle,
  );
  await expect(page.getByTestId("budget-card-plan-value")).toContainText(
    planValue,
  );
  await expect(page.getByTestId("budget-card-actual-title")).toContainText(
    "Current Working Model",
  );
  await expect(page.getByTestId("budget-card-actual-value")).toContainText(
    actualValue,
  );
  await expect(page.getByTestId("budget-card-variance-title")).toContainText(
    "Variance",
  );
  await expect(page.getByTestId("budget-card-variance-value")).toContainText(
    varianceValue,
  );
};
