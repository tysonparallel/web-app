# E2E Testing Practices

# Introduction

Welcome to our engineering team's guide on End-to-End (E2E) testing.

## Goals of E2E Testing

The primary aim is to maintain a high level of quality in the application prior to deploys through regression testing, as well as leveraging writing new tests that will become regression tests moving forward.

Our tests check many possible states of a feature to make sure it behaves as expected with the goal of catching bugs before they reach production.

## Getting Started

To run E2E tests, both the monolith API and web-app should be up and running.

### Seeding the Database

The database needs to be prepared in order for all tests to pass. If a test ends up writing to the database, you will need to create a corresponding seed file/user for it.

**Important**: Execute the following command on the API repo before running the test suite:

```
npm run db:seed
```

This action will purge the data from the database, and reseed the database. This action is independent of migrations.

### Running Tests

Playwright is leveraged to run E2E tests. A thorough documentation of running the test suite can be found [here](https://playwright.dev/docs/running-tests). A simple command for getting the whole suite running is simply:

```
npx playwright test
```

## Utility Functions

### Login Utility

At the start of every function, run the login utility and pass in the email associated with the seed used for the test.

### Compare Table Data Function

To compare table data, follow these steps:

1. Select the table you want to compare and assign it to a variable.
2. Run .locator("tbody tr"); on the table variable and assign the result to a rows variable.
3. Create an expected version of the table and assign it to a variable. This will be an array of arrays, where each sub-array represents a row in the table.

## Fixtures

If your test requires the use of files or other assets, place them in the `fixtures` folder under a relevant subdirectory. File names within `fixtures` should follow camel case naming conventions.

### Assigning a File from Fixtures to a Variable

To use a file from the `fixtures` folder in your test, you can assign it to a variable like this:

```javascript
const filePath = path.join(
  process.cwd(),
  "/tests/fixtures/specificTestFiles/fileForTest.csv",
);
```

This way, the file at the given path can be easily accessed in your tests.

## Best Practices

### Running in parallel

Despite the line of code in each file that says `test.describe.configure({ mode: "parallel" });`, it is a playwright feature to run tests in unison and not because our company name is "Parallel". This is for maximum speed when trying to run all tests simultaneously.

When writing tests, think of them as unit tests where the intent is for one test to not impact another. If this occurs, it's best practice to create a new seed user as to avoid conflict and make the test more independent.

## A Deeper Dive Into Seeds

When using seeds, opt for dynamically generated UUIDs rather than hard-coded ones. This approach makes our tests more adaptable and dependable.

## Reusable Components and Data-testid

For reusable components, the ID property is linked to a `data-testid` attribute which we use as the locator in our E2E tests to target specific elements.

To target a specific element in your tests, append a `data-testid="your-test-id"` attribute to that element.

### Writing Test Files

Each E2E test file can contain multiple test cases, offering you the flexibility to evaluate different aspects of a single feature.

**Note**: If multiple test cases in a file modify the database, evaluate if they can be combined. If they remain separate, each test case will require its own seed.

## Tests To Add

- [ ] Unassigned employee that has been a task for long enough that they have a termination date. When generating or assigning a position, it should automatically set the position as inactive and trigger a backfill task
- [ ] Deactivate position - check calculations on deactivating a position with 1 forecasted change
- [ ] Deactivate position - check calculations on deactivating a position with 2 forecasted changes
- [ ] Forecasted term - check calculations on a backfill
- [ ] Forecasted term - check calculations without a backfill
- [ ] Forecasted term - check calculations on undoing a term forecast
- [ ] Delete forecast - delete first of two forecasts
- [ ] Delete forecast - delete last of two forecasts
- [ ] Delete forecast - delete all forecasts
