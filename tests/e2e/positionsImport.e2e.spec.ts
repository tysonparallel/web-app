import { expect, test } from "@playwright/test";
import path from "path";
import login from "../utils/login";
import compareTableData from "../utils/compareTableData";

test.describe.configure({ mode: "parallel" });

test("Create and view a bulk positions import", async ({ page }) => {
  await login(page, "test+importnomap@getparallel.com");
  await page.goto(`/positions/imports`);

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "File Name",
    "Uploaded",
    "Created By",
    "Positions Created",
  ]);

  const emptyState = page.getByTestId("empty-state");

  await expect(emptyState).toHaveText(
    "No imports foundUpload CSV files to create positions.",
  );

  const importButton = page.getByTestId("import-positions-button");

  await importButton.click();

  await expect(page.getByTestId("select-file-state-modal")).toBeVisible();

  const fileDropZone = page.getByTestId("positions-import-file-dropzone");

  await expect(fileDropZone).toHaveText(
    " Drag and drop or choose a file to upload your Positions.csv file compatible",
  );

  const filePathWithErrors = path.join(
    process.cwd(),
    "/tests/fixtures/importForecastedPositionsFiles/headersErrors.csv",
  );

  await page
    .getByTestId("positions-import-file-input")
    .setInputFiles(filePathWithErrors);

  await expect(fileDropZone).toHaveText("File Ready: headersErrors.csvRemove");

  const uploadButton = page.getByTestId("upload-positions-import-button");

  await uploadButton.click();

  await expect(page.getByTestId("columns-with-errors-count")).toHaveText(
    "1 columns with errors",
  );

  const errorTableHeaders = await page.$$eval(
    '[data-testid="import-errors-table"] th',
    (elements) => elements.map((el) => el.textContent),
  );

  expect(errorTableHeaders).toEqual(["Column Name", "Preview", "Details"]);

  const errorTable = page.getByTestId("import-errors-table");
  const errorRows = errorTable.locator("tbody tr");

  const expectedData = [["Employment Type", "FULL_TIM", ""]];

  await compareTableData(errorRows, expectedData);

  const informationPopoverButton = await page.locator(
    '[data-testid="import-errors-table"] svg',
  );

  await informationPopoverButton.click();

  await expect(page.getByTestId("import-error-popover")).toBeVisible();

  await expect(page.getByTestId("import-error-popover")).toHaveText(
    "Invalid Employment Type values1 value errorsThis column contains values that aren't able to be imported",
  );

  await informationPopoverButton.click();

  const reUploadButton = page.getByTestId("reupload-positions-import-button");

  await reUploadButton.click();

  await expect(page.getByTestId("select-file-state-modal")).toBeVisible();

  await expect(fileDropZone).toHaveText(
    " Drag and drop or choose a file to upload your Positions.csv file compatible",
  );

  const filePathNoErrors = path.join(
    process.cwd(),
    "/tests/fixtures/importForecastedPositionsFiles/headersNoErrors.csv",
  );

  await page
    .getByTestId("positions-import-file-input")
    .setInputFiles(filePathNoErrors);

  await expect(fileDropZone).toHaveText(
    "File Ready: headersNoErrors.csvRemove",
  );

  await uploadButton.click();

  await expect(page.getByTestId("success-state-modal")).toBeVisible();

  const successModalText = page.getByTestId("success-positions-created-text");

  await expect(successModalText).toHaveText(
    "3 roles were successfully imported.",
  );

  const successCloseButton = page.getByTestId("success-close-button");

  await successCloseButton.click();

  await page.waitForSelector('[data-testid="table-header-column"]');

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "File Name",
    "Uploaded",
    "Created By",
    "Positions Created",
  ]);

  const finalTable = page.locator("table");
  const finalRows = finalTable.locator("tbody tr");

  await expect(page.getByText("headersNoErrors.csv")).toBeVisible();

  const expectedFinalData = [
    ["headersNoErrors.csv", "09/05/2023", "Ryan Pierce", "3"],
  ];

  await compareTableData(finalRows, expectedFinalData);
});

test("Create and view a bulk positions import with mapping", async ({
  page,
}) => {
  await login(page, "test+importmap@getparallel.com");

  await page.goto(`/positions/imports`);

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "File Name",
    "Uploaded",
    "Created By",
    "Positions Created",
  ]);

  const emptyState = page.getByTestId("empty-state");

  await expect(emptyState).toHaveText(
    "No imports foundUpload CSV files to create positions.",
  );

  const importButton = page.getByTestId("import-positions-button");

  await importButton.click();

  await expect(page.getByTestId("select-file-state-modal")).toBeVisible();

  const fileDropZone = page.getByTestId("positions-import-file-dropzone");

  await expect(fileDropZone).toHaveText(
    " Drag and drop or choose a file to upload your Positions.csv file compatible",
  );

  const filePathWithErrors = path.join(
    process.cwd(),
    "/tests/fixtures/importForecastedPositionsFiles/missingHeadersErrors.csv",
  );

  await page
    .getByTestId("positions-import-file-input")
    .setInputFiles(filePathWithErrors);

  await expect(fileDropZone).toHaveText(
    "File Ready: missingHeadersErrors.csvRemove",
  );

  const uploadButton = page.getByTestId("upload-positions-import-button");

  await uploadButton.click();

  await expect(page.getByTestId("file-mapping-state-modal")).toBeVisible();

  const mappingTableHeaders = await page.$$eval(
    '[data-testid="import-mapping-table"] th',
    (elements) => elements.map((el) => el.textContent),
  );

  expect(mappingTableHeaders).toEqual(["Column Name", "Preview", "Tag"]);

  const mappingTable = page.getByTestId("import-mapping-table");
  const mappingRowsWithErrors = mappingTable.locator("tbody tr");

  const expectedMappingDataWithErrors = [
    [
      "title",
      "Salesmansoftware internsoftware engineer",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "managerName",
      "Emma LeeBenjamin Anderson",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "groupName",
      "FinanceEngineeringEngineering",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "effectiveAt",
      "09/09/202309/09/202309/09/2023",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "employment",
      "FULL_TIMEINTERNFULL_TIME",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "compensationRate",
      "352540",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "paymentUnit",
      "HOURLYHOURLYSALARY",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "expectedWeeklyHours",
      "4020",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
  ];

  await compareTableData(mappingRowsWithErrors, expectedMappingDataWithErrors);

  await page.getByTestId("title-tag").selectOption("title");
  await page.getByTestId("managerName-tag").selectOption("title");

  const validateMappingButton = page.getByTestId("validate-mapping-button");

  await validateMappingButton.click();

  await expect(page.getByTestId("groupName-tag-error")).toHaveText(
    "Needs property",
  );
  await expect(page.getByTestId("effectiveAt-tag-error")).toHaveText(
    "Needs property",
  );
  await expect(page.getByTestId("employment-tag-error")).toHaveText(
    "Needs property",
  );
  await expect(page.getByTestId("compensationRate-tag-error")).toHaveText(
    "Needs property",
  );
  await expect(page.getByTestId("paymentUnit-tag-error")).toHaveText(
    "Needs property",
  );
  await expect(page.getByTestId("expectedWeeklyHours-tag-error")).toHaveText(
    "Needs property",
  );

  await expect(page.getByTestId("import-mapping-errors")).toHaveText(
    "Errors:You have assigned a header more than once.You missed assigning a mandatory header.",
  );

  await page.getByTestId("title-tag").selectOption("title");
  await page.getByTestId("managerName-tag").selectOption("managerName");
  await page.getByTestId("groupName-tag").selectOption("groupName");
  await page.getByTestId("effectiveAt-tag").selectOption("effectiveAt");
  await page.getByTestId("employment-tag").selectOption("employmentType");
  await page
    .getByTestId("compensationRate-tag")
    .selectOption("compensationRate");
  await page.getByTestId("paymentUnit-tag").selectOption("paymentUnit");
  await page
    .getByTestId("expectedWeeklyHours-tag")
    .selectOption("expectedWeeklyHours");

  await validateMappingButton.click();

  await expect(page.getByTestId("columns-with-errors-count")).toHaveText(
    "1 columns with errors",
  );

  const errorTableHeaders = await page.$$eval(
    '[data-testid="import-errors-table"] th',
    (elements) => elements.map((el) => el.textContent),
  );

  expect(errorTableHeaders).toEqual(["Column Name", "Preview", "Details"]);

  const errorTable = page.getByTestId("import-errors-table");
  const errorRows = errorTable.locator("tbody tr");

  const expectedErrorTableData = [["Expected Weekly Hours", "undefined20", ""]];

  await compareTableData(errorRows, expectedErrorTableData);

  const informationPopoverButton = await page.locator(
    '[data-testid="import-errors-table"] svg',
  );

  await informationPopoverButton.click();

  await expect(page.getByTestId("import-error-popover")).toBeVisible();

  await expect(page.getByTestId("import-error-popover")).toHaveText(
    "Invalid Expected Weekly Hours values2 value errorsThis column contains values that aren't able to be imported",
  );

  await informationPopoverButton.click();

  const reUploadButton = page.getByTestId("reupload-positions-import-button");

  await reUploadButton.click();

  await expect(page.getByTestId("select-file-state-modal")).toBeVisible();

  await expect(fileDropZone).toHaveText(
    " Drag and drop or choose a file to upload your Positions.csv file compatible",
  );

  const filePathNoErrors = path.join(
    process.cwd(),
    "/tests/fixtures/importForecastedPositionsFiles/missingHeadersNoErrors.csv",
  );

  await page
    .getByTestId("positions-import-file-input")
    .setInputFiles(filePathNoErrors);

  await expect(fileDropZone).toHaveText(
    "File Ready: missingHeadersNoErrors.csvRemove",
  );

  await uploadButton.click();

  const mappingRowsNoErrors = mappingTable.locator("tbody tr");

  const expectedMappingDataNoErrors = [
    [
      "title",
      "Salesmansoftware internsoftware engineer",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "managerName",
      "Emma LeeBenjamin Anderson",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "groupName",
      "FinanceEngineeringEngineering",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "effectiveAt",
      "09/09/202309/09/202309/09/2023",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "employment",
      "FULL_TIMEINTERNFULL_TIME",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "compensationRate",
      "352540",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "paymentUnit",
      "HOURLYHOURLYSALARY",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
    [
      "expectedWeeklyHours",
      "4035",
      "PropertyTitleManager NameGroup NameEffective AtEmployment TypeCompensation RatePayment UnitExpected Weekly Hours",
    ],
  ];

  await compareTableData(mappingRowsNoErrors, expectedMappingDataNoErrors);

  await validateMappingButton.click();

  await expect(page.getByTestId("success-state-modal")).toBeVisible();

  const successModalText = page.getByTestId("success-positions-created-text");

  await expect(successModalText).toHaveText(
    "3 roles were successfully imported.",
  );

  const successCloseButton = page.getByTestId("success-close-button");

  await successCloseButton.click();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "File Name",
    "Uploaded",
    "Created By",
    "Positions Created",
  ]);

  const finalTable = page.locator("table");
  const finalRows = finalTable.locator("tbody tr");

  await expect(page.getByText("missingHeadersNoErrors.csv")).toBeVisible();

  const expectedFinalData = [
    ["missingHeadersNoErrors.csv", "09/05/2023", "Ryan Pierce", "3"],
  ];

  await compareTableData(finalRows, expectedFinalData);
});
