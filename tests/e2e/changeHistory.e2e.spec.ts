import { expect, test } from "@playwright/test";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc.js";
import login from "../utils/login";
import compareTableData from "../utils/compareTableData";

dayjs.extend(utc);
test.describe.configure({ mode: "parallel" });

test("Change History test", async ({ page }) => {
  await login(page, "test+changehistory@getparallel.com");

  await page.goto(`/dashboard`);

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "Employee",
    "Position",
    "Department",
    "Effective",
    "Modification",
    "Original",
    "Updated",
    "",
  ]);

  await expect(page.getByText("Freddy Krueger")).toBeVisible();
  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    [
      "Freddy Krueger",
      "DevOps Emperor",
      "Engineering",
      "08/05/2023",
      "6 modifications",
      "-",
      "-",
      "",
    ],
    [
      "Compensation Rate$175,000.00$85.00Employment TypeFull TimePart TimeExpected Weekly Hours-20ManagerEmma Lee-Payment UnitSalaryHourlyTitleDevOps KingDevOps Emperor",
    ],
    [
      "Michael Myers",
      "Halfstaff Engineer",
      "Engineering",
      "07/05/2023",
      "2 modifications",
      "-",
      "-",
      "",
    ],
    [
      "Compensation Rate$150,000.00$185,000.00TitleQuarterstaff EngineerHalfstaff Engineer",
    ],
    [
      "Jason Voorhees",
      "Software Engineer",
      "Engineering",
      "05/01/2023",
      "Compensation Rate",
      "$125,000.00",
      "$135,000.00",
      "",
    ],
    [
      "-",
      "Senior Engineer",
      "Engineering",
      "04/01/2023",
      "2 modifications",
      "-",
      "-",
      "",
    ],
    ["EmployeeJon Richards-StatusFilledClosed"],
  ];

  await compareTableData(rows, expectedData);

  await page
    .getByTestId(new RegExp("table-expand-change-history"))
    .first()
    .click();

  await expect(
    page.getByTestId("change-history-change-list").first(),
  ).toHaveText(
    `Compensation Rate$175,000.00$85.00Employment TypeFull TimePart TimeExpected Weekly Hours-20ManagerEmma Lee-Payment UnitSalaryHourlyTitleDevOps KingDevOps Emperor`,
  );
});
