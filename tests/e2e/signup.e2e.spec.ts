// import { expect, tests } from "@playwright/tests";
//
// tests.describe.configure({ mode: "parallel" });
//
//
// tests("redirect to login on base url visit", async ({ page }) => {
//   await page.goto(`/`);
//   await expect(page).toHaveURL(/.*login/);
//
//   await page.getByTestId("sign-up").click();
//   await expect(page).toHaveURL(/.*sign-up/);
// });
//
// tests("sing up form", async ({ page }) => {
//   await page.goto(`/auth/sign-up`);
//
//   /**
//    * Test that the login form is invalid when empty
//    */
//   const signUpButton = await page.getByTestId("create-account");
//   await signUpButton.click();
//
//   const nameErrorMessage = await page.getByTestId("name-input-error");
//   await expect(nameErrorMessage).toHaveText(
//     "Please enter a first and last name"
//   );
//
//   const emailErrorMessage = await page.getByTestId("email-input-error");
//   await expect(emailErrorMessage).toHaveText("Please enter a valid email");
//
//   const passwordErrorMessage = await page.getByTestId("password-input-error");
//   await expect(passwordErrorMessage).toHaveText(
//     "Please enter a valid password (8 characters, a number, and a special character)"
//   );
//
//   const companyNameErrorMessage = await page.getByTestId(
//     "company-name-input-error"
//   );
//   await expect(companyNameErrorMessage).toHaveText(
//     "Please enter a valid company name"
//   );
//
//   /**
//    * Test that the signup form is successful when filled out
//    */
//   const emailInput = await page.getByTestId("email-input");
//   const passwordInput = await page.getByTestId("password-input");
//   const nameInput = await page.getByTestId("name-input");
//   const companyInput = await page.getByTestId("company-name-input");
//
//   await emailInput.fill(`test+${Date.now()}@getparallel.com`);
//   await passwordInput.fill("Password123!");
//   await nameInput.fill("Testy McTesterson");
//   await companyInput.fill("Parallel");
//
//   await signUpButton.click();
//
//   await page.waitForURL(`/onboarding`);
// });
