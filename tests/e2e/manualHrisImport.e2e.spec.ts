import { expect, test } from "@playwright/test";
import login from "../utils/login";
import path from "path";
import compareTableData from "../utils/compareTableData";

test.describe.configure({ mode: "parallel" });

test.describe("Manual HRIS import", async () => {
  test("Initial import", async ({ page }) => {
    await login(page, "test+manualhrisinitial@getparallel.com");
    await page.goto(`/settings`);

    await page.getByText("Manual File Upload").click();

    /**
     * Check that the "Other" input is not visible when a different selection is made than "Other"
     */
    await page.getByTestId("manualIntegrationHrisSelection-selection").click();
    await page
      .getByTestId("manualIntegrationHrisSelection-option-Rippling")
      .click();
    await expect(page.getByTestId("other-hris-name-input")).not.toBeVisible();
    await page.getByTestId("manualIntegrationHrisSelection-selection").click();
    await page
      .getByTestId("manualIntegrationHrisSelection-option-Other")
      .click();
    await expect(page.getByTestId("other-hris-name-input")).toBeVisible();

    /**
     * Check validation on inputs before advancing
     */
    await page.getByTestId("upload-hris-file").click();
    await expect(page.getByTestId("other-hris-name-input-error")).toHaveText(
      "Data source is required",
    );
    await expect(page.getByTestId("manual-hris-upload-file-error")).toHaveText(
      "A file must be selected",
    );

    /**
     * File upload
     */
    await page.getByText("choose a file").click();
    const fileToUpload = path.join(
      process.cwd(),
      "/tests/fixtures/manualHrisImport/manualHrisInitialImport.csv",
    );
    await page
      .getByTestId("hris-import-file-input")
      .setInputFiles(fileToUpload);
    await expect(
      page.getByText("File Ready: manualHrisInitialImport.csv"),
    ).toBeVisible();

    await page.getByTestId("other-hris-name-input").fill("Manually tracked");

    await page.getByTestId("upload-hris-file").click();

    /**
     * Mapping Page
     */
    await expect(page.getByText("Map Columns")).toBeVisible();

    /**
     * Form validation
     */
    await page.getByTestId("submit-hris-data").click();
    await expect(page.getByTestId("mapping-required-fields")).toHaveText(
      `Employee NumberFirst NameLast NameHire DateJob TitleEmployment Type (Full Part/Time)Compensation RatePayment Type (Hourly/Salary)`,
    );

    /**
     * Field mapping
     */
    await page.getByTestId("field-mapping-Employee Number-selection").click();
    await page
      .getByTestId("field-mapping-Employee Number-option-employeeNumber")
      .click();
    await page.getByTestId("field-mapping-First name-button").click();
    // Check that previously selected option is disabled
    await expect(
      page.getByTestId("field-mapping-First name-option-employeeNumber"),
    ).toBeDisabled();
    await page
      .getByTestId("field-mapping-First name-option-employeeFirstName")
      .click();
    await page.getByTestId("field-mapping-Last name-selection").click();
    await page
      .getByTestId("field-mapping-Last name-option-employeeLastName")
      .click();
    await page.getByTestId("field-mapping-Title-button").click();
    await page.getByTestId("field-mapping-Title-option-jobTitle").click();
    await page.getByTestId("field-mapping-Department-button").click();
    await page
      .getByTestId("field-mapping-Department-option-department")
      .click();
    await page.getByTestId("field-mapping-Manager-button").click();
    await page.getByTestId("field-mapping-Manager-option-managerName").click();
    await page.getByTestId("field-mapping-Employment type name-button").click();
    await page
      .getByTestId("field-mapping-Employment type name-option-employmentType")
      .click();
    await page.getByTestId("field-mapping-Annual compensation-button").click();
    await page
      .getByTestId("field-mapping-Annual compensation-option-compensationRate")
      .click();
    await page.getByTestId("field-mapping-Start date-button").click();
    await page.getByTestId("field-mapping-Start date-option-hireDate").click();
    await page.getByTestId("field-mapping-End date-button").click();
    await page
      .getByTestId("field-mapping-End date-option-terminationDate")
      .click();
    await page.getByTestId("field-mapping-Work email-selection").click();
    await page
      .getByTestId("field-mapping-Work email-option-employeeEmail")
      .click();
    await page.getByTestId("field-mapping-Payment type-button").click();
    await page
      .getByTestId("field-mapping-Payment type-option-paymentType")
      .click();

    await page.getByTestId("submit-hris-data").click();

    const h3Selector = 'h3:has-text("Success")';
    await page.waitForSelector(h3Selector);

    await expect(
      page.getByText(
        "3 current and past roles were successfully imported. View uploaded employees on the Headcount page.",
      ),
    ).toBeVisible();

    await page.getByRole("button", { name: "Close" }).click();
    await expect(page.getByText("Employee Data Last Updated")).toBeVisible();

    /**
     * Verify data imported on headcount page
     */
    await page.goto(
      `/headcount?type=current&sortBy=employeeName&sortDirection=asc`,
    );

    // Wait for table to render
    await expect(page.getByText("Clark Kent")).toBeVisible();

    await page.waitForSelector("table");

    await expect(page.getByTestId("table-header-column")).toHaveText([
      "",
      "ID",
      "Employee",
      "Job Title",
      "Pay Rate",
      "Department",
      "Hire Date",
      "",
    ]);

    const table = page.locator("table");
    const rows = table.locator("tbody tr");

    const expectedData = [
      [
        "",
        "1",
        "Clark Kent",
        "CTO",
        "$240,000/yr",
        "Executive Management",
        "07/01/2023",
        "",
      ],
      [
        "",
        "2",
        "Eric Clapton",
        "CEO",
        "$60,000/yr",
        "Executive Management",
        "07/01/2023",
        "",
      ],
      [
        "",
        "3",
        "Jon Hamm",
        "Financial Analyst",
        "$15.75/hr",
        "Finance",
        "12/01/2023",
        "",
      ],
    ];

    await compareTableData(rows, expectedData);
  });

  test("Update", async ({ page }) => {
    await login(page, "test+manualhrisupdate@getparallel.com");
    await page.goto(`/settings`);

    await page.getByText("Manual File Upload").click();

    await page.getByTestId("manualIntegrationHrisSelection-selection").click();
    await page
      .getByTestId("manualIntegrationHrisSelection-option-Other")
      .click();

    /**
     * File upload
     */
    await page.getByText("choose a file").click();
    const fileToUpload = path.join(
      process.cwd(),
      "/tests/fixtures/manualHrisImport/manualHrisUpdate.csv",
    );
    await page
      .getByTestId("hris-import-file-input")
      .setInputFiles(fileToUpload);
    await expect(
      page.getByText("File Ready: manualHrisUpdate.csv"),
    ).toBeVisible();

    await page.getByTestId("other-hris-name-input").fill("Manually tracked");

    await page.getByTestId("upload-hris-file").click();

    /**
     * Mapping Page
     */
    await expect(page.getByText("Map Columns")).toBeVisible();

    /**
     * Form validation
     */
    await page.getByTestId("submit-hris-data").click();
    await expect(page.getByTestId("mapping-required-fields")).toHaveText(
      `Employee NumberFirst NameLast NameHire DateJob TitleEmployment Type (Full Part/Time)Compensation RatePayment Type (Hourly/Salary)`,
    );

    /**
     * Field mapping
     */
    await page.getByTestId("field-mapping-Employee Number-selection").click();
    await page
      .getByTestId("field-mapping-Employee Number-option-employeeNumber")
      .click();
    await page.getByTestId("field-mapping-First name-button").click();
    // Check that previously selected option is disabled
    await expect(
      page.getByTestId("field-mapping-First name-option-employeeNumber"),
    ).toBeDisabled();
    await page
      .getByTestId("field-mapping-First name-option-employeeFirstName")
      .click();
    await page.getByTestId("field-mapping-Last name-selection").click();
    await page
      .getByTestId("field-mapping-Last name-option-employeeLastName")
      .click();
    await page.getByTestId("field-mapping-Title-button").click();
    await page.getByTestId("field-mapping-Title-option-jobTitle").click();
    await page.getByTestId("field-mapping-Department-button").click();
    await page
      .getByTestId("field-mapping-Department-option-department")
      .click();
    await page.getByTestId("field-mapping-Manager-button").click();
    await page.getByTestId("field-mapping-Manager-option-managerName").click();
    await page.getByTestId("field-mapping-Employment type name-button").click();
    await page
      .getByTestId("field-mapping-Employment type name-option-employmentType")
      .click();
    await page.getByTestId("field-mapping-Annual compensation-button").click();
    await page
      .getByTestId("field-mapping-Annual compensation-option-compensationRate")
      .click();
    await page.getByTestId("field-mapping-Start date-button").click();
    await page.getByTestId("field-mapping-Start date-option-hireDate").click();
    await page.getByTestId("field-mapping-End date-button").click();
    await page
      .getByTestId("field-mapping-End date-option-terminationDate")
      .click();
    await page.getByTestId("field-mapping-Work email-selection").click();
    await page
      .getByTestId("field-mapping-Work email-option-employeeEmail")
      .click();
    await page.getByTestId("field-mapping-Payment type-button").click();
    await page
      .getByTestId("field-mapping-Payment type-option-paymentType")
      .click();

    await page.getByTestId("submit-hris-data").click();

    const h3Selector = 'h3:has-text("Success")';
    await page.waitForSelector(h3Selector);

    await expect(
      page.getByText("There were 4 updates to your headcount data"),
    ).toBeVisible();

    await expect(page.getByTestId("modifications-summary-value")).toHaveText(
      "2",
    );
    await expect(page.getByTestId("new-employees-summary-value")).toHaveText(
      "1",
    );
    await expect(page.getByTestId("terms-summary-value")).toHaveText("1");

    await page.getByRole("button", { name: "Close" }).click();
    await expect(page.getByText("Employee Data Last Updated")).toBeVisible();

    /**
     * Verify data imported on headcount page
     */
    await page.goto(
      `/headcount?type=current&sortBy=employeeName&sortDirection=asc`,
    );

    // Wait for table to render
    await expect(page.getByText("Clark Kent")).toBeVisible();

    await page.waitForSelector("table");

    await expect(page.getByTestId("table-header-column")).toHaveText([
      "",
      "ID",
      "Employee",
      "Job Title",
      "Pay Rate",
      "Department",
      "Hire Date",
      "",
    ]);

    const table = page.locator("table");
    const rows = table.locator("tbody tr");

    const expectedData = [
      ["", "4", "Loki Laufeyson", "-", "-", "-", "11/15/2023", "Assign"],
      [
        "",
        "1",
        "Clark Kent",
        "CTO",
        "$240,000/yr",
        "Executive Management",
        "07/01/2023",
        "",
      ],
      [
        "",
        "2",
        "Eric Clapton",
        "CEO",
        "$67,000/yr",
        "Executive Management",
        "07/01/2023",
        "",
      ],
      [
        "",
        "3",
        "Jon Hamm",
        "Financial Analyst",
        "$15.75/hr",
        "Finance",
        "12/01/2023",
        "",
      ],
      [
        "",
        "5",
        "Peter Parker",
        "Industry Analyst",
        "$30.00/hr",
        "Finance",
        "08/01/2023",
        "",
      ],
    ];

    await compareTableData(rows, expectedData);

    // Validate that forecasted termination converted to actual after upload
    await page.goto(`/positions/a2de4e5a-eaff-4856-8339-1e45c033ab66`);

    await expect(
      page.locator('[data-testid^="version-change-description"]').nth(0),
    ).toHaveText("Backfill");

    await expect(
      page.locator('[data-testid^="version-change-description"]').nth(1),
    ).toHaveText("Employee terminated");

    const actualTermHistory = await page
      .locator('[data-testid^="position-version-actual-card-"]')
      .first();
    await expect(actualTermHistory.getByTestId(/change-reason-.*/)).toHaveText(
      "Termination",
    );
    await expect(
      actualTermHistory.getByTestId(/change-effective-date-.*/),
    ).toHaveText("10/17/2023");

    await expect(
      page.locator('[data-testid^="version-change-description"]').nth(2),
    ).toHaveText("Initial Position");

    // Validate that forecasted changes prior to the actual coming in are deleted
    await page.goto(`/positions/cc358404-4337-47bf-8779-a84406f8fc2d`);

    await expect(
      page.locator('[data-testid^="version-change-description"]'),
    ).toHaveCount(3);

    // Asserting "Raise 3" is present as it was not superseded by the manual upload
    await expect(
      page.locator('[data-testid^="version-change-description"]').nth(0),
    ).toHaveText("Raise 3");

    // Asserting "Raise 2" is no longer present as it was superseded by the manual upload
    await expect(
      page.locator('[data-testid^="version-change-description"]').nth(1),
    ).toHaveText("Updated from manual import");

    // Asserting "Raise 1" is no longer present as it was superseded by the manual upload
    await expect(
      page.locator('[data-testid^="version-change-description"]').nth(2),
    ).toHaveText("Initial Position");

    const actualForecastChangeHistory = await page
      .locator('[data-testid^="position-version-actual-card-"]')
      .first();

    await expect(
      actualForecastChangeHistory.getByTestId(/change-effective-date-.*/),
    ).toHaveText("09/05/2023");

    await page.goto("/tasks");

    const backfillElement = page.locator('[id^="backfill-position-"]').first();
    expect(await backfillElement.textContent()).toContain("Clark Kent");

    const unassignedTask = page
      .locator('[id^="unassigned-employee-task-"]')
      .first();
    expect(await unassignedTask.textContent()).toContain("Loki Laufeyson");
  });
});
