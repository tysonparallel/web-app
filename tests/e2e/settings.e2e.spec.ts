import { expect, test } from "@playwright/test";
import login from "../utils/login";

test.describe.configure({ mode: "parallel" });

test.beforeEach(async ({ page }) => {
  await login(page, "test+growth@getparallel.com");
});

test("Settings Page - Fiscal year", async ({ page }) => {
  await page.goto(`/settings`);

  const fiscalYearStartSelectionFirst = page.getByTestId(
    "fiscal-year-start-selection",
  );
  await expect(fiscalYearStartSelectionFirst).toHaveText("January");

  await page.getByTestId("fiscal-year-start-button").click();
  await page.getByTestId("fiscal-year-start-option-FEBRUARY").click();
  await page.getByTestId("fiscal-year-start-save-button").click();

  await expect(fiscalYearStartSelectionFirst).toHaveText("February");

  await page.getByTestId("fiscal-year-start-button").click();
  await page.getByTestId("fiscal-year-start-option-JANUARY").click();
  await page.getByTestId("fiscal-year-start-save-button").click();
  const fiscalYearStartSelectionSecond = page.getByTestId(
    "fiscal-year-start-selection",
  );
  await expect(fiscalYearStartSelectionSecond).toHaveText("January");

  await page.getByTestId("connect-an-hris-button").click();
  await expect(page).toHaveURL(/.*onboarding/);
});
