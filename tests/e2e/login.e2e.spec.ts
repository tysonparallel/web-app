import { expect, test } from "@playwright/test";
import login from "../utils/login";

test.describe.configure({ mode: "parallel" });

test("redirect to login on base url visit", async ({ page }) => {
  await page.goto(`/`);
  await expect(page).toHaveURL(/.*login/);
});

test("login form", async ({ page }) => {
  await page.goto(`/auth/login?method=standard`);

  await page.getByTestId("email-input").fill("");
  await page.getByTestId("password-input").fill("");

  /**
   * Test that the login form is invalid when empty
   */
  await page.getByRole("button", { name: "Log In" }).click();

  const emailErrorMessage = await page.getByTestId("email-input-error");
  await expect(emailErrorMessage).toHaveText("Please enter a valid email");

  const passwordErrorMessage = await page.getByTestId("password-input-error");
  await expect(passwordErrorMessage).toHaveText(
    "Please enter a valid password (8 characters, a number, and a special character)",
  );

  /**
   * Test that the login form is successful when filled out
   */
  await login(page);
});

test("otp login", async ({ page }) => {
  await page.goto(
    `/auth/otp?otp=123456&emailValue=test%2Botplogin%40getparallel.com`,
  );

  page.waitForURL("/onboarding");

  await page.getByTestId("sidemenu-logout").click();

  page.waitForURL("/auth/login");

  await page.goto(
    `/auth/otp?otp=123456&emailValue=test%2Botplogin%40getparallel.com`,
  );

  await page.waitForEvent("requestfinished", {
    predicate: (req) => req.url().includes("/v1/auth/otp"),
  });

  expect(page.getByText("Invalid Code")).toBeVisible();
});
