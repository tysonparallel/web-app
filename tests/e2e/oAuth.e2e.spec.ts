import { expect, test } from "@playwright/test";
import login from "../utils/login";

test.describe.configure({ mode: "parallel" });

test("GSheets integration, logged in", async ({ page }) => {
  await login(page, "test+growth@getparallel.com");

  await page.goto(
    `oauth?clientId=209f306e-ec4b-4f56-9b8a-58f939ad49b2&callbackUrl=https://docs.google.com/spreadsheets/d/xyzpdq`,
  );

  await expect(page.getByTestId("oauth-connection-div")).toBeVisible();

  await page.getByRole("button", { name: "Accept" }).click();
  await page.waitForURL(
    "https://script.google.com/macros/s/AKfycbx88S1pge4wEESKwvQq9_ucPvUVnktbnlQEuzU8NZsocGv4FhuuSGiA0fNdVtvo63uxdA/exec?code=**",
  );
});

test("GSheets integration, not logged in", async ({ page }) => {
  await page.goto(
    `oauth?clientId=209f306e-ec4b-4f56-9b8a-58f939ad49b2&callbackUrl=https://docs.google.com/spreadsheets/d/xyzpdq`,
  );

  await page.getByTestId("email-input").fill("test+growth@getparallel.com");
  await page.getByTestId("password-input").fill("Password123!");
  await page.getByRole("button", { name: "Log In" }).click();

  await expect(page.getByTestId("oauth-connection-div")).toBeVisible();

  await page.waitForURL(
    "**/oauth?clientId=209f306e-ec4b-4f56-9b8a-58f939ad49b2&callbackUrl=https://docs.google.com/spreadsheets/d/xyzpdq",
  );
});

test("GSheets integration, declining authorization", async ({ page }) => {
  await login(page, "test+growth@getparallel.com");

  await page.goto(
    `oauth?clientId=209f306e-ec4b-4f56-9b8a-58f939ad49b2&callbackUrl=https://www.mockurl.com`,
  );

  await page.getByRole("button", { name: "Decline" }).click();

  await page.waitForURL(
    "https://script.google.com/macros/s/AKfycbx88S1pge4wEESKwvQq9_ucPvUVnktbnlQEuzU8NZsocGv4FhuuSGiA0fNdVtvo63uxdA/exec",
  );
});
