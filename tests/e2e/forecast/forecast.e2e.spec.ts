import { expect, test } from "@playwright/test";
import * as fs from "fs";
import login from "../../utils/login";
import compareTableData from "../../utils/compareTableData";
import compareDownload from "../../utils/compareDownload";

test.describe.configure({ mode: "parallel" });

test("Forecast Page", async ({ page }) => {
  await login(page, "test+default@getparallel.com");

  await page.goto(`/forecast`);

  await expect(page.getByText("Finance").first()).toBeVisible();

  await page.getByTestId("segment-headcount").click();

  const employeeHeadcountHeaderRows = await page.locator(
    '[data-testid="forecast-header-Headcount"] + div thead tr th',
  );

  await expect(employeeHeadcountHeaderRows.first()).toBeVisible();

  const expectedEmployeeHeadcountHeaders = [
    "Department",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  await compareTableData(
    employeeHeadcountHeaderRows,
    expectedEmployeeHeadcountHeaders,
  );

  const employeeHeadcountRows = page.locator(
    '[data-testid="forecast-header-Headcount"] + div tbody tr',
  );

  const employeeExpectedData = [
    ["Finance", "0", "1", "1", "1", "1", "1", "1", "1", "2", "2", "2", "2"],
    ["Engineering", "0", "0", "1", "0", "0", "0", "0", "0", "0", "1", "1", "1"],
    ["Total", "0", "1", "2", "1", "1", "1", "1", "1", "2", "3", "3", "3"],
  ];

  await compareTableData(employeeHeadcountRows, employeeExpectedData);
});

test("Forecast Page, Headcount - Download CSV", async ({ page, browser }) => {
  await login(page, "test+default@getparallel.com");
  await page.goto(`/forecast`);
  await expect(page.getByText("Finance").first()).toBeVisible();
  await page.getByTestId("segment-headcount").click();
  // Wait for table to load
  await expect(page.getByText("Finance").first()).toBeVisible();

  await compareDownload(
    page,
    "download-headcount-projections-csv",
    `Headcount
  Department,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec
  Finance,0,1,1,1,1,1,1,1,2,2,2,2
  Engineering,0,0,1,0,0,0,0,0,0,1,1,1
  Total,0,1,2,1,1,1,1,1,2,3,3,3
  Employee Bridge
  Department,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec
  Month Start,0,0,1,2,1,1,1,1,1,2,3,3
  New Hires / Transfers In,0,1,1,0,0,0,0,0,1,1,0,0
  Terms / Transfers Out,0,0,0,1,0,0,0,0,0,0,0,0
  Month End,0,1,2,1,1,1,1,1,2,3,3,3`,
  );
});

test("Forecast Page, Salaries - Download CSV ", async ({ page, browser }) => {
  await login(page, "test+default@getparallel.com");
  await page.goto(`/forecast`);
  await expect(page.getByText("Finance").first()).toBeVisible();
  await page.getByTestId("segment-salaries").click();
  // Wait for table to load
  await expect(page.getByText("Department")).toHaveCount(5);

  await compareDownload(
    page,
    "download-salaries-projections-csv",
    `"Salaries"
  "Department","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
  "Finance","$0","$10,000","$10,000","$10,000","$10,000","$10,000","$10,000","$10,000","$15,000","$15,000","$15,000","$15,000"
  "Engineering","$0","$0","$10,000","$333","$0","$0","$0","$0","$0","$28,000","$28,000","$28,000"
  "Total","$0","$10,000","$20,000","$10,333","$10,000","$10,000","$10,000","$10,000","$15,000","$43,000","$43,000","$43,000"
  "Bonuses"
  "Department","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
  "Finance","$0","$0","$0","$0","$0","$0","$0","$0","$0","$0","$0","$0"
  "Engineering","$0","$0","$0","$0","$0","$0","$0","$0","$0","$833","$833","$833"
  "Total","$0","$0","$0","$0","$0","$0","$0","$0","$0","$833","$833","$833"
  "Commissions"
  "Department","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
  "Finance","$0","$0","$0","$0","$0","$0","$0","$0","$750","$750","$750","$750"
  "Engineering","$0","$0","$0","$0","$0","$0","$0","$0","$0","$0","$0","$0"
  "Total","$0","$0","$0","$0","$0","$0","$0","$0","$750","$750","$750","$750"
  "Burden"
  "Department","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
  "Finance","$0","$0","$0","$0","$2,000","$2,000","$2,000","$2,000","$3,150","$3,150","$3,150","$3,150"
  "Engineering","$0","$0","$0","$0","$0","$0","$0","$0","$0","$5,767","$5,767","$5,767"
  "Total","$0","$0","$0","$0","$2,000","$2,000","$2,000","$2,000","$3,150","$8,917","$8,917","$8,917"
  "Swag"
  "Department","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
  "Finance","$0","$0","$0","$0","$0","$15","$15","$15","$30","$30","$30","$30"
  "Engineering","$0","$0","$0","$0","$0","$0","$0","$0","$0","$15","$15","$15"
  "Total","$0","$0","$0","$0","$0","$15","$15","$15","$30","$45","$45","$45"`,
  );
});

test("Forecast Page, Expense Models", async ({ page }) => {
  await login(page, "test+default@getparallel.com");
  await page.locator('a[href="/forecast"]').first().click();

  await expect(page.getByTestId("forecast-header-Swag")).toBeVisible();

  const swagExpenseTable = await page.locator(
    '[data-testid="forecast-header-Swag"] + div table',
  );

  await expect(swagExpenseTable.locator("th")).toHaveText([
    "Department",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ]);

  const swagExpectedData = [
    [
      "Finance",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$15",
      "$15",
      "$15",
      "$30",
      "$30",
      "$30",
      "$30",
    ],
    [
      "Engineering",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$15",
      "$15",
      "$15",
    ],
    [
      "Total",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$15",
      "$15",
      "$15",
      "$30",
      "$45",
      "$45",
      "$45",
    ],
  ];

  await compareTableData(
    await swagExpenseTable.locator("tbody tr"),
    swagExpectedData,
  );
  await expect(page.getByTestId("forecast-header-Burden")).toBeVisible();

  const burdenExpenseTable = await page.locator(
    '[data-testid="forecast-header-Burden"] + div table',
  );

  await expect(burdenExpenseTable.locator("th")).toHaveText([
    "Department",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ]);

  const burdenExpectedData = [
    [
      "Finance",
      "$0",
      "$0",
      "$0",
      "$0",
      "$2,000",
      "$2,000",
      "$2,000",
      "$2,000",
      "$3,150",
      "$3,150",
      "$3,150",
      "$3,150",
    ],
    [
      "Engineering",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$5,767",
      "$5,767",
      "$5,767",
    ],
    [
      "Total",
      "$0",
      "$0",
      "$0",
      "$0",
      "$2,000",
      "$2,000",
      "$2,000",
      "$2,000",
      "$3,150",
      "$8,917",
      "$8,917",
      "$8,917",
    ],
  ];

  await compareTableData(
    await burdenExpenseTable.locator("tbody tr"),
    burdenExpectedData,
  );
});

test("Forecast Page, Variable Pay", async ({ page }) => {
  await login(page, "test+default@getparallel.com");
  await page.locator('a[href="/forecast"]').first().click();

  await expect(page.getByTestId("forecast-header-Bonuses")).toBeVisible();

  const bonusExpenseTable = await page.locator(
    '[data-testid="forecast-header-Bonuses"] + div table',
  );

  await expect(bonusExpenseTable.locator("th")).toHaveText([
    "Department",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ]);

  const bonusExpectedData = [
    [
      "Finance",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
    ],
    [
      "Engineering",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$833",
      "$833",
      "$833",
    ],
    [
      "Total",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$833",
      "$833",
      "$833",
    ],
  ];

  await compareTableData(
    await bonusExpenseTable.locator("tbody tr"),
    bonusExpectedData,
  );
  await expect(page.getByTestId("forecast-header-Commissions")).toBeVisible();

  const commissionExpenseTable = await page.locator(
    '[data-testid="forecast-header-Commissions"] + div table',
  );

  await expect(commissionExpenseTable.locator("th")).toHaveText([
    "Department",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ]);

  const commissionExpectedData = [
    [
      "Finance",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$750",
      "$750",
      "$750",
      "$750",
    ],
    [
      "Engineering",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
    ],
    [
      "Total",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$0",
      "$750",
      "$750",
      "$750",
      "$750",
    ],
  ];

  await compareTableData(
    await commissionExpenseTable.locator("tbody tr"),
    commissionExpectedData,
  );
});
