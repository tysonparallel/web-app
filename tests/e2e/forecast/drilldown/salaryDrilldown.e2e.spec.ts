import { expect, Page, test } from "@playwright/test";
import login from "../../../utils/login";
import compareDownload from "../../../utils/compareDownload";
import compareTableData from "../../../utils/compareTableData";

test.describe.configure({ mode: "parallel" });

test("Forecast Drilldown Salary", async ({ page }) => {
  /**
   * Setup test and get to forecast page
   */
  await login(page, "demo@getparallel.com");
  await page.goto(`/forecast?format=salaries`);
  await expect(page.getByText("Marketing").first()).toBeVisible();

  // Salaries -> Customer Success -> February
  await page.getByText("$69,279").click();

  /**
   * Drawer assertions
   */
  await expect(page.getByTestId("drawer-title")).toHaveText("Salaries");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "February 2023 | Customer Success",
  );

  let drawer = page.getByTestId("drawer");

  /**
   * Check table is correct
   */
  const drilldownTable = drawer.locator("table");
  await expect(drilldownTable.getByTestId("table-header-row")).toHaveText([
    "EMPLOYEE (13)POSITIONSCALC.MO. IMPACT",
  ]);

  const drilldownRows = drilldownTable.locator("tbody tr");
  const expectedDrilldownRows: string[][] = [
    ["Aaron Eckerly32", "Customer Success AdvocateFull Time", "View", "$3,120"],
    ["Amy Granger23", "Customer Retention ManagerFull Time", "View", "$6,250"],
    ["Andy Graves25", "Customer Success AdvocateFull Time", "View", "$5,417"],
    ["Cheryl Barnet10", "VP of Customer SuccessFull Time", "View", "$7,500"],
    [
      "Ellen Karren99",
      "Customer Implementation ManagerFull Time",
      "View",
      "$5,417",
    ],
    ["Emily Gomez31", "Customer Success AdvocateFull Time", "View", "$3,120"],
    ["John Quentin47", "Customer Success AdvocatePart Time", "View", "$10,000"],
    [
      "Nelson Elliott70",
      "Software Implementation SpecialistFull Time",
      "View",
      "$4,583",
    ],
    ["Omar Waldwin68", "Customer Success AdvocateFull Time", "View", "$3,120"],
    [
      "Shawn Williams63",
      "Software Implementation SpecialistFull Time",
      "View",
      "$3,868",
    ],
    [
      "Susan Roberts64",
      "Software Implementation SpecialistFull Time",
      "View",
      "$3,384",
    ],
    [
      "Trey Pickard78",
      "Software Implementation SpecialistUnknown",
      "View",
      "$7,917",
    ],
    [
      "Yasmine Dean58",
      "Software Implementation SpecialistFull Time",
      "View",
      "$5,583",
    ],
  ];
  await compareTableData(drilldownRows, expectedDrilldownRows);

  /**
   * Calculation modal
   */
  await drawer.getByText("View").first().click();
  let calculationModal = page.getByTestId("drilldown-calculation-modal");
  await expect(calculationModal).toBeVisible();
  await expect(calculationModal).toHaveText(
    "Aaron EckerlyCustomer Success Advocate02/01/2023-02/28/2023$18.00*40*52*1/12*28/28=$3,120.00",
  );
  await page.getByTestId("drilldown-calculation-modal-close").click();

  /**
   * Download and compare CSV
   */
  await compareDownload(
    page,
    "drilldown-drawer-export",
    `EmployeeNumber,EmployeeName,PositionTitle,StartDate,EndDate,Calculation32,AaronEckerly,CustomerSuccessAdvocate,02/01/2023,
    02/28/2023,=ROUND(18*40*52*1/12*28/28,2)23,AmyGranger,CustomerRetentionManager,02/01/2023,02/28/2023,
    =ROUND(75000*1/12*28/28,2)25,AndyGraves,CustomerSuccessAdvocate,02/01/2023,02/28/2023,=ROUND(65000*1/12*28/28,2)10,
    CherylBarnet,VPofCustomerSuccess,02/01/2023,02/28/2023,=ROUND(90000*1/12*28/28,2)99,EllenKarren,CustomerImplementationManager,
    02/01/2023,02/28/2023,=ROUND(65000*1/12*28/28,2)31,EmilyGomez,CustomerSuccessAdvocate,02/01/2023,02/28/2023,
    =ROUND(18*40*52*1/12*28/28,2)47,JohnQuentin,CustomerSuccessAdvocate,02/01/2023,02/28/2023,=ROUND(120000*1/12*28/28,2)70,
    NelsonElliott,SoftwareImplementationSpecialist,02/01/2023,02/28/2023,=ROUND(55000*1/12*28/28,2)68,OmarWaldwin,
    CustomerSuccessAdvocate,02/01/2023,02/28/2023,=ROUND(18*40*52*1/12*28/28,2)63,ShawnWilliams,SoftwareImplementationSpecialist,
    02/01/2023,02/28/2023,=ROUND(14*40*52*1.594/12*28/28,2)64,SusanRoberts,SoftwareImplementationSpecialist,02/01/2023,02/28/2023,
    =ROUND(24*40*52*0.8135/12*28/28,2)78,TreyPickard,SoftwareImplementationSpecialist,02/01/2023,02/28/2023,
    =ROUND(95000*1/12*28/28,2)58,YasmineDean,SoftwareImplementationSpecialist,02/01/2023,02/28/2023,=ROUND(67000*1/12*28/28,2)`,
  );

  /**
   * Check table is correct after searching
   */
  const searchInput = drawer.getByTestId("drilldown-drawer-input-input");
  await searchInput.fill("Cheryl Barnet");
  const expectedRowsAfterSearch = [
    ["Cheryl Barnet10", "VP of Customer SuccessFull Time", "View", "$7,500"],
  ];
  const newDrilldownRows = drilldownTable.locator("tbody tr");
  await compareTableData(newDrilldownRows, expectedRowsAfterSearch);

  /**
   * Close drawer
   */
  await drawer.getByTestId("drilldown-drawer-close").click();

  /**
   * Run similar tests with sales to check for edge cases
   */
  await page.getByText("$15,929,344").click();
  await expect(page.getByTestId("drawer-title")).toHaveText("Salaries");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "February 2023 | Sales",
  );

  drawer = page.getByTestId("drawer");

  await drawer.getByText("View").first().click();
  calculationModal = page.getByTestId("drilldown-calculation-modal");
  await expect(calculationModal).toBeVisible();
  await expect(calculationModal).toHaveText(
    "Carly SeymourSales Team Lead02/01/2023-02/28/2023$90,000.00*1/12*28/28=$7,500.00",
  );
  await page.getByTestId("drilldown-calculation-modal-close").click();
  await drawer.getByTestId("drilldown-drawer-close").click();

  /**
   * Test currency conversion
   */
  await page.getByText("$55,689").first().click();
  await expect(page.getByTestId("drawer-title")).toHaveText("Salaries");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "February 2023 | Human Resources",
  );

  drawer = page.getByTestId("drawer");
  await drawer.getByText("View").first().click();
  calculationModal = page.getByTestId("drilldown-calculation-modal");
  await expect(calculationModal).toBeVisible();
  await expect(calculationModal).toHaveText(
    "Adam HoltTechnical Recruiter02/01/2023-02/28/2023A$24.00*40*52*0.7377/12*28/28=$3,068.87",
  );
  await page.getByTestId("drilldown-calculation-modal-close").click();
  await drawer.getByTestId("drilldown-drawer-close").click();
  /**
   * Test different month
   */
  await page.getByText("$64,160").click();
  await expect(page.getByTestId("drawer-title")).toHaveText("Salaries");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "March 2023 | Customer Success",
  );
  drawer = page.getByTestId("drawer");
  await drawer.getByText("View").first().click();
  calculationModal = page.getByTestId("drilldown-calculation-modal");
  await expect(calculationModal).toBeVisible();
  await expect(calculationModal).toHaveText(
    "Aaron EckerlyCustomer Success Advocate03/01/2023-03/31/2023$18.00*40*52*1/12*31/31=$3,120.00",
  );
  await page.getByTestId("drilldown-calculation-modal-close").click();

  /**
   * Navigate to position page from table
   */
  await page.getByText("VP of Customer Success").first().click();
  await page.waitForURL(
    new RegExp(
      /\/positions\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/,
    ),
  );
});

test("Forecast Drilldown Bonus", async ({ page }) => {
  // login and go to forecast page
  await login(page, "test+completeforecast@getparallel.com");
  await page.goto(`/forecast?format=salaries`);
  await expect(page.getByText("Finance").first()).toBeVisible();

  // Click on first bonus cell with a value > 0
  await page.getByText("$833").first().click();

  // Drawer assertions
  await expect(page.getByTestId("drawer-title")).toHaveText("Bonuses");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "October 2023 | Engineering",
  );
  let drawer = page.getByTestId("drawer");

  // check table
  const drilldownTable = drawer.locator("table");
  await expect(drilldownTable.getByTestId("table-header-row")).toHaveText([
    "EMPLOYEE (1)POSITIONSCALC.MO. IMPACT",
  ]);

  const drilldownRows = drilldownTable.locator("tbody tr");
  const expectedDrilldownRows = [
    ["-", "Distinguished EngineerFull Time", "View", "$833"],
  ];
  await compareTableData(drilldownRows, expectedDrilldownRows);

  // Check modal
  await drawer.getByText("View").first().click();
  let calculationModal = page.getByTestId("drilldown-calculation-modal");
  await expect(calculationModal).toBeVisible();
  await expect(calculationModal).toHaveText(
    "Distinguished Engineer10/01/2023-10/31/2023$10,000.00*1/12*31/31=$833.33",
  );
  await page.getByTestId("drilldown-calculation-modal-close").click();

  // Check download csv
  await compareDownload(
    page,
    "drilldown-drawer-export",
    `EmployeeNumber,EmployeeName,PositionTitle,StartDate,EndDate,Calculation,,DistinguishedEngineer,
    10/01/2023,10/31/2023,=ROUND(10000*1/12*31/31,2)`,
  );

  // Check navigation
  await page.getByText("Distinguished Engineer").first().click();
  await page.waitForURL(
    new RegExp(
      /\/positions\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/,
    ),
  );
});

test("Forecast Drilldown Commission", async ({ page }) => {
  await login(page, "test+completeforecast@getparallel.com");
  await page.goto(`/forecast?format=salaries`);
  await expect(page.getByText("Finance").first()).toBeVisible();

  // Click on first commission cell with a value > 0
  await page.getByText("$750").first().click();

  // Drawer assertions
  await expect(page.getByTestId("drawer-title")).toHaveText("Commissions");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "September 2023 | Finance",
  );
  let drawer = page.getByTestId("drawer");

  // check table
  const drilldownTable = drawer.locator("table");
  await expect(drilldownTable.getByTestId("table-header-row")).toHaveText([
    "EMPLOYEE (1)POSITIONSCALC.MO. IMPACT",
  ]);
  const drilldownRows = drilldownTable.locator("tbody tr");
  const expectedDrilldownRows = [
    ["Thomas Edison", "AnalystFull Time", "View", "$750"],
  ];
  await compareTableData(drilldownRows, expectedDrilldownRows);

  // Check modal
  await drawer.getByText("View").first().click();
  let calculationModal = page.getByTestId("drilldown-calculation-modal");
  await expect(calculationModal).toBeVisible();
  await expect(calculationModal).toHaveText(
    "Thomas EdisonAnalyst09/01/2023-09/30/2023$12,000.00*0.75*1/12*30/30=$750.00",
  );
  await page.getByTestId("drilldown-calculation-modal-close").click();

  // Check download csv
  await compareDownload(
    page,
    "drilldown-drawer-export",
    `EmployeeNumber,EmployeeName,PositionTitle,StartDate,EndDate,Calculation,ThomasEdison,Analyst,
    09/01/2023,09/30/2023,=ROUND(12000*0.75*1/12*30/30,2)`,
  );

  // Check navigation
  await page.getByText("Analyst").first().click();
  await page.waitForURL(
    new RegExp(
      /\/positions\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/,
    ),
  );
});

test("Forecast Drilldown Expense Model", async ({ page }) => {
  // Login
  await login(page, "test+completeforecast@getparallel.com");
  await page.goto(`/forecast?format=salaries`);
  await expect(page.getByText("Finance").first()).toBeVisible();

  // Click on first expense cell with a value $2000 in order to test percentage expense
  await page.getByText("$2,000").first().click();

  // Drawer assertions
  await expect(page.getByTestId("drawer-title")).toHaveText("Burden");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "May 2023 | Finance",
  );
  let drawer = page.getByTestId("drawer");

  // check table
  const drilldownTable = drawer.locator("table");
  await expect(drilldownTable.getByTestId("table-header-row")).toHaveText([
    "EMPLOYEE (1)POSITIONSCALC.MO. IMPACT",
  ]);
  const drilldownRows = drilldownTable.locator("tbody tr");
  const expectedDrilldownRows = [
    ["Emma Lee", "Finance ManagerFull Time", "View", "$2,000"],
  ];
  await compareTableData(drilldownRows, expectedDrilldownRows);

  // Check modal
  await drawer.getByText("View").first().click();
  let calculationModal = page.getByTestId("drilldown-calculation-modal");
  await expect(calculationModal).toBeVisible();
  await expect(calculationModal).toHaveText(
    "Emma LeeFinance Manager05/01/2023-05/31/2023($120,000.00+$0.00+$0.00*0)*1/12*31/31*0.2=$2,000.00",
  );
  await page.getByTestId("drilldown-calculation-modal-close").click();

  // Check download csv
  await compareDownload(
    page,
    "drilldown-drawer-export",
    `EmployeeNumber,EmployeeName,PositionTitle,StartDate,EndDate,Calculation,EmmaLee,FinanceManager,05/01/2023,05/31/2023,
    =ROUND((120000+0+0*0)*1/12*31/31*0.2,2)`,
  );

  await drawer.getByTestId("drilldown-drawer-close").click();

  // Test fixed expense
  await page.getByText("$30").first().click();

  // Drawer assertions
  await expect(page.getByTestId("drawer-title")).toHaveText("Swag");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "September 2023 | Finance",
  );
  drawer = page.getByTestId("drawer");

  // check table
  const secondDrilldownTable = drawer.locator("table");
  await expect(drilldownTable.getByTestId("table-header-row")).toHaveText([
    "EMPLOYEE (2)POSITIONSCALC.MO. IMPACT",
  ]);
  const secondDrilldownRows = secondDrilldownTable.locator("tbody tr");
  const secondExpectedDrilldownRows = [
    ["Emma Lee", "Finance ManagerFull Time", "View", "$15.00"],
    ["Thomas Edison", "AnalystFull Time", "View", "$15.00"],
  ];
  await compareTableData(secondDrilldownRows, secondExpectedDrilldownRows);

  // Check modal
  await drawer.getByText("View").first().click();
  calculationModal = page.getByTestId("drilldown-calculation-modal");
  await expect(calculationModal).toBeVisible();
  await expect(calculationModal).toHaveText(
    "Emma LeeFinance Manager09/01/2023-09/30/2023$15.00=$15.00",
  );
  await page.getByTestId("drilldown-calculation-modal-close").click();

  // Check download csv
  await compareDownload(
    page,
    "drilldown-drawer-export",
    `"Employee Number","Employee Name","Position Title","Start Date","End Date","Calculation"
"","Emma Lee","Finance Manager","09/01/2023","09/30/2023","=ROUND(15,2)"
"","Thomas Edison","Analyst","09/01/2023","09/30/2023","=ROUND(15,2)"`,
  );

  // Check navigation
  await page.getByText("Manager").first().click();
  await page.waitForURL(
    new RegExp(
      /\/positions\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/,
    ),
  );
});
