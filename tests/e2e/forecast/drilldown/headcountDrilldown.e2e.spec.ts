import { expect, Page, test } from "@playwright/test";
import login from "../../../utils/login";
import compareDownload from "../../../utils/compareDownload";
import compareTableData from "../../../utils/compareTableData";

test.describe.configure({ mode: "parallel" });

test("Forecast Drilldown Headcount", async ({ page }) => {
  /**
   * Setup test and get to forecast page
   */
  await login(page, "demo@getparallel.com");
  await page.goto(`/forecast?format=headcount`);
  await expect(page.getByText("Marketing").first()).toBeVisible();

  await page.getByRole("button", { name: "11" }).first().click();

  await expect(page.getByText("Ending Headcount (11)")).toBeVisible();

  await page.getByTestId("Ending Headcount (11)-expand-button").click();

  /**
   * Drawer assertions
   */
  await expect(page.getByTestId("drawer-title")).toHaveText("Headcount");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "February 2023 | Human Resources",
  );

  let drawer = page.getByTestId("drawer");

  /**
   * Check table is correct
   */
  const drilldownEndingTable = drawer.locator("table").last();
  await expect(drilldownEndingTable.getByTestId("table-header-row")).toHaveText(
    ["EMPLOYEEPOSITION"],
  );

  const drilldownEndingRows = drilldownEndingTable.locator("tbody tr");
  const expectedDrilldownRows: string[][] = [
    ["Adam Holt73", "Technical RecruiterFull Time", ""],
    ["Ashley Adams2", "HR AdministratorFull Time", ""],
    ["Charlotte Abbott1", "Sr. HR AdministratorFull Time", ""],
    ["Christina Agluinda3", "HR AdministratorFull Time", ""],
    ["Jake Huang100", "Benefits AdministratorFull Time", ""],
    ["Jennifer Caldwell6", "VP of PeopleFull Time", ""],
    ["Michael Romney91", "Benefits AdministratorUnknown", ""],
    ["Nathan Pazavich48", "Benefits AdministratorPart Time", ""],
    ["Shannon Anderson4", "HR AdministratorFull Time", ""],
    ["Warren Poole57", "Technical RecruiterFull Time", ""],
    ["Whitney Webster34", "Benefits AdministratorFull Time", ""],
  ];
  await compareTableData(drilldownEndingRows, expectedDrilldownRows);

  /**
   * Download and compare CSV
   */
  await compareDownload(
    page,
    "drilldown-drawer-export",
    `NewHires/TransfersIn(0)NodataavailableTerminations/TransfersOut(0)NodataavailableEndingHeadcount(11)EmployeeNumber,EmployeeName,PositionTitle,Start/EndDate73,AdamHolt,TechnicalRecruiter,2,AshleyAdams,HRAdministrator,1,CharlotteAbbott,Sr.HRAdministrator,3,ChristinaAgluinda,HRAdministrator,100,JakeHuang,BenefitsAdministrator,6,JenniferCaldwell,VPofPeople,91,MichaelRomney,BenefitsAdministrator,48,NathanPazavich,BenefitsAdministrator,4,ShannonAnderson,HRAdministrator,57,WarrenPoole,TechnicalRecruiter,34,WhitneyWebster,BenefitsAdministrator,`,
  );

  /**
   * Check table is correct after searching
   */
  const searchInput = drawer.getByTestId("drilldown-drawer-input-input");
  await searchInput.fill("Ashley Adams");
  const expectedRowsAfterSearch = [
    ["Ashley Adams2", "HR AdministratorFull Time", ""],
  ];
  const newDrilldownEndingRows = drilldownEndingTable.locator("tbody tr");
  await compareTableData(newDrilldownEndingRows, expectedRowsAfterSearch);

  /**
   * Close drawer
   */
  await drawer.getByTestId("drilldown-drawer-close").click();

  /**
   * Run similar tests with sales to check for edge cases
   */
  await page.getByRole("button", { name: "87" }).first().click();

  await expect(page.getByText("Ending Headcount (87)")).toBeVisible();

  await page.getByTestId("New Hires / Transfers In (2)-expand-button").click();

  await page
    .getByTestId("Terminations / Transfers Out (1)-expand-button")
    .click();

  await expect(page.getByTestId("drawer-title")).toHaveText("Headcount");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "June 2023 | Total",
  );

  drawer = page.getByTestId("drawer");

  const drilldownNewHireTable = drawer.locator("table").first();
  await expect(
    drilldownNewHireTable.getByTestId("table-header-row"),
  ).toHaveText(["EMPLOYEEPOSITIONSTART DATE"]);

  const drilldownNewHireRows = drilldownNewHireTable.locator("tbody tr");

  const expectedDrilldownNewHireRows = [
    ["Jeremy Steel209", "Corporate TrainerFull Time", "06/13/2023"],
    ["Katrina Cox208", "Software EngineerFull Time", "06/24/2023"],
  ];

  await compareTableData(drilldownNewHireRows, expectedDrilldownNewHireRows);

  const drilldownTerminationTable = drawer.locator("table").nth(1);
  await expect(
    drilldownTerminationTable.getByTestId("table-header-row"),
  ).toHaveText(["EMPLOYEEPOSITIONEND DATE"]);

  const drilldownTerminationRows =
    drilldownTerminationTable.locator("tbody tr");

  const expectedDrilldownTerminationRows = [
    ["Emily Gomez31", "Customer Success AdvocateFull Time", "06/21/2023"],
  ];

  await compareTableData(
    drilldownTerminationRows,
    expectedDrilldownTerminationRows,
  );
});

test("Forecast Drilldown Employee Bridge", async ({ page }) => {
  // login and go to forecast page
  await login(page, "test+default@getparallel.com");
  await page.goto(`/forecast?format=headcount`);
  await expect(page.getByText("Finance").first()).toBeVisible();

  const monthStartAprilCell = await page.getByTestId(
    "table-cell-All Groups: Month Start-4",
  );

  await monthStartAprilCell.locator("button").click();

  await expect(page.getByText("Emma Lee")).toBeVisible();

  // Drawer assertions
  await expect(page.getByTestId("drawer-title")).toHaveText("Employee Bridge");
  await expect(page.getByTestId("drawer-subtitle")).toHaveText(
    "April 2023 | Month Start",
  );
  let drawer = page.getByTestId("drawer");

  // check table
  const drilldownTable = drawer.locator("table");
  await expect(drilldownTable.getByTestId("table-header-row")).toHaveText([
    "EMPLOYEEPOSITIONDEPARTMENT",
  ]);

  const drilldownRows = drilldownTable.locator("tbody tr");
  const expectedDrilldownRows = [
    ["Emma Lee", "Finance ManagerFull Time", "Finance"],
    ["Jon Richards", "Senior EngineerFull Time", "Engineering"],
  ];
  await compareTableData(drilldownRows, expectedDrilldownRows);

  // Check download csv
  await compareDownload(
    page,
    "drilldown-drawer-export",
    `EmployeeNumber,EmployeeName,PositionTitle,Department,Start/EndDate,EmmaLee,FinanceManager,Finance,,JonRichards,SeniorEngineer,Engineering,`,
  );

  await drawer.getByTestId("drilldown-drawer-close").click();

  await page
    .getByTestId("table-cell-All Groups: New Hires / Transfers In-3")
    .locator("button")
    .click();

  await expect(page.getByText("Jon Richards")).toBeVisible();

  drawer = page.getByTestId("drawer");

  const drilldownNewHireTable = drawer.locator("table").first();

  await expect(
    drilldownNewHireTable.getByTestId("table-header-row"),
  ).toHaveText(["EMPLOYEEPOSITIONDEPARTMENTSTART DATE"]);

  const drilldownNewHireRows = drilldownNewHireTable.locator("tbody tr");

  const expectedDrilldownNewHireRows = [
    ["Jon Richards", "Senior EngineerFull Time", "Engineering", "03/01/2023"],
  ];

  await compareTableData(drilldownNewHireRows, expectedDrilldownNewHireRows);

  await compareDownload(
    page,
    "drilldown-drawer-export",
    `EmployeeNumber,EmployeeName,PositionTitle,Department,Start/EndDate,JonRichards,SeniorEngineer,Engineering,03/01/2023`,
  );

  await drawer.getByTestId("drilldown-drawer-close").click();

  await page
    .getByTestId("table-cell-All Groups: Terms / Transfers Out-4")
    .locator("button")
    .click();

  await expect(page.getByText("Jon Richards")).toBeVisible();

  drawer = page.getByTestId("drawer");

  const drilldownTerminationTable = drawer.locator("table").first();

  await expect(
    drilldownTerminationTable.getByTestId("table-header-row"),
  ).toHaveText(["EMPLOYEEPOSITIONDEPARTMENTEND DATE"]);

  const drilldownTerminationRows =
    drilldownTerminationTable.locator("tbody tr");

  const expectedDrilldownTerminationRows = [
    ["Jon Richards", "Senior EngineerFull Time", "Engineering", "04/01/2023"],
  ];

  await compareTableData(
    drilldownTerminationRows,
    expectedDrilldownTerminationRows,
  );

  await compareDownload(
    page,
    "drilldown-drawer-export",
    `EmployeeNumber,EmployeeName,PositionTitle,Department,Start/EndDate,JonRichards,SeniorEngineer,Engineering,04/01/2023`,
  );

  await drawer.getByTestId("drilldown-drawer-close").click();

  await page
    .getByTestId("table-cell-All Groups: Month End-4")
    .locator("button")
    .click();

  await expect(page.getByText("Emma Lee")).toBeVisible();

  drawer = page.getByTestId("drawer");

  const drilldownEndingTable = drawer.locator("table").first();

  await expect(drilldownEndingTable.getByTestId("table-header-row")).toHaveText(
    ["EMPLOYEEPOSITIONDEPARTMENT"],
  );

  const drilldownEndingRows = drilldownEndingTable.locator("tbody tr");

  const expectedDrilldownEndingRows = [
    ["Emma Lee", "Finance ManagerFull Time", "Finance"],
  ];

  await compareTableData(drilldownEndingRows, expectedDrilldownEndingRows);

  await compareDownload(
    page,
    "drilldown-drawer-export",
    `EmployeeNumber,EmployeeName,PositionTitle,Department,Start/EndDate,EmmaLee,FinanceManager,Finance,`,
  );

  await page.getByText("Finance Manager").click();

  await page.waitForURL(
    new RegExp(
      /\/positions\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/,
    ),
  );
});
