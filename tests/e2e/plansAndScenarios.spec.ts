import { expect, test } from "@playwright/test";
import login from "../utils/login";
import { v4 } from "uuid";

test.describe.configure({ mode: "parallel" });

test("Create Plan", async ({ page }) => {
  await login(page, "test+growth@getparallel.com");

  await page.locator('a[href="/plans-and-scenarios"]').first().click();

  /**
   * Create plan
   */
  const uuid = v4();
  const planInput = page.getByTestId("plan-name-input");
  const planName = `Test Plan ${uuid}`;
  await planInput.fill(planName);
  await page.getByTestId("create-plan-button").click();

  const planHistoryList = await page.waitForSelector(
    'ul[data-testid="plan-history-list"]',
  );

  await expect(page.getByText(planName)).toBeVisible();

  const firstLiChild = await planHistoryList.$("li");
  const textContent = await firstLiChild?.textContent();

  const testContent = `${planName} (Created: 09/05/2023)`;
  expect(textContent).toBe(testContent);
});
