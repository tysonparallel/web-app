import { expect, test } from "@playwright/test";
import login from "../utils/login";
import compareTableData from "../utils/compareTableData";
import date from "../utils/date";
import { addMonths, format, startOfMonth } from "date-fns";

test.describe.configure({ mode: "parallel" });

test("Headcount Page", async ({ page }) => {
  await login(page, "test+growth@getparallel.com");

  await page.goto(`/headcount`);

  page.getByText("Headcount Report");

  // Wait for table to load
  await page.locator(".table-row-data").first().waitFor();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "",
    "ID",
    "Employee",
    "Job Title",
    "Pay Rate",
    "Department",
    "Hire Date",
    "",
  ]);

  await expect(page.getByText("Emma Lee")).toBeVisible();

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    [
      "",
      "-",
      "Emma Lee",
      "Employee #1",
      "$120,000/yr",
      "Finance",
      format(startOfMonth(addMonths(date().toDate(), -4)), "MM/dd/yyyy"),
      "",
    ],
    [
      "",
      "-",
      "Renato Villanueva",
      "Employee #3",
      "$120,000/yr",
      "Engineering",
      format(startOfMonth(addMonths(date().toDate(), 2)), "MM/dd/yyyy"),
      "",
    ],
  ];

  await compareTableData(rows, expectedData);
});

test("Headcount Page - Filtering", async ({ page }) => {
  await login(page, "demo@getparallel.com");

  await page.goto(`/headcount`);

  page.getByText("Headcount Report");

  // Wait for table to load
  await page.locator(".table-row-data").first().waitFor();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "",
    "ID",
    "Employee",
    "Job Title",
    "Pay Rate",
    "Department",
    "Hire Date",
    "",
  ]);

  await expect(page.getByText("Dallon Khristensen")).toBeVisible();

  const employmentTypeFilter = await page.getByRole("button", {
    name: "All Employment Types",
  });

  await employmentTypeFilter.click();

  await page.getByTestId("fulltime-checkbox").click();
  await page.getByTestId("parttime-checkbox").click();
  await page.getByTestId("filter-save").click();

  await expect(page.getByText("Trent Walsh")).not.toBeVisible();
  await expect(page.getByText("Michael Romney")).toBeVisible();

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    ["", "75", "Dallon Khristensen", "-", "-", "-", "08/07/2022", "Assign"],
    ["", "66", "Antonio Kruz", "-", "-", "-", "12/05/2022", "Assign"],
    [
      "",
      "91",
      "Michael Romney",
      "Benefits Administrator",
      "CA$74,000/yr",
      "Human Resources",
      "10/07/2022",
      "",
    ],
    [
      "",
      "78",
      "Trey Pickard",
      "Software Implementation Specialist",
      "$95,000/yr",
      "Customer Success",
      "11/07/2022",
      "",
    ],
  ];

  await compareTableData(rows, expectedData);
});
