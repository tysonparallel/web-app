import { expect, test } from "@playwright/test";
import dayjs from "dayjs";
import quarterOfYear from "dayjs/plugin/quarterOfYear.js";
import login from "../utils/login";
import assertBudgetCard from "../utils/assertBudgetCard";
import date from "../utils/date";
dayjs.extend(quarterOfYear);

test.describe.configure({ mode: "parallel" });

test("Dashboard - Growth persona", async ({ page }) => {
  await login(page, "test+growth@getparallel.com");

  await page.goto(`/dashboard`);

  const headlineIntro = page.getByTestId("headline-intro");
  await expect(headlineIntro).toHaveText("Welcome Tyler");

  const headlineDate = page.getByTestId("headline-current-date");
  await expect(headlineDate).toHaveText(`${date().format("MMMM YYYY")}`);

  const headcountSummaryTotal = page.getByTestId("headcount-summary-total");
  await expect(headcountSummaryTotal).toHaveText("1");

  const headcountSummaryTotalChange = page.getByTestId(
    "headcount-summary-beginning-of-period",
  );
  await expect(headcountSummaryTotalChange).toHaveText("1");

  const headcountSummaryHiresDuringPeriod = page.getByTestId(
    "headcount-summary-hires-during-period",
  );
  await expect(headcountSummaryHiresDuringPeriod).toHaveText("0");

  const headcountSummaryTerminations = page.getByTestId(
    "headcount-summary-terminations",
  );
  await expect(headcountSummaryTerminations).toHaveText("0");

  const headcountSummaryTotalMath = page.getByTestId(
    "headcount-summary-total-math",
  );
  await expect(headcountSummaryTotalMath).toHaveText("1");

  await assertBudgetCard(page, {
    total: "-",
    planTitle: `${date().format("MMMM")} Salaries & Expense Models`,
    planValue: "-",
    actualValue: "$10,000",
    varianceValue: "-",
    budgetTag: "",
  });

  const futureHeadcountPlannedHires = page.getByTestId(
    "future-headcount-planned-hires",
  );
  await expect(futureHeadcountPlannedHires).toHaveText("0");

  const tasksCardState = page.getByTestId("tasks-card-state");
  await expect(tasksCardState).toHaveText(
    "Backfills1Department Leader Requests0Outdated Forecasts0Potential Transfers0New Employees1No Outstanding Tasks",
  );
});

test("Dashboard - Under budget with plan", async ({ page }) => {
  await login(page, "test+underbudget@getparallel.com");

  await page.goto(`/dashboard`);

  await page.getByTestId("select-plan-button").click();
  await page
    .getByTestId("select-plan-option-bdb84768-c3ce-4515-936b-9b2049835d7d")
    .click();

  await assertBudgetCard(page, {
    total: "$10,000",
    planTitle: `${date().format("MMMM")} Salaries & Expense Models`,
    planValue: "$20,000",
    actualValue: "$10,000",
    varianceValue: "$10,000",
    budgetTag: "Within Budget",
  });

  await page.getByTestId("select-date-range").click();

  await page.getByTestId("select-date-range-option-quarter").click();

  await assertBudgetCard(page, {
    total: "$30,000",
    planTitle: `Quarter ${date().quarter()} Salaries & Expense Models`,
    planValue: "$60,000",
    actualValue: "$30,000",
    varianceValue: "$30,000",
    budgetTag: "Within Budget",
  });

  await page.getByTestId("select-date-range").click();

  await page.getByTestId("select-date-range-option-year").click();

  await assertBudgetCard(page, {
    total: "$89,667",
    planTitle: `${date().year()} Salaries & Expense Models`,
    planValue: "$210,000",
    actualValue: "$120,333",
    varianceValue: "$89,667",
    budgetTag: "Within Budget",
  });
});

test("Dashboard - Analytics card", async ({ page }) => {
  await login(page, "test+underbudget@getparallel.com");

  await page.goto(`/dashboard`);

  await page.getByTestId("analytics-card-wrapper").scrollIntoViewIfNeeded();

  const verifyTooltip = async (
    month: string,
    plan: string,
    actual: string,
    variance: string,
  ) => {
    const monthLabel = page
      .getByTestId("analytics-card-wrapper")
      .getByText(month);
    await monthLabel.isVisible();
    await monthLabel.hover();
    const boundingBox = await monthLabel.boundingBox();
    await page.mouse.move(boundingBox.x, boundingBox.y - 50);
    return expect(
      page.getByTestId("analytics-card-tooltip-plan-label"),
    ).toHaveText("Plan");
  };

  await verifyTooltip("Jan", "-", "$0", "-");
  await verifyTooltip("Feb", "-", "$10,000", "-");
  await verifyTooltip("Mar", "-", "$20,000", "-");
  await verifyTooltip("Apr", "-", "$10,000", "-");
  await verifyTooltip("Mar", "-", "$10,000", "-");
  await verifyTooltip("Jun", "-", "$10,000", "-");
  await verifyTooltip("Jul", "-", "$10,000", "-");
  await verifyTooltip("Aug", "-", "$10,000", "-");
  await verifyTooltip("Sep", "-", "$10,000", "-");
  await verifyTooltip("Oct", "-", "$10,000", "-");
  await verifyTooltip("Nov", "-", "$10,000", "-");
  await verifyTooltip("Dec", "-", "$10,000", "-");

  await page.getByTestId("select-plan-button").click();
  await page.keyboard.press("ArrowDown");
  await page.keyboard.press("Enter");

  await verifyTooltip("Jan", "$0", "$0", "$0");
  await verifyTooltip("Feb", "$20,000", "$20,000", "$0");
  await verifyTooltip("Mar", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Apr", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Mar", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Jun", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Jul", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Aug", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Sep", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Oct", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Nov", "$20,000", "$10,000", "$10,000");
  await verifyTooltip("Dec", "$20,000", "$10,000", "$10,000");
});
