import { expect, test } from "@playwright/test";
import * as fs from "fs";
import login from "../utils/login";
import compareDownload from "../utils/compareDownload";
import fillDate from "../utils/fillDate";

test.describe.configure({ mode: "parallel" });

test("Export Current Headcount data", async ({ page }) => {
  await login(page, "test+exportdata@getparallel.com");
  await page.getByTestId("sidemenu-headcount").click();
  await expect(page.getByText("Emma Lee")).toBeVisible();

  compareDownload(
    page,
    "download-current-csv",
    `EmployeeID,EmployeeName,JobTitle,ManagerName,Department,EmploymentType,PaymentUnit,
    Compensation,Currency,ExpectedWeeklyHours,Bonus,Commission,Attainment,Location,HireDate,
    YujiItadori,HandsThrower,,Finance,FULL_TIME,HOURLY,42,USD,,,,,,01/01/2023,EmmaLee,FinanceManager,
    ,Finance,FULL_TIME,SALARY,120000,USD,,,,,,02/01/2023,ThomasEdison,Analyst,,Finance,
    FULL_TIME,SALARY,60000,USD,,,12000,0.75,,09/01/2023`,
  );
});

test("Export Forecasted Headcount data", async ({ page }) => {
  await login(page, "test+exportdata@getparallel.com");
  await page.getByTestId("sidemenu-headcount").click();
  await expect(page.getByText("Emma Lee")).toBeVisible();

  await page.getByTestId("segment-forecast").click();

  await expect(page.getByText("Distinguished Engineer")).toBeVisible();

  await compareDownload(
    page,
    "download-forecast-csv",
    `EmployeeID,EmployeeName,JobTitle,ManagerName,Department,EmploymentType,PaymentUnit,
    Compensation,Currency,ExpectedWeeklyHours,Bonus,Commission,Attainment,Location,HireDate,
    ,DistinguishedEngineer,,Web Development,FULL_TIME,SALARY,336000,USD,,10000,,,,10/01`,
  );
});

test("Export Itemized Variance page data", async ({ page }) => {
  await login(page, "test+exportdata@getparallel.com");
  await page.goto(`/dashboard`);

  await page.getByTestId("view-variances-link").click();

  await page.waitForURL(/\/variances\?planUuid=[a-fA-F0-9-]+&mode=month/);
  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await compareDownload(
    page,
    "download-itemized-csv",
    `\"ID Number\",\"Name\",\"Job Title\",\"Department\",\"Modification\",\"Effective Date\",\"Impact\"
  \"\",\"Yuji Itadori\",\"Hands Thrower\",\"Finance\",\"Salary Change\",\"01/01/23\",\"-7280\"
  \"\",\"\",\"Interim CFO\",\"Finance\",\"Salary Change\",\"08/01/23\",\"10000\"
  \"\",\"Thomas Edison\",\"Analyst\",\"Finance\",\"Commission Change\",\"09/01/23\",\"-750\"
  \"\",\"Thomas Edison\",\"Analyst\",\"Finance\",\"Salary Change\",\"09/01/23\",\"-5000\"
  \"\",\"Burden\",\"\",\"Finance\",\"Headcount Change\",\"09/30/23\",\"-606\"
  \"\",\"\",\"Senior Engineer\",\"Engineering\",\"Salary Change\",\"04/01/23\",\"10000\"
  \"\",\"Burden\",\"\",\"Engineering\",\"Headcount Change\",\"09/30/23\",\"2000\"
  \"Total\",\"\",\"\",\"Total\",\"\",\"\",\"8364\"`,
  );

  await page.getByTestId("select-date-range-selection").click();

  await page.getByTestId("select-date-range-option-quarter").click();

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await compareDownload(
    page,
    "download-itemized-csv",
    `\"ID Number\",\"Name\",\"Job Title\",\"Department\",\"Modification\",\"Effective Date\",\"Impact\"
  \"\",\"Yuji Itadori\",\"Hands Thrower\",\"Finance\",\"Salary Change\",\"01/01/23\",\"-21840\"
  \"\",\"Burden\",\"\",\"Finance\",\"Headcount Change\",\"07/31/23\",\"-1518\"
  \"\",\"\",\"Interim CFO\",\"Finance\",\"Salary Change\",\"08/01/23\",\"20000\"
  \"\",\"Thomas Edison\",\"Analyst\",\"Finance\",\"Commission Change\",\"09/01/23\",\"-750\"
  \"\",\"Thomas Edison\",\"Analyst\",\"Finance\",\"Salary Change\",\"09/01/23\",\"-5000\"
  \"\",\"Swag\",\"\",\"Finance\",\"Headcount Change\",\"09/30/23\",\"-15\"
  \"\",\"\",\"Senior Engineer\",\"Engineering\",\"Salary Change\",\"04/01/23\",\"30000\"
  \"\",\"Burden\",\"\",\"Engineering\",\"Headcount Change\",\"07/31/23\",\"6000\"
  \"\",\"Swag\",\"\",\"Engineering\",\"Headcount Change\",\"08/31/23\",\"30\"
  \"Total\",\"\",\"\",\"Total\",\"\",\"\",\"26907\"`,
  );

  await page.getByTestId("select-date-range-selection").click();

  await page.getByTestId("select-date-range-option-year").click();

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await compareDownload(
    page,
    "download-itemized-csv",
    `IDNumber,Name,JobTitle,Department,Modification,EffectiveDate,Impact,YujiItadori,HandsThrower,Finance,
    SalaryChange,01/01/23,-87360,Burden,,Finance,HeadcountChange,05/31/23,-12248,,InterimCFO,Finance,SalaryChange,
    08/01/23,20323,ThomasEdison,Analyst,Finance,CommissionChange,09/01/23,-3000,ThomasEdison,Analyst,Finance,
    SalaryChange,09/01/23,-20000,Swag,,Finance,HeadcountChange,09/30/23,-105,,SeniorEngineer,Engineering,SalaryChange,
    04/01/23,89667,Burden,,Engineering,HeadcountChange,05/31/23,-1300,Swag,,Engineering,HeadcountChange,
    08/31/23,30,,DistinguishedEngineer,Engineering,BonusChange,10/01/23,-2500,,DistinguishedEngineer,Engineering,
    SalaryChange,10/01/23,-84000Total,,,Total,,,-100494`,
  );
});

test("Export Department Variance page data", async ({ page }) => {
  await login(page, "test+exportdata@getparallel.com");
  await page.goto(`/dashboard`);

  await page.getByTestId("view-variances-link").click();

  await page.waitForURL(/\/variances\?planUuid=[a-fA-F0-9-]+&mode=month/);
  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await page.getByTestId("segment-department").click();

  await compareDownload(
    page,
    "download-department-csv",
    `\"Department\",\"Modifications\",\"Total Impact\"
    \"Finance\",\"4\",\"-3636\"
    \"Engineering\",\"2\",\"12000\"
    \"Total\",\"6\",\"8364\"`,
  );

  await page.getByTestId("select-date-range-selection").click();

  await page.getByTestId("select-date-range-option-quarter").click();

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await compareDownload(
    page,
    "download-department-csv",
    `\"Department\",\"Modifications\",\"Total Impact\"
    \"Finance\",\"5\",\"-9123\"
    \"Engineering\",\"3\",\"36030\"
    \"Total\",\"8\",\"26907\"`,
  );

  await page.getByTestId("select-date-range-selection").click();

  await page.getByTestId("select-date-range-option-year").click();

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await compareDownload(
    page,
    "download-department-csv",
    `Department,Modifications,TotalImpactFinance,5,-102390Engineering,4,1897Total,9,-100494`,
  );
});

test("Export Salary drilldown data", async ({ page }) => {
  await login(page, "test+exportdata@getparallel.com");
  await page.goto(`/forecast`);

  await expect(page.getByText("Finance").first()).toBeVisible();

  const salaryForecastTable = await page.locator(
    '[data-testid="forecast-header-Salaries"] + div tbody >> tr:nth-child(1) >> td:nth-child(3) >> button',
  );

  await salaryForecastTable.click();

  const salariesDrillDown = await page.getByTestId("drilldown-tables-salaries");

  await expect(salariesDrillDown).toBeVisible();

  await compareDownload(
    page,
    "drilldown-drawer-export",
    `"EmployeeNumber,EmployeeName,PositionTitle,StartDate,EndDate,Calculation,EmmaLee,FinanceManager,
    02/01/2023,02/28/2023,=ROUND(120000*1/12*28/28,2),YujiItadori,HandsThrower,02/01/2023,02/28/2023,=ROUND(42*40*52*1/12*28/28,2)"`,
  );
});

test("Sanitize CSV data before exporting", async ({ page }) => {
  await login(page, "test+exportdata@getparallel.com");
  await page.goto(`/headcount?type=forecast`);

  await page.getByTestId("open-create-position-modal").click();
  await expect(page.getByTestId("create-position-modal")).toBeVisible();

  await page.fill('[data-testid="jobTitle-input"]', "=(5+5)");
  const departmentSelect = page.getByTestId("department-select-selection");
  await departmentSelect.click();
  const departmentOption = page.getByText("Finance").first();
  await departmentOption.click();
  await fillDate({ page, id: "effectiveAt", date: "09/05/2023" });
  await page.getByText("Create Position").click();
  await page.getByTestId("paymentAmount-currency-input").fill("120000");
  await page.getByTestId("commission-currency-input").fill("100000");
  await page.getByTestId("attainment-percentage-input").fill("75");
  await page.getByTestId("continue-create-position").click();

  await compareDownload(
    page,
    "download-forecast-csv",
    `EmployeeID,EmployeeName,JobTitle,ManagerName,Department,EmploymentType,
    PaymentUnit,Compensation,Currency,ExpectedWeeklyHours,Bonus,Commission,Attainment,Location,HireDate,,
    '=(5+5),,Finance,FULL_TIME,SALARY,120000,USD,,,100000,0.75,,09/05/2023,,DistinguishedEngineer,,WebDevelopment,
    FULL_TIME,SALARY,336000,USD,,10000,,,,10/01/2023`,
  );
});
