import { expect, test } from "@playwright/test";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc.js";
import login from "../utils/login";
import { format, addDays } from "date-fns";
import date from "../utils/date";
import compareTableData from "../utils/compareTableData";

dayjs.extend(utc);
test.describe.configure({ mode: "parallel" });

test("Invite admin and then revoke the invitation", async ({ page }) => {
  await login(page, "test+growth@getparallel.com");

  await page.goto(`/settings`);

  await page.getByTestId("add-new-user").click();

  await expect(page.getByTestId("add-new-user-modal")).toBeVisible();

  await page.getByLabel("Name").fill("Ryan Pierce");

  await page.getByLabel("Email").fill("ryan@getparallel.com");

  await page.getByTestId("admin").check();

  await page.getByTestId("attempt-create-new-user").click();

  await expect(page.getByText("Invitation created successfully")).toBeVisible();

  await expect(page.getByTestId("add-new-user-modal")).not.toBeVisible();

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    [
      "Ryan Pierce ryan@getparallel.com",
      "Admin",
      "All",
      "Pending Approval",
      "",
    ],
    [
      "Tyler Slater  (you)test+growth@getparallel.com",
      "Admin",
      "All",
      "Active",
      "",
    ],
  ];

  await compareTableData(rows, expectedData);

  await page.getByTestId("ryan@getparallel.com-options").click();

  await page.getByTestId("ryan@getparallel.com-options-Revoke Access").click();

  await expect(page.getByTestId("revoke-access-modal")).toBeVisible();

  await page.getByTestId("submit-revoke-access").click();

  await expect(page.getByText("Access revoked successfully")).toBeVisible();

  await expect(page.getByTestId("revoke-access-modal")).not.toBeVisible();
});

test("Create invited admin and remove admin's access and give it back", async ({
  page,
}) => {
  await page.goto(
    `/auth/signup?invitationId=${encodeURIComponent(
      "10248614-78cf-4f32-b359-50f1f156b637",
    )}`,
  );

  await expect(page.getByTestId("first-time-login-modal")).toBeVisible();

  await page.getByTestId("continue-to-parallel").click();

  await page.getByTestId("sidemenu-settings").click();

  await expect(page.getByText("Invite Admin")).toBeVisible();

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    [
      "Invite Admin test+inviteadmin@getparallel.com",
      "Admin",
      "All",
      "Active",
      "",
    ],
    ["Ryan Pierce  (you)ryan@getparallel.com", "Admin", "All", "Active", ""],
  ];

  await compareTableData(rows, expectedData);

  await page.getByTestId("test+inviteadmin@getparallel.com-options").click();

  await page
    .getByTestId("test+inviteadmin@getparallel.com-options-Revoke Access")
    .click();

  await expect(page.getByTestId("revoke-access-modal")).toBeVisible();

  await page.getByTestId("submit-revoke-access").click();

  await expect(page.getByText("Access revoked successfully")).toBeVisible();

  await expect(page.getByTestId("revoke-access-modal")).not.toBeVisible();

  await page.getByTestId("test+inviteadmin@getparallel.com-options").click();

  await page
    .getByTestId("test+inviteadmin@getparallel.com-options-Reactivate User")
    .click();

  await expect(page.getByText("User Information")).toBeVisible();

  await page.getByTestId("attempt-create-new-user").click();

  await expect(page.getByText("Pending Approval")).toBeVisible();
});

test.skip("Invite user, edit permissions, and then revoke the invitation", async ({
  page,
}) => {
  await login(page, "test+changehistory@getparallel.com");

  await page.goto(`/settings`);

  await page.getByTestId("add-new-user").click();

  await expect(page.getByTestId("add-new-user-modal")).toBeVisible();

  await page.getByLabel("Name").fill("Invite NewUser");

  await page.getByLabel("Email").fill("test+inviteuser@getparallel.com");

  await page.getByTestId("user").check();

  const departmentSelect = page.getByTestId("department-select-button");

  await expect(departmentSelect).toBeVisible();

  await departmentSelect.click();

  await page
    .locator('[data-testid^="department-select-option-"]')
    .first()
    .click();

  await departmentSelect.click();

  await expect(page.getByTestId("department-select-selection")).toHaveText(
    "Finance",
  );

  await page.getByTestId("attempt-create-new-user").click();

  await expect(page.getByText("Invitation created successfully")).toBeVisible();

  await expect(page.getByTestId("add-new-user-modal")).not.toBeVisible();

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    ["", "User", "Finance", "Pending Approval", ""],
    ["", "Admin", "All", "Active", ""],
  ];

  await compareTableData(rows, expectedData);

  await page.getByTestId("test+inviteuser@getparallel.com-options").click();

  await page.getByTestId("edit-information").click();

  await expect(page.getByTestId("add-new-user-modal")).toBeVisible();

  await expect(page.getByLabel("Name")).toHaveValue("Invite NewUser");

  await expect(page.getByLabel("Email")).toHaveValue(
    "test+inviteuser@getparallel.com",
  );

  await expect(departmentSelect).toBeVisible();

  await departmentSelect.click();

  await page
    .locator('[data-testid^="department-select-option-"]')
    .last()
    .click();

  await departmentSelect.click();

  await expect(page.getByTestId("department-select-selection")).toHaveText(
    "Finance, Engineering",
  );

  await page.getByTestId("attempt-create-new-user").click();

  await expect(
    page.getByText("Permissions updated successfully"),
  ).toBeVisible();

  await page.getByTestId("test+inviteuser@getparallel.com-options").click();

  await page
    .getByTestId("test+inviteuser@getparallel.com-option-Revoke Access")
    .click();

  await expect(page.getByTestId("revoke-access-modal")).toBeVisible();

  await page.getByTestId("submit-revoke-access").click();

  await expect(page.getByText("Access revoked successfully")).toBeVisible();

  await expect(page.getByTestId("revoke-access-modal")).not.toBeVisible();
});

test("Create invited user", async ({ page }) => {
  await page.goto(
    `/auth/signup?invitationId=${encodeURIComponent(
      "b84d9870-8bc2-43ca-a00e-9660e59d6f00",
    )}`,
  );

  await expect(page.getByTestId("first-time-login-modal")).toBeVisible();

  await page.getByTestId("continue-to-parallel").click();

  await page.getByTestId("sidemenu-logout").click();

  await login(page, "test+inviteuser@getparallel.com");

  await page.getByTestId("sidemenu-settings").click();

  await expect(page.getByText("Test Invite User")).toBeVisible();

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    [
      "Invite User  (you)test+inviteuser@getparallel.com",
      "Admin",
      "All",
      "Active",
      "",
    ],
    [
      "Test Invite User test+inviteNewUser@getparallel.com",
      "User",
      "Finance",
      "Active",
      "",
    ],
  ];

  await compareTableData(rows, expectedData);

  await page.getByTestId("test+inviteNewUser@getparallel.com-options").click();

  await page
    .getByTestId("test+inviteNewUser@getparallel.com-options-Revoke Access")
    .click();

  await expect(page.getByTestId("revoke-access-modal")).toBeVisible();

  await page.getByTestId("submit-revoke-access").click();

  await expect(page.getByText("Access revoked successfully")).toBeVisible();

  await expect(page.getByTestId("revoke-access-modal")).not.toBeVisible();
});

test("Edit user permissions", async ({ page }) => {
  await login(page, "test+edituserpermissions@getparallel.com");

  await page.goto(`/settings`);

  await expect(
    page.getByText("test+edituserpermissions@getparallel.com"),
  ).toBeVisible();

  const table = page.getByTestId("org-users-table");
  const rows = table.locator("tbody tr");
  const expectedData = [
    [
      "Edit User Permissions  (you)test+edituserpermissions@getparallel.com",
      "Admin",
      "All",
      "Active",
      "",
    ],
    [
      "Edit Permissions editpermissions@getparallel.com",
      "User",
      "",
      "Active",
      "Edit UserRevoke Access",
    ],
  ];
  compareTableData(rows, expectedData);

  await page.getByTestId("editpermissions@getparallel.com-options").click();
  await page
    .getByTestId("editpermissions@getparallel.com-options-Edit User")
    .click();
  await expect(page.getByTestId("add-new-user-modal")).toBeVisible();
  await page.getByTestId("department-select-button").click();
  await page
    .locator('[data-testid^="department-select-option-"]')
    .nth(0)
    .click();
  await page
    .locator('[data-testid^="department-select-option-"]')
    .nth(1)
    .click();
  await page.getByTestId("department-select-button").click();
  await page.getByTestId("attempt-create-new-user").click();
  await expect(
    page.getByText("Permissions updated successfully"),
  ).toBeVisible();
  await expect(
    page.getByText("test+edituserpermissions@getparallel.com"),
  ).toBeVisible();

  const updatedTable = page.getByTestId("org-users-table");
  const updatedRows = updatedTable.locator("tbody tr");
  const updatedExpectedData = [
    [
      "Edit User Permissions  (you)test+edituserpermissions@getparallel.com",
      "Admin",
      "All",
      "Active",
      "",
    ],
    [
      "Edit Permissions editpermissions@getparallel.com",
      "User",
      "Finance, Engineering",
      "Active",
      "",
    ],
  ];
  compareTableData(updatedRows, updatedExpectedData);
});
