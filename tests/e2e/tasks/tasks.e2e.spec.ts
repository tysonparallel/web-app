import { expect, test } from "@playwright/test";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc.js";
import login from "../../utils/login";
import { format, addDays } from "date-fns";
import date from "../../utils/date";
import fillDate from "../../utils/fillDate";

dayjs.extend(utc);
test.describe.configure({ mode: "parallel" });

test("Unassigned Employee Task", async ({ page }) => {
  await login(page, "demo@getparallel.com");

  await page.goto(`/dashboard`);

  await page.waitForSelector('[data-testid="unassigned-total"]');
  await expect(page.getByTestId("unassigned-total")).toHaveText(
    "New Employees2",
  );

  await page.getByTestId("view-all-tasks-link").click();

  await page.waitForSelector('[data-testid="task-head-explain"]');
  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await page.waitForSelector('[data-testid^="unassigned-employee-metadata-"]');
  await expect(
    page.getByTestId(
      "unassigned-employee-metadata-cbb0ef62-925b-4652-aecd-e0b60e3e706c",
    ),
  ).toHaveText(
    "NameAntonio KruzDepartmentMarketingStart Date07/05/2023Payment TypeHourlyCompensation$14.00",
  );

  await page.getByText("Assign to Position").first().click();

  await page.waitForSelector('[data-testid="assign-employee-modal"]');
  await expect(page.getByTestId("assign-employee-modal")).toBeVisible();

  // test search functionality
  await page.fill('[data-testid="assign-employee-search-input"]', "account");
  await page.waitForSelector('[data-testid^="assign-employee-position-"]');
  let rowCount = await page
    .locator('[data-testid^="assign-employee-position-"]')
    .count();
  expect(rowCount).toBe(1);
  const rowElement = page
    .locator('[data-testid^="assign-employee-position-"]')
    .first();
  const text = await rowElement.textContent();
  expect(text).toContain("Account Executive");
  await page.fill('[data-testid="assign-employee-search-input"]', "");
  rowCount = await page
    .locator('[data-testid^="assign-employee-position-"]')
    .count();
  expect(rowCount).toBe(2);

  await page
    .locator('[data-testid^="assign-employee-position-"]')
    .first()
    .click();

  await page.getByRole("button", { name: "Assign" }).click();

  await page.waitForSelector('[data-testid="validate-nonHris-modal"]');

  await expect(page.getByLabel("Expected Hours Per Week")).toHaveValue("40");
  await page.getByLabel("Commission$").fill("100,000");
  await page.getByTestId("attainment-percentage-input").fill("100");

  await page.locator('[data-testid^="submit-transfer-button"]').first().click();

  await expect(page.getByText("Assigned employee")).toBeVisible();

  page
    .getByTestId(
      "unassigned-employee-metadata-cbb0ef62-925b-4652-aecd-e0b60e3e706c",
    )
    .waitFor({ state: "detached" });

  await page.waitForSelector('[data-testid^="unassigned-employee-metadata-"]');
  await expect(
    page.locator('[data-testid^="unassigned-employee-metadata-"]').first(),
  ).toHaveText(
    "NameDallon KhristensenDepartmentSalesStart Date08/07/2022Payment TypeHourlyCompensation$30.00",
  );

  await page.getByText("Assign to Position").first().click();

  await page.getByText("Generate Position").click();

  await page.waitForSelector('[data-testid="validate-nonHris-modal"]');

  await page.getByLabel("Expected Hours Per Week").fill("40");
  await page.getByLabel("Commission$").fill("100,000");
  await page.getByTestId("attainment-percentage-input").fill("100");

  await page.locator('[data-testid^="submit-transfer-button"]').first().click();

  await expect(page.getByText("New position generated")).toBeVisible();

  // verify that the forecasted initial position for Antonio is gone and replaced with actual
  await page.goto(`/positions/3e5d84c2-d1e6-4d6f-959d-2c2fd9863901`);
  await expect(
    page.getByRole("heading", { name: "No Forecasted Changes" }),
  ).toBeVisible();
});

test("Backfill Task", async ({ page }) => {
  await login(page, "test+tasksmodel@getparallel.com");

  await page.goto(`/dashboard`);

  await expect(page.getByTestId("backfill-total")).toHaveText("Backfills1");

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await expect(page.getByTestId("backfill-task-title")).toHaveText(
    "Open Position: Employee Term",
  );

  await expect(page.getByTestId("task-details-metadata")).toHaveText(
    "TitleSoftware EngineerLast FilledJason VoorheesDepartmentEngineeringPayment TypeSalaryCompensation$125,000",
  );

  await page.getByTestId("backfill-task-button-confirm").click();

  await expect(page.getByTestId("modify-position-modal")).toBeVisible();

  await expect(page.getByLabel("Title")).toHaveValue("Software Engineer");

  await fillDate({ page, id: "effectiveAt", date: "09/10/2024" });

  await expect(page.getByLabel("Note")).toHaveValue("Back-filling position");

  await page.getByTestId("continue-create-position").click();

  await expect(
    page.getByText("Backfill Task Completed Successfully"),
  ).toBeVisible();

  await page.goto(`/headcount?type=forecast`);
  await expect(page.getByText("Software Engineer")).toBeVisible();
});

test("Outdated Position Task", async ({ page }) => {
  await login(page, "test+tasksmodel@getparallel.com");

  await page.goto(`/dashboard`);

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await expect(page.getByTestId("outdatedPosition-task-title")).toHaveText(
    "Forecasted New Role: Outdated",
  );

  await expect(page.getByTestId("outdatedPosition-metadata")).toHaveText(
    "TitleStaff EngineerForecast Date02/01/2023DepartmentEngineeringChange DescriptionInitial Position",
  );

  await page.getByTestId("outdatedPosition-task-button-edit").click();

  await expect(page.getByTestId("modify-position-version-modal")).toBeVisible();

  await expect(page.getByLabel("Title")).toHaveValue("Staff Engineer");

  await expect(page.getByText("This date has passed")).toBeVisible();

  await fillDate({ page, id: "effectiveAt", date: "09/05/2024" });

  await expect(page.getByLabel("Note")).toHaveValue("Initial Position");

  await page.getByTestId("bonus-currency-input").fill("10,000");

  await page.getByTestId("commission-currency-input").fill("10,000");

  await page.getByTestId("attainment-percentage-input").fill("100");

  await page.getByTestId("continue-create-position").click();

  await expect(page.getByText("Task Completed Successfully")).toBeVisible();

  await page.goto(`/headcount?type=forecast`);

  await expect(page.getByText("Staff Engineer")).toBeVisible();

  await page.getByText("Staff Engineer").click();

  await expect(page.getByTestId("current-position-details-display")).toHaveText(
    "Role DetailsViewJob TitleStaff EngineerDepartmentEngineeringManagerEmma LeeCompensationViewEmployment TypeFull TimePay Rate$150,000 / yearPayment TypeSalaryManually TrackedViewBonus$10,000Commission$10,000Attainment100.00%",
  );

  await expect(
    page.locator('[data-testid^="position-version-forecast-card"]'),
  ).toHaveCount(1);
});

test("Outdated Change Task", async ({ page }) => {
  await login(page, "test+tasksmodel@getparallel.com");

  await page.goto("/forecast");

  await expect(
    page.getByText("Incomplete tasks may affect your metrics"),
  ).toBeVisible();

  await page.goto(`/dashboard`);

  await expect(
    page.getByText("Incomplete tasks may affect your metrics"),
  ).toBeVisible();

  await page.getByTestId("view-all-tasks-link").click();

  await page.waitForSelector('[data-testid="task-head-explain"]');
  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await page.waitForSelector('[data-testid="outdatedChange-task-title"]');
  await expect(page.getByTestId("outdatedChange-task-title")).toHaveText(
    "Forecasted Change: Outdated",
  );

  await page.waitForSelector('[data-testid="outdatedChange-metadata"]');
  await expect(page.getByTestId("outdatedChange-metadata")).toHaveText(
    "TitleQuarterstaff EngineerForecast Date08/05/2023DepartmentEngineeringChange DescriptionRaise",
  );

  await page.getByTestId("outdatedChange-task-button-edit").click();

  await page.waitForSelector('[data-testid="modify-position-version-modal"]');
  await expect(page.getByTestId("modify-position-version-modal")).toBeVisible();

  await expect(page.getByLabel("Title")).toHaveValue("Quarterstaff Engineer");

  await expect(page.getByText("This date has passed")).toBeVisible();

  await page.waitForTimeout(500);
  await fillDate({ page, id: "effectiveAt", date: "09/05/2024" });

  await expect(page.getByLabel("Note")).toHaveValue("Raise");

  await page.getByTestId("continue-create-position").click();

  await expect(page.getByText("Task Completed Successfully")).toBeVisible({
    timeout: 5000,
  });
});

test("Outdated Termination Task", async ({ page }) => {
  await login(page, "test+tasksmodel@getparallel.com");

  await page.goto(`/dashboard`);

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await expect(page.getByTestId("outdatedTermination-task-title")).toHaveText(
    "Forecasted Termination: Outdated",
  );

  await expect(page.getByTestId("outdatedTermination-metadata")).toHaveText(
    "TitleDevOps KingForecast Date08/05/2023DepartmentEngineeringChange DescriptionTermination",
  );

  await page.getByTestId("outdatedTermination-task-button-edit").click();

  await expect(page.getByTestId("modify-position-version-modal")).toBeVisible();

  await expect(page.getByLabel("Title")).toHaveValue("DevOps King");

  await expect(page.getByText("This date has passed")).toBeVisible();

  await fillDate({ page, id: "effectiveAt", date: "09/05/2024" });

  await expect(page.getByLabel("Note")).toHaveValue("Termination");

  await page.getByTestId("continue-create-position").click();

  await expect(page.getByText("Task Completed Successfully")).toBeVisible();
});
