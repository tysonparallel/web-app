import { expect, test } from "@playwright/test";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc.js";
import login from "../../utils/login";
import { format, addDays } from "date-fns";
import date from "../../utils/date";
import fillDate from "../../utils/fillDate";

dayjs.extend(utc);
test.describe.configure({ mode: "parallel" });

test("Deactivate Position in Backfill Task", async ({ page }) => {
  await login(page, "test+deletetasks@getparallel.com");

  await page.goto(`/dashboard`);

  await expect(page.getByTestId("backfill-total")).toHaveText("Backfills1");

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await expect(page.getByTestId("backfill-task-title")).toHaveText(
    "Open Position: Employee Term",
  );

  await expect(page.getByTestId("task-details-metadata")).toHaveText(
    "TitleSoftware EngineerLast FilledJason VoorheesDepartmentEngineeringPayment TypeSalaryCompensation$125,000",
  );

  await page
    .locator('[data-testid^="deactivate-backfill-task"]')
    .first()
    .click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await expect(page.getByTestId("block-navigation-modal")).toContainText(
    "This position will be deleted and any impact on the budget will be removed. Are you sure you want to do this?Do not display this warning againCancelConfirm, Deactivate",
  );

  await page.getByTestId("continue-to-next-page-button").click();

  await expect(
    page.getByText("Backfill Task Completed Successfully"),
  ).toBeVisible();
});

test("Delete Position on Outdated Position Task", async ({ page }) => {
  await login(page, "test+deletetasks@getparallel.com");

  await page.goto(`/headcount?type=forecast`);

  await expect(page.getByText("Staff Engineer")).toBeVisible();

  await page.goto(`/dashboard`);

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await expect(page.getByTestId("outdatedPosition-task-title")).toHaveText(
    "Forecasted New Role: Outdated",
  );

  await expect(page.getByTestId("outdatedPosition-metadata")).toHaveText(
    "TitleStaff EngineerForecast Date02/01/2023DepartmentEngineeringChange DescriptionInitial Position",
  );

  await page.getByTestId("outdatedPosition-task-button-delete").click();

  await expect(page.getByTestId("block-navigation-modal")).toContainText(
    "The forecast will be deleted and any impact on the budget will be removed. Are you sure you want to do this?Do not display this warning againCancelConfirm, Delete",
  );

  await page.getByTestId("continue-to-next-page-button").click();

  await expect(page.getByText("Forecast Deleted")).toBeVisible();

  await page.goto(`/headcount?type=forecast`);

  await expect(page.getByText("Staff Engineer")).not.toBeVisible();
});

test("Delete Forecast on Outdated Change Task", async ({ page }) => {
  await login(page, "test+deletetasks@getparallel.com");

  await page.goto(`/headcount`);

  await expect(page.getByText("Quarterstaff Engineer")).toBeVisible();

  await page.getByText("Quarterstaff Engineer").click();

  await expect(page.getByText("08/05/2023")).toBeVisible();

  await page.goto(`/dashboard`);

  await page.getByTestId("view-all-tasks-link").click();

  await page.waitForSelector('[data-testid="task-head-explain"]');
  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await page.waitForSelector('[data-testid="outdatedChange-task-title"]');
  await expect(page.getByTestId("outdatedChange-task-title")).toHaveText(
    "Forecasted Change: Outdated",
  );

  await page.waitForSelector('[data-testid="outdatedChange-metadata"]');
  await expect(page.getByTestId("outdatedChange-metadata")).toHaveText(
    "TitleQuarterstaff EngineerForecast Date08/05/2023DepartmentEngineeringChange DescriptionRaise",
  );

  await page.getByTestId("outdatedChange-task-button-delete").click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await expect(page.getByTestId("block-navigation-modal")).toContainText(
    "The forecast will be deleted and any impact on the budget will be removed. Are you sure you want to do this?Do not display this warning againCancelConfirm, Delete",
  );

  await page.getByTestId("continue-to-next-page-button").click();

  await expect(page.getByText("Forecast Deleted")).toBeVisible();

  await page.goto(`/headcount`);

  await expect(page.getByText("Quarterstaff Engineer")).toBeVisible();

  await page.getByText("Quarterstaff Engineer").click();

  await expect(page.getByText("08/05/2023")).not.toBeVisible();
});

test("Outdated Termination Task", async ({ page }) => {
  await login(page, "test+deletetasks@getparallel.com");

  await page.goto(`/headcount`);

  await expect(page.getByText("DevOps King")).toBeVisible();

  await page.getByText("DevOps King").click();

  await expect(page.getByText("08/05/2023")).toBeVisible();

  await page.goto(`/dashboard`);

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByTestId("task-head-explain")).toHaveText(
    "This is where you will be assigning an employee to a forecasted role, verifying if a position should be backfilled, and keeping forecasted changes up to date.",
  );

  await expect(page.getByTestId("outdatedTermination-task-title")).toHaveText(
    "Forecasted Termination: Outdated",
  );

  await expect(page.getByTestId("outdatedTermination-metadata")).toHaveText(
    "TitleDevOps KingForecast Date08/05/2023DepartmentEngineeringChange DescriptionTermination",
  );

  await page.getByTestId("outdatedTermination-task-button-delete").click();

  await expect(page.getByTestId("block-navigation-modal")).toContainText(
    "The forecasted term and associated backfill will both be deleted. Are you sure you want to continue?Do not display this warning againCancelConfirm, Delete",
  );

  await page.getByTestId("continue-to-next-page-button").click();

  await page.goto(`/headcount`);

  await expect(page.getByText("DevOps King")).toBeVisible();

  await page.getByText("DevOps King").click();

  await expect(page.getByText("Freddy Krueger")).toBeVisible();

  await expect(page.getByText("08/05/2023")).not.toBeVisible();
});
