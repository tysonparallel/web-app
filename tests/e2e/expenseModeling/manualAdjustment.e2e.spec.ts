import { expect, test } from "@playwright/test";
import login from "../../utils/login";
import compareTableData from "../../utils/compareTableData";

test("Expense Modeling - Create Manual Adjustment", async ({ page }) => {
  await login(page, "test+createadjustment@getparallel.com");
  await page.locator('a[href="/expense-models"]').first().click();

  await page.getByRole("button", { name: "Create Model" }).click();

  await page.getByRole("button", { name: "Manual Adjustments" }).click();

  const createAdjustmentButton = await page.getByTestId(
    "save-manual-adjustment-button",
  );

  await createAdjustmentButton.click();

  await expect(page.getByText("Please enter a valid value")).toBeVisible();

  await page
    .getByTestId("manualAdjustment-name-input")
    .fill("Test Manual Adjustment");

  await createAdjustmentButton.click();

  await expect(page.getByText("Manual Adjustment created")).toBeVisible();

  const createdManualAdjustment = await page.getByText(
    "Test Manual Adjustment",
  );

  await expect(createdManualAdjustment).toBeVisible();

  await createdManualAdjustment.click();

  await expect(page.getByTestId("manual-adjustment-table")).toBeVisible();

  await page.getByRole("button", { name: "Edit" }).click();

  await expect(page.getByTestId("Finance-Jan 23-input")).toBeVisible();

  await page.getByTestId("Finance-Jan 23-input").click();

  await page.getByTestId("Finance-Jan 23-input").fill("10,000");

  await page.getByTestId("Finance-Feb 23-input").click();

  await page.getByTestId("Finance-Feb 23-input").fill("10,000");

  await page.getByTestId("Finance-Mar 23-input").click();

  await page.getByTestId("Finance-Mar 23-input").fill("10,000");

  await page.getByRole("button", { name: "Cancel" }).click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await page.getByTestId("cancel-navigate-away-button").click();

  await page.getByTestId("sidemenu-forecast").click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await page.getByTestId("cancel-navigate-away-button").click();

  await page.getByTestId("save-edit-adjustment-button").click();

  await expect(page.getByTestId("save-adjustments-modal")).toBeVisible();

  await expect(page.getByTestId("cost-change-display")).toContainText(
    `CostIncreases (3)$30,000Decreases (0)$0Total Impact (3)($30,000)`,
  );

  await page.getByTestId("save-adjustment-changes-button").click();

  await expect(page.getByText("Please provide a reason")).toBeVisible();

  await page.getByTestId("cancel-saving-adjustments-button").click();

  await expect(page.getByTestId("save-adjustments-modal")).not.toBeVisible();

  await page.getByTestId("Finance-Jan 23-input").click();

  await page.getByTestId("Finance-Jan 23-input").fill("-10,000");

  await page.getByTestId("Finance-Feb 23-input").click();

  await page.getByTestId("Finance-Feb 23-input").fill("-10,000");

  await page.getByTestId("Finance-Mar 23-input").click();

  await page.getByTestId("Finance-Mar 23-input").fill("-10,000");

  await page.getByTestId("save-edit-adjustment-button").click();

  await expect(page.getByTestId("save-adjustments-modal")).toBeVisible();

  await expect(page.getByTestId("cost-change-display")).toContainText(
    `CostIncreases (0)$0Decreases (3)$30,000Total Impact (3)$30,000`,
  );

  await page.getByTestId("cancel-saving-adjustments-button").click();

  await expect(page.getByTestId("save-adjustments-modal")).not.toBeVisible();

  await page.getByTestId("Engineering-Jan 23-input").fill("20,000");

  await page.getByTestId("Engineering-Feb 23-input").fill("20,000");

  await page.getByTestId("Engineering-Mar 23-input").fill("20,000");

  await page.getByTestId("save-edit-adjustment-button").click();

  await expect(page.getByTestId("save-adjustments-modal")).toBeVisible();

  await expect(page.getByTestId("cost-change-display")).toContainText(
    `CostIncreases (3)$60,000Decreases (3)$30,000Total Impact (6)($30,000)`,
  );

  await page.getByTestId("save-adjustment-changes-button").click();

  await page.getByTestId("adjustmentReason-input").fill("Test Reason");

  await page.getByTestId("save-adjustment-changes-button").click();

  await expect(page.getByText("Adjustments applied")).toBeVisible();

  const EngJan23 = await page.getByTestId("Engineering-Jan 23-cell-label");

  await expect(EngJan23).toContainText("$20,000");

  await expect(page.getByTestId("Engineering-Feb 23-cell-label")).toContainText(
    "$20,000",
  );

  await expect(page.getByTestId("Engineering-Mar 23-cell-label")).toContainText(
    "$20,000",
  );

  const tableRows = await page.locator(
    "[data-testid=manual-adjustment-history-table] >> tbody >> tr",
  );

  const expectedData = [
    [
      "($10,000.00)",
      "Test Reason",
      "Finance",
      "Jan 2023",
      "Create Adjustment",
      "09/06/2023",
      "",
    ],
    [
      "($10,000.00)",
      "Test Reason",
      "Finance",
      "Feb 2023",
      "Create Adjustment",
      "09/06/2023",
      "",
    ],
    [
      "($10,000.00)",
      "Test Reason",
      "Finance",
      "Mar 2023",
      "Create Adjustment",
      "09/06/2023",
      "",
    ],
    [
      "$20,000.00",
      "Test Reason",
      "Engineering",
      "Jan 2023",
      "Create Adjustment",
      "09/06/2023",
      "",
    ],
    [
      "$20,000.00",
      "Test Reason",
      "Engineering",
      "Feb 2023",
      "Create Adjustment",
      "09/06/2023",
      "",
    ],
    [
      "$20,000.00",
      "Test Reason",
      "Engineering",
      "Mar 2023",
      "Create Adjustment",
      "09/06/2023",
      "",
    ],
  ];

  await compareTableData(tableRows, expectedData);

  await EngJan23.click();

  const newTableRows = await page.locator(
    "[data-testid=manual-adjustment-history-table] >> tbody >> tr",
  );

  const newExpectedData = [
    [
      "$20,000.00",
      "Test Reason",
      "Engineering",
      "Jan 2023",
      "Create Adjustment",
      "09/06/2023",
      "",
    ],
  ];

  await compareTableData(newTableRows, newExpectedData);

  const adjustmentOption = await page.getByTestId(
    "Engineering-Jan 2023-adjustment-button",
  );

  await expect(adjustmentOption).toBeVisible();

  await adjustmentOption.click();

  const deleteAdjustmentOption = await page.getByTestId(
    "Engineering-Jan 2023-option-delete-button",
  );

  await expect(deleteAdjustmentOption).toBeVisible();

  await deleteAdjustmentOption.click();

  await expect(page.getByText("Adjustment deleted successfully")).toBeVisible();
});

test("Expense Modeling - Check variance from manual adjustment", async ({
  page,
}) => {
  await login(page, "test+adjvariances@getparallel.com");

  await page.locator('a[href="/expense-models"]').first().click();

  await page.getByRole("button", { name: "Create Model" }).click();

  await page.getByRole("button", { name: "Manual Adjustments" }).click();

  const createAdjustmentButton = await page.getByTestId(
    "save-manual-adjustment-button",
  );

  await createAdjustmentButton.click();

  await expect(page.getByText("Please enter a valid value")).toBeVisible();

  await page
    .getByTestId("manualAdjustment-name-input")
    .fill("Test Manual Adjustment");

  await createAdjustmentButton.click();

  await expect(page.getByText("Manual Adjustment created")).toBeVisible();

  const createdManualAdjustment = await page.getByText(
    "Test Manual Adjustment",
  );

  await expect(createdManualAdjustment).toBeVisible();

  await createdManualAdjustment.click();

  await expect(page.getByTestId("manual-adjustment-table")).toBeVisible();

  await page.getByRole("button", { name: "Edit" }).click();

  await expect(page.getByTestId("Finance-Jan 23-input")).toBeVisible();

  await page.getByTestId("Finance-Jan 23-input").fill("-10,000.00");

  await page.getByTestId("Finance-Feb 23-input").fill("-10,000.00");

  await page.getByTestId("Finance-Sep 23-input").fill("-10,000.00");

  await page.getByTestId("Engineering-Jan 23-input").fill("20,000.00");

  await page.getByTestId("Engineering-Feb 23-input").fill("20,000.00");

  await page.getByTestId("Engineering-Sep 23-input").fill("20,000.00");

  await page.getByTestId("save-edit-adjustment-button").click();

  await expect(page.getByTestId("save-adjustments-modal")).toBeVisible();

  await expect(page.getByTestId("cost-change-display")).toContainText(
    `CostIncreases (3)$60,000Decreases (3)$30,000Total Impact (6)($30,000)`,
  );

  await page.getByTestId("adjustmentReason-input").fill("Test Reason");

  await page.getByTestId("save-adjustment-changes-button").click();

  await expect(page.getByText("Adjustments applied")).toBeVisible();

  const EngJan23 = await page.getByTestId("Engineering-Jan 23-cell-label");

  await expect(EngJan23).toContainText("$20,000.00");

  await expect(page.getByTestId("Engineering-Feb 23-cell-label")).toContainText(
    "$20,000.00",
  );

  await expect(page.getByTestId("Engineering-Sep 23-cell-label")).toContainText(
    "$20,000.00",
  );

  await page.getByTestId("sidemenu-dashboard").click();

  await expect(page.getByText("View Variance")).toBeVisible();

  await page.getByTestId("view-variances-link").click();

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  const expectedData = [
    [
      "-",
      "-",
      "Interim CFO",
      "Finance",
      "Salary Change",
      "08/01/23",
      "$10,000",
    ],
    [
      "-",
      "Test Manual Adjustment",
      "-",
      "Finance",
      "Test Reason",
      "09/01/23",
      "$10,000",
    ],
    [
      "-",
      "Thomas Edison",
      "Analyst",
      "Finance",
      "Commission Change",
      "09/01/23",
      "($750)",
    ],
    [
      "-",
      "Thomas Edison",
      "Analyst",
      "Finance",
      "Salary Change",
      "09/01/23",
      "($5,000)",
    ],
    ["-", "Burden", "-", "Finance", "Headcount Change", "09/30/23", "$850"],
    [
      "-",
      "-",
      "Senior Engineer",
      "Engineering",
      "Salary Change",
      "04/01/23",
      "$10,000",
    ],
    [
      "-",
      "Test Manual Adjustment",
      "-",
      "Engineering",
      "Test Reason",
      "09/01/23",
      "($20,000)",
    ],
    [
      "-",
      "Burden",
      "-",
      "Engineering",
      "Headcount Change",
      "09/30/23",
      "$2,000",
    ],
    ["-", "Swag", "-", "Engineering", "Headcount Change", "09/30/23", "$15"],
    ["Total", "", "", "", "", "", "$7,115"],
  ];

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "ID Number",
    "Name",
    "Job Title",
    "Department",
    "Modification",
    "Effective Date",
    "Impact",
  ]);

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  await compareTableData(rows, expectedData);

  await page.getByTestId("sidemenu-plans-and-scenarios").click();

  await page.getByTestId("plan-name-input").fill("Test Plan");

  await page.getByTestId("create-plan-button").click();

  await expect(page.getByText("Plan created successfully")).toBeVisible();

  await page.getByTestId("sidemenu-dashboard").click();

  await expect(page.getByTestId("budget-card-variance-value")).toContainText(
    "-",
  );

  await page.getByTestId("sidemenu-expense-models").click();

  await page.mouse.move(300, 0);

  await page.getByText("Test Manual Adjustment").click();

  await page.getByTestId("Engineering-Sep 2023-adjustment-button").click();

  await page.getByTestId("Engineering-Sep 2023-option-delete-button").click();

  await expect(page.getByText("Adjustment deleted successfully")).toBeVisible();

  await page.getByTestId("Finance-Sep 2023-adjustment-button").click();

  await page.getByTestId("Finance-Sep 2023-option-delete-button").click();

  await expect(page.getByText("Adjustment deleted successfully")).toBeVisible();

  await page.getByTestId("sidemenu-dashboard").click();

  await expect(page.getByTestId("budget-card-variance-value")).toContainText(
    "$10,000",
  );

  await page.getByTestId("view-variances-link").click();

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  const newExpectedData = [
    [
      "-",
      "Test Manual Adjustment",
      "-",
      "Finance",
      "Test Reason",
      "09/01/23",
      "($10,000)",
    ],
    [
      "-",
      "Test Manual Adjustment",
      "-",
      "Engineering",
      "Test Reason",
      "09/01/23",
      "$20,000",
    ],
    ["Total", "", "", "", "", "", "$10,000"],
  ];

  await compareTableData(rows, newExpectedData);

  // test delete
  await page.getByTestId("sidemenu-expense-models").click();
  await page.getByText("Test Manual Adjustment").click();
  await page.getByTestId("manual-adjustment-ellipsis-dropdown").click();
  await page.getByTestId("manual-adjustment-ellipsis-dropdown-Delete").click();
  await page.getByTestId("delete-manual-expense-model-button").click();
  await expect(page).toHaveURL(/expense-model/);
});
