import { expect, test } from "@playwright/test";
import login from "../../utils/login";
import compareTableData from "../../utils/compareTableData";

test.describe.configure({ mode: "parallel" });

test("Expense Modeling - viewing", async ({ page }) => {
  await login(page, "test+default@getparallel.com");
  await page.locator('a[href="/expense-models"]').first().click();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "Name",
    "Current Value",
    "Type",
    "Last Modified",
    "",
  ]);

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  await page.waitForSelector('tbody tr:has-text("Swag")');

  const expectedData = [
    ["Swag", "$15.00", "Fixed, per employee", "06/01/2023", ""],
    ["Burden", "20.00%", "Percentage of salary", "05/01/2023", ""],
  ];

  await compareTableData(rows, expectedData);
});

test("Expense Modeling - create and delete", async ({ page }) => {
  await login(page, "test+expensemodel@getparallel.com");
  await page.locator('a[href="/expense-models"]').first().click();

  await page.getByRole("button", { name: "Create Model" }).click();

  await page.getByRole("button", { name: "Headcount Driven" }).click();

  await page.getByTestId("name-input").fill("Test Expense");

  await page.getByTestId("type-button").click();
  await page.getByTestId("type-option-FIXED_PER_EMPLOYEE").click();

  await page.getByTestId("period-picker-button").click();
  await page.getByTestId("Oct").click();

  await page.getByRole("button", { name: "+ New Segment" }).click();

  await page.getByTestId("segment-name-input").fill("All employees");

  await page.getByTestId("segment-amount-input").fill("$50");

  await page.getByTestId("filter-save").click();

  await page.getByTestId("save-expense-model-button").click();

  await expect(
    page.getByText("Test Expense$50.00Fixed, per employee").first(),
  ).toBeVisible();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "Name",
    "Current Value",
    "Type",
    "Last Modified",
    "",
  ]);

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    ["Test Expense", "$50.00", "Fixed, per employee", "09/05/2023", ""],
    ["Swag", "$15.00", "Fixed, per employee", "06/01/2023", ""],
    ["Burden", "20.00%", "Percentage of salary", "05/01/2023", ""],
  ];

  await compareTableData(rows, expectedData);

  await page.getByText("Test Expense").click();

  await page.getByTestId("delete-expense-model-button").click();

  await expect(page.getByText("Delete Model Confirmation")).toBeVisible();

  await page.getByTestId("continue-to-next-page-button").click();

  await expect(page.getByText("Expense Model Deleted")).toBeVisible();

  await expect(page.getByText("Swag")).toBeVisible();

  const expectedDataAfterDelete = [
    ["Swag", "$15.00", "Fixed, per employee", "06/01/2023", ""],
    ["Burden", "20.00%", "Percentage of salary", "05/01/2023", ""],
  ];

  await compareTableData(rows, expectedDataAfterDelete);
});
