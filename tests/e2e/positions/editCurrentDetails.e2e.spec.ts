import { expect, test } from "@playwright/test";
import { v4 } from "uuid";
import login from "../../utils/login";
import compareTableData from "../../utils/compareTableData";
import { format, addDays } from "date-fns";
import date from "../../utils/date";
import fillDate from "../../utils/fillDate";

test.describe.configure({ mode: "parallel" });

test("Update forecasted position from Current Details", async ({ page }) => {
  await login(page, "test+editcurrentdetails@getparallel.com");

  await page.goto(`/headcount`);

  await expect(page.getByText("Emma Lee")).toBeVisible();

  await page.getByTestId("segment-forecast").click();

  await page.getByText("Distinguished Engineer").click();

  await expect(page.getByTestId("current-position-details-display")).toHaveText(
    "Role DetailsViewJob TitleDistinguished EngineerDepartmentWeb DevelopmentManagerNo manager assignedCompensationViewEmployment TypeFull TimePay Rate$336,000 / yearPayment TypeSalaryManually TrackedViewBonus$10,000Commission-Attainment-",
  );

  await page.getByTestId("edit-forecasted-current-position").click();

  await page.getByTestId("sidemenu-onboarding").click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await page.getByTestId("continue-to-next-page-button").click();

  await page.waitForURL(`/onboarding`);

  await page.goto(`/headcount`);

  await expect(page.getByText("Emma Lee")).toBeVisible();

  await page.getByTestId("segment-forecast").click();

  await page.getByText("Distinguished Engineer").click();

  await expect(page.getByTestId("current-position-details-display")).toHaveText(
    "Role DetailsViewJob TitleDistinguished EngineerDepartmentWeb DevelopmentManagerNo manager assignedCompensationViewEmployment TypeFull TimePay Rate$336,000 / yearPayment TypeSalaryManually TrackedViewBonus$10,000Commission-Attainment-",
  );

  await page.getByTestId("edit-forecasted-current-position").click();

  await page.getByTestId("sidemenu-dashboard").click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await page.getByTestId("cancel-navigate-away-button").click();

  await expect(page.getByTestId("block-navigation-modal")).not.toBeVisible();

  await fillDate({ page, id: "effectiveAt", date: "10/05/2024" });

  await page.getByTestId("department-select-selection").click();

  await page
    .locator('[data-testid^="department-select-option"]')
    .first()
    .click();

  await page.getByLabel("Commission").fill("1000000");

  await page.getByTestId("attainment-percentage-input").fill("85");

  await page.getByTestId("save-modification-to-forecasted-position").click();

  await expect(page.getByTestId("current-position-details-display")).toHaveText(
    "Role DetailsViewJob TitleDistinguished EngineerDepartmentFinanceManagerNo manager assignedCompensationViewEmployment TypeFull TimePay Rate$336,000 / yearPayment TypeSalaryManually TrackedViewBonus$10,000Commission$1,000,000Attainment85.00%",
  );

  await expect(
    page.locator('[data-testid^="position-version-forecast-card"]'),
  ).toHaveCount(1);
});

test("Update actual position from Current Details", async ({ page }) => {
  await login(page, "test+editcurrentdetails@getparallel.com");

  await page.goto(`/headcount`);

  await expect(page.getByText("Emma Lee")).toBeVisible();

  await page.getByText("Emma Lee").click();

  await expect(page.getByTestId("current-position-details-display")).toHaveText(
    "Role DetailsViewJob TitleFinance ManagerDepartmentFinanceManagerNo manager assignedCompensationViewEmployment TypeFull TimePay Rate$120,000 / yearPayment TypeSalaryManually TrackedEditViewBonus-Commission-Attainment-",
  );

  await page.getByTestId("edit-actual-current-position").click();

  await page.getByTestId("sidemenu-onboarding").click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await page.getByTestId("continue-to-next-page-button").click();

  await page.waitForURL(`/onboarding`);

  await page.goto(`/headcount`);

  await expect(page.getByText("Emma Lee")).toBeVisible();

  await page.getByText("Emma Lee").click();

  await expect(page.getByTestId("current-position-details-display")).toHaveText(
    "Role DetailsViewJob TitleFinance ManagerDepartmentFinanceManagerNo manager assignedCompensationViewEmployment TypeFull TimePay Rate$120,000 / yearPayment TypeSalaryManually TrackedEditViewBonus-Commission-Attainment-",
  );

  await page.getByTestId("edit-actual-current-position").click();

  await page.getByTestId("sidemenu-dashboard").click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await page.getByTestId("cancel-navigate-away-button").click();

  await expect(page.getByTestId("block-navigation-modal")).not.toBeVisible();

  await page.getByLabel("Bonus").fill("1000000");

  await page.getByLabel("Commission").fill("1000000");

  await page.getByTestId("attainment-percentage-input").fill("85");

  await page.getByTestId("save-modification-to-actual-position").click();

  await expect(page.getByTestId("current-position-details-display")).toHaveText(
    "Role DetailsViewJob TitleFinance ManagerDepartmentFinanceManagerNo manager assignedCompensationViewEmployment TypeFull TimePay Rate$120,000 / yearPayment TypeSalaryManually TrackedEditViewBonus$1,000,000Commission$1,000,000Attainment85.00%",
  );
});
