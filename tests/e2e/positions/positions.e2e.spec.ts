import { expect, test } from "@playwright/test";
import { v4 } from "uuid";
import login from "../../utils/login";
import compareTableData from "../../utils/compareTableData";
import { format, addDays } from "date-fns";
import date from "../../utils/date";
import fillDate from "../../utils/fillDate";

test.describe.configure({ mode: "parallel" });

test("Create, view, and update new position", async ({ page }) => {
  await login(page, "test+write@getparallel.com");

  await page.goto(`/headcount`);

  await expect(page.getByText("Will Iam")).toBeVisible();

  await page.getByTestId("segment-forecast").click();

  await page.getByTestId("open-create-position-modal").click();

  await expect(page.getByTestId("create-position-modal")).toBeVisible();

  const saveButton = page.getByTestId("continue-create-position");

  await saveButton.click();

  const jobTitleErrorMessage = page.getByTestId("jobTitle-input-error");
  const effectiveAtErrorMessage = page.getByTestId("effectiveAt-error");
  const paymentAmountErrorMessage = page.getByTestId(
    "paymentAmount-currency-input-error",
  );

  await expect(jobTitleErrorMessage).toHaveText("Job title is required");

  await expect(effectiveAtErrorMessage).toHaveText("Start date is required");
  await expect(paymentAmountErrorMessage).toHaveText(
    "Payment amount is required",
  );

  const jobTitleInput = page.getByTestId("jobTitle-input");

  const uuid = v4();
  const title = `Customer Success Manager ${uuid}`;
  await jobTitleInput.fill(title);

  const departmentSelect = page.getByTestId("department-select-selection");
  await departmentSelect.click();

  await page
    .getByTestId(
      "department-select-option-61624fda-54ae-463d-a317-4f8dc715ae4c",
    )
    .click();

  await fillDate({ page, id: "effectiveAt", date: "09/05/2023" });

  await page.getByText("Create Position").click();

  await page.getByTestId("paymentAmount-currency-input").fill("120000");

  await page.getByTestId("commission-currency-input").fill("100000");
  await page.getByTestId("attainment-percentage-input").fill("75");

  await saveButton.click();

  await page.getByTestId("segment-current").click();

  await expect(page.getByText("Employee #1")).toBeVisible();

  await page.getByTestId("segment-forecast").click();

  const positionTitle = page.getByTitle(title);
  await expect(positionTitle).toBeInViewport();

  /**
   * Verify position takes effect within reports
   */
  await page.goto(`/forecast?format=headcount`);

  const employeeHeadcountHeader = page.locator(
    '[data-testid="forecast-header-Headcount"]',
  );
  const employeeHeadcountTable = employeeHeadcountHeader
    .locator("xpath=following-sibling::div//table")
    .first();

  await expect(employeeHeadcountTable).toBeVisible();

  const employeeHeadcountTableRows = employeeHeadcountTable.locator("tbody tr");

  const expectedData = [
    [
      "Department A",
      "0",
      "0",
      "0",
      "0",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
      "1",
    ],
    [
      "Department B",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "1",
      "1",
      "1",
      "1",
    ],
    ["Total", "0", "0", "0", "0", "1", "1", "1", "1", "2", "2", "2", "2"],
  ];

  await compareTableData(employeeHeadcountTableRows, expectedData);

  /**
   * Read position details
   */
  await page.goto(`/headcount?type=forecast`);
  await page.getByTitle(title).click();

  await expect(page).toHaveURL(
    /.*positions\/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/,
  );

  await expect(page.getByTestId("current-position-title")).toHaveText(title);
  await expect(page.getByTestId("current-position-department")).toHaveText(
    "Department B",
  );
  await expect(page.getByTestId("current-position-manager")).toHaveText(
    "No manager assigned",
  );
  await expect(page.getByTestId("current-position-employment-type")).toHaveText(
    "Full Time",
  );
  await expect(
    page.getByTestId("current-position-compensation-rate"),
  ).toHaveText("$120,000 / year");

  await expect(page.getByTestId("current-position-bonus-rate")).toHaveText("-");

  await expect(page.getByTestId("current-position-commission-rate")).toHaveText(
    "$100,000",
  );

  await expect(page.getByTestId("current-position-attainment")).toHaveText(
    "75.00%",
  );

  await expect(page.getByTestId("current-position-payment-unit")).toHaveText(
    "Salary",
  );

  await expect(page.getByText("No Assigned Employee")).toBeVisible();

  const forecasts = page.locator(
    '[data-testid^="position-version-forecast-card"]',
  );

  await expect(forecasts).toHaveCount(1);

  await page.getByTestId("edit-forecasted-current-position").click();

  await page.getByTestId("paymentAmount-currency-input").fill("130000");

  await page.getByTestId("currencyTypeSelect-button").click();
  await page.getByTestId("currencyTypeSelect-option-EUR").click();

  await page.getByTestId("save-modification-to-forecasted-position").click();

  await expect(
    page.getByTestId("edit-forecasted-current-position"),
  ).toBeVisible();

  await page.goto(`/headcount?type=forecast`);

  await page.getByTitle(title).click();

  await expect(
    page.getByTestId("current-position-compensation-rate"),
  ).toHaveText("€130,000 / year");

  const updatedForecasts = await page.locator(
    '[data-testid^="position-version-forecast-card"]',
  );

  await expect(updatedForecasts).toHaveCount(1);
});

test("Create forecasted version for initially imported position", async ({
  page,
}) => {
  await login(page, "test+write@getparallel.com");

  await page.goto(`/headcount`);

  await page
    .getByTestId("go-to-position-c91f7990-9d31-4c76-a99e-266dd5ef812e")
    .click();

  await page.getByText("No Forecasted Changes").isVisible();

  await page
    .getByTestId("dropdown-main-modify-position-details-button")
    .click();

  await expect(page.getByTestId("paymentAmount-currency-input")).toHaveValue(
    "120,000",
  );

  await page.getByTestId("paymentAmount-currency-input").fill("130000.00");

  await page.getByTestId("changeDescription-input").fill("Raise");

  await page.getByTestId("continue-create-position").click();

  const forecasts = page.locator(
    '[data-testid^="position-version-forecast-card"]',
  );

  await expect(forecasts).toHaveCount(1);

  const actuals = page.locator('[data-testid^="position-version-actual-card"]');

  await expect(actuals).toHaveCount(1);

  await expect(
    page.locator('[data-testid^="version-change-description"]').nth(0),
  ).toHaveText("Raise");

  await expect(page.locator('[data-testid^="change-label"]').nth(0)).toHaveText(
    "Compensation",
  );
  await expect(page.locator('[data-testid^="changed-from"]').nth(0)).toHaveText(
    "$120,000",
  );
  await expect(page.locator('[data-testid^="changed-to"]').nth(0)).toHaveText(
    "$130,000",
  );
});

test("Forecast, and delete a termination", async ({ page }) => {
  await login(page, "test+tasksmodel@getparallel.com");

  await page.goto(`/headcount`);

  await page.getByTestId(new RegExp("go-to-position.*")).first().click();

  await page.getByTestId(new RegExp("dropdown-icon.*")).first().click();

  await page.getByText("Forecast Termination").click();

  await expect(page.getByTestId("forecast-termination-modal")).toBeVisible();

  await fillDate({ page, id: "terminationAt", date: "09/05/2023" });

  await page.getByText("Term Employee").click();

  await page.getByTestId("forecast-termination-submit-button").click();

  await expect(
    page.locator('[data-testid^="version-change-description"]').nth(0),
  ).toHaveText("Termination");

  await expect(page.locator('[data-testid^="change-label"]').nth(0)).toHaveText(
    "Status",
  );
  await expect(page.locator('[data-testid^="changed-from"]').nth(0)).toHaveText(
    "Filled",
  );
  await expect(page.locator('[data-testid^="changed-to"]').nth(0)).toHaveText(
    "Closed",
  );

  await expect(page.locator('[data-testid^="change-label"]').nth(1)).toHaveText(
    "Employee",
  );
  await expect(page.locator('[data-testid^="changed-from"]').nth(1)).toHaveText(
    "Emma Lee",
  );
  await expect(page.locator('[data-testid^="changed-to"]').nth(1)).toHaveText(
    "None",
  );

  await page.getByTestId("deactivatePositionDropdown").click();

  await page.getByRole("button", { name: "Delete Forecast" }).click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await page.getByRole("button", { name: "Confirm, Delete Forecasts" }).click();

  await expect(page.getByText("No Forecasted Changes")).toBeVisible();
});

test("Forecast a termination and create a backfill version, then delete", async ({
  page,
}) => {
  await login(page, "test+tasksmodel@getparallel.com");

  await page.goto(`/headcount`);

  await page.getByTestId(new RegExp("go-to-position.*")).nth(1).click();

  await page.getByText("No Forecasted Changes").isVisible();

  await page.getByTestId(new RegExp("dropdown-icon.*")).first().click();

  await page.getByText("Forecast Termination").click();

  await expect(page.getByTestId("forecast-termination-modal")).toBeVisible();

  await fillDate({ page, id: "terminationAt", date: "09/05/2023" });

  await page.getByTestId("backfill-checkbox").check();

  await expect(page.getByTestId("backfill-form")).toBeVisible();

  await fillDate({ page, id: "effectiveAt", date: "09/10/2023" });

  await fillDate({ page, id: "terminationAt", date: "09/15/2023" });

  await expect(page.getByTestId("effectiveAt-error")).toHaveText(
    "Backfill must be after termination date",
  );

  await fillDate({ page, id: "effectiveAt", date: "09/20/2023" });

  await page.getByTestId("forecast-termination-submit-button").click();

  await expect(
    page.locator('[data-testid^="version-change-description"]').nth(1),
  ).toHaveText("Termination");

  await expect(page.locator('[data-testid^="change-label"]').nth(1)).toHaveText(
    "Status",
  );
  await expect(page.locator('[data-testid^="changed-from"]').nth(1)).toHaveText(
    "Filled",
  );
  await expect(page.locator('[data-testid^="changed-to"]').nth(1)).toHaveText(
    "Closed",
  );

  await expect(page.locator('[data-testid^="change-label"]').nth(2)).toHaveText(
    "Employee",
  );
  await expect(page.locator('[data-testid^="changed-from"]').nth(2)).toHaveText(
    "John Doe",
  );
  await expect(page.locator('[data-testid^="changed-to"]').nth(2)).toHaveText(
    "None",
  );

  await expect(
    page.locator('[data-testid^="version-change-description"]').nth(0),
  ).toHaveText("Back-filling position");

  await expect(page.locator('[data-testid^="change-label"]').nth(0)).toHaveText(
    "Status",
  );
  await expect(page.locator('[data-testid^="changed-from"]').nth(0)).toHaveText(
    "Closed",
  );
  await expect(page.locator('[data-testid^="changed-to"]').nth(0)).toHaveText(
    "Filled",
  );

  await page.getByTestId("sidemenu-headcount").click();

  await expect(page.getByText("Emma Lee")).toBeVisible();

  await page.getByTestId("segment-forecast").click();

  await page.getByRole("button", { name: "Backfill for: John Doe" }).click();

  await page.getByTestId("deactivatePositionDropdown").nth(1).click();

  await page.getByRole("button", { name: "Delete Forecast" }).click();

  await expect(page.getByTestId("block-navigation-modal")).toBeVisible();

  await page.getByRole("button", { name: "Confirm, Delete Forecasts" }).click();

  await expect(page.getByText("No Forecasted Changes")).toBeVisible();
});

test("Deactivate position", async ({ page }) => {
  await login(page, "test+deactivate@getparallel.com");

  await page.goto(`/headcount?type=forecast`);
  await page.getByText("Software Engineer").click();

  await page.getByText("Deactivate Position").click();

  await expect(page.getByText("Position deactivated")).toBeVisible();
  await expect(page).toHaveURL(
    /.*headcount\?type=forecast&sortBy=hireDate&sortDirection=asc.*/,
  );
  await expect(page.getByText("Software Engineer")).not.toBeVisible();
});
