import { expect, test } from "@playwright/test";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc.js";
import login from "../utils/login";
import { format, addDays } from "date-fns";
import date from "../utils/date";
import compareTableData from "../utils/compareTableData";
import fillDate from "../utils/fillDate";

dayjs.extend(utc);
test.describe.configure({ mode: "parallel" });

test("New Position Requests", async ({ page }) => {
  await login(page, "test+positionrequestuser@getparallel.com", "user");

  await page.goto(`/headcount`);

  await expect(page.getByText("Emma Lee")).toBeVisible();

  await page.getByTestId("segment-forecast").click();

  await page.getByTestId("open-create-position-modal").click();

  await expect(page.getByTestId("create-position-modal")).toBeVisible();

  const saveButton = await page.getByTestId("continue-create-position");

  await saveButton.click();

  const jobTitleErrorMessage = await page.getByTestId("jobTitle-input-error");

  const effectiveAtErrorMessage = await page.getByTestId("effectiveAt-error");
  const paymentAmountErrorMessage = await page.getByTestId(
    "paymentAmount-currency-input-error",
  );

  await expect(jobTitleErrorMessage).toHaveText("Job title is required");
  await expect(effectiveAtErrorMessage).toHaveText("Start date is required");
  await expect(paymentAmountErrorMessage).toHaveText(
    "Payment amount is required",
  );

  const jobTitleInput = page.getByTestId("jobTitle-input");

  await jobTitleInput.fill("Position to Reject");

  await expect(page.getByTestId("department-select-selection")).toHaveText(
    "Finance",
  );

  await fillDate({ page, id: "effectiveAt", date: "09/05/2023" });

  await page.getByText("Job Details").click();

  await page.getByTestId("paymentAmount-currency-input").fill("120000.00");

  await page.getByTestId("commission-currency-input").fill("10000.00");
  await page.getByTestId("attainment-percentage-input").fill("75");

  await page.getByTestId("employmentTypeSelect-button").click();
  await page.getByTestId("employmentTypeSelect-option-FULL_TIME").click();

  await page
    .getByTestId("changeDescription-input")
    .fill("I need this position real bad");

  await saveButton.click();

  await expect(page.getByTestId("create-position-modal")).not.toBeVisible();

  await page.getByTestId("open-create-position-modal").click();

  await expect(page.getByTestId("create-position-modal")).toBeVisible();

  await jobTitleInput.fill("Position to Approve");

  await expect(page.getByTestId("department-select-selection")).toHaveText(
    "Finance",
  );

  await fillDate({ page, id: "effectiveAt", date: "09/05/2023" });

  await page.getByText("Job Details").click();

  await page.getByTestId("paymentAmount-currency-input").fill("130000.00");

  await page.getByTestId("commission-currency-input").fill("10000.00");

  await page.getByTestId("attainment-percentage-input").fill("75");

  await page.getByTestId("employmentTypeSelect-button").click();

  await page.getByTestId("employmentTypeSelect-option-FULL_TIME").click();

  await page
    .getByTestId("changeDescription-input")
    .fill("I need this position real bad");

  await saveButton.click();

  await expect(page.getByTestId("create-position-modal")).not.toBeVisible();

  await page.getByTestId("open-create-position-modal").click();

  await expect(page.getByTestId("create-position-modal")).toBeVisible();

  await jobTitleInput.fill("Position to Withdraw");

  await expect(page.getByTestId("department-select-selection")).toHaveText(
    "Finance",
  );

  await fillDate({ page, id: "effectiveAt", date: "09/05/2023" });

  await page.getByText("Job Details").click();

  await page.getByTestId("paymentAmount-currency-input").fill("210000.00");

  await page.getByTestId("commission-currency-input").fill("12000.00");

  await page.getByTestId("attainment-percentage-input").fill("85");

  await page.getByTestId("employmentTypeSelect-button").click();

  await page.getByTestId("employmentTypeSelect-option-FULL_TIME").click();

  await page
    .getByTestId("changeDescription-input")
    .fill("I don't need this position real bad");

  await saveButton.click();

  await expect(page.getByTestId("create-position-modal")).not.toBeVisible();

  await page.getByTestId("sidemenu-dashboard").click();

  await expect(page.getByTestId("position-request-total")).toHaveText(
    "Position Requests5",
  );

  await page.getByTestId("view-all-tasks-link").click();

  await expect(
    page.locator('[data-testid^="position-request-metadata-"]').nth(0),
  ).toHaveText(
    "TitleApproval SpecialistCompensation$65,000DepartmentFinanceManagerEmma LeeStart Date05/01/2024",
  );

  await expect(
    page.locator('[data-testid^="position-request-metadata-"]').nth(1),
  ).toHaveText(
    "TitleRejection SpecialistCompensation$65,000DepartmentFinanceManagerEmma LeeStart Date05/01/2024",
  );

  await expect(
    page.locator('[data-testid^="position-request-metadata-"]').nth(2),
  ).toHaveText(
    "TitlePosition to RejectCompensation$120,000DepartmentFinanceStart Date09/05/2023",
  );

  await expect(
    page.locator('[data-testid^="position-request-metadata-"]').nth(3),
  ).toHaveText(
    "TitlePosition to ApproveCompensation$130,000DepartmentFinanceStart Date09/05/2023",
  );

  await expect(
    page.locator('[data-testid^="position-request-metadata-"]').nth(4),
  ).toHaveText(
    "TitlePosition to WithdrawCompensation$210,000DepartmentFinanceStart Date09/05/2023",
  );

  await page
    .locator('[data-testid^="dismiss-position-request-"]')
    .nth(0)
    .click();

  await expect(page.getByText("Request Withdrawn Successfully")).toBeVisible();

  await page
    .locator('[data-testid^="view-reject-position-request-"]')
    .nth(0)
    .click();

  await expect(page.getByTestId("reject-request-modal")).toBeVisible();

  await page.getByTestId("dismiss-rejected-position-request").click();

  await expect(page.getByTestId("reject-request-modal")).not.toBeVisible();

  await expect(
    page.getByText("Request Withdrawn Successfully").first(),
  ).toBeVisible();

  await page
    .locator('[data-testid^="withdraw-position-request-"]')
    .nth(2)
    .click();

  await expect(page.getByTestId("withdraw-request-modal")).toBeVisible();

  await page.getByTestId("submit-withdraw-request").click();

  await expect(page.getByTestId("withdraw-request-modal")).not.toBeVisible();

  await expect(
    page.getByText("Request Withdrawn Successfully").first(),
  ).toBeVisible();

  await page.getByTestId("sidemenu-logout").click();

  await login(page, "test+positionrequest@getparallel.com");

  await page.goto("/dashboard");

  await expect(page.getByTestId("department-leader-requests-total")).toHaveText(
    "Department Leader Requests3",
  );

  await page.getByTestId("view-all-tasks-link").click();

  await expect(
    page.locator('[data-testid^="position-request-metadata-"]').nth(0),
  ).toHaveText(
    "TitlePosition to RejectCompensation$120,000DepartmentFinanceStart Date09/05/2023",
  );

  await expect(
    page.locator('[data-testid^="position-request-metadata-"]').nth(1),
  ).toHaveText(
    "TitlePosition to ApproveCompensation$130,000DepartmentFinanceStart Date09/05/2023",
  );

  await expect(
    page.locator('[data-testid^="position-request-metadata-"]').nth(2),
  ).toHaveText(
    "TitlePosition to WithdrawCompensation$210,000DepartmentFinanceStart Date09/05/2023",
  );

  await page
    .locator('[data-testid^="dismiss-position-request-"]')
    .nth(0)
    .click();

  await expect(page.getByText("Request Dismissed Successfully")).toBeVisible();

  await page
    .getByText(
      "TitlePosition to WithdrawCompensation$210,000DepartmentFinanceStart Date09/05/2023",
    )
    .waitFor({ state: "detached" });

  await page.locator('[data-testid^="view-position-request-"]').nth(0).click();

  await expect(page.getByTestId("view-position-request-modal")).toBeVisible();

  await expect(page.getByTestId("view-position-request-modal")).toHaveText(
    'Requested byPosition Request UserDate09/05/2023StatusPENDINGRole DetailsJob TitlePosition to RejectDepartmentFinanceHire Date09/05/2023CompensationEmployment TypeFull TimePayment TypeSalarySalary$120,000DepartmentFinanceCommission$10,000Attainment0.75Number of Positions to Create1Request Reason"I need this position real bad"CancelRejectApprove',
  );

  await page.getByTestId("reject-position-request").click();

  await expect(page.getByTestId("reject-request-modal")).toBeVisible();

  const textarea = await page.getByTestId("rejection-reasoning-textarea");
  await expect(textarea).toBeVisible();
  await textarea.fill("No you don't");

  await page.getByTestId("submit-reject-request").click();

  await expect(page.getByText("Request Rejected Successfully")).toBeVisible();

  await page
    .getByText(
      "TitlePosition to RejectCompensation$120,000DepartmentFinanceStart Date09/05/2023",
    )
    .waitFor({ state: "detached" });

  await page.locator('[data-testid^="view-position-request-"]').click();

  await expect(page.getByTestId("view-position-request-modal")).toBeVisible();

  await expect(page.getByTestId("view-position-request-modal")).toHaveText(
    'Requested byPosition Request UserDate09/05/2023StatusPENDINGRole DetailsJob TitlePosition to ApproveDepartmentFinanceHire Date09/05/2023CompensationEmployment TypeFull TimePayment TypeSalarySalary$130,000DepartmentFinanceCommission$10,000Attainment0.75Number of Positions to Create1Request Reason"I need this position real bad"CancelRejectApprove',
  );

  await page.getByTestId("approve-position-request").click();

  await page.goto("/headcount?type=forecast&sortBy=hireDate&sortDirection=asc");

  await page
    .getByText(
      "TitlePosition to ApproveCompensation$130,000DepartmentFinanceStart Date09/05/2023",
    )
    .waitFor({ state: "detached" });

  await expect(page.getByText("Position to Approve")).toBeVisible();
});

test("Position Modification Requests", async ({ page }) => {
  await login(page, "test+modificationrequestuser@getparallel.com", "user");

  await page.goto(`/headcount`);

  const approvalRequestPosition = await page.getByText("Jason Voorhees");

  await expect(approvalRequestPosition).toBeVisible();

  await approvalRequestPosition.click();

  const jobTitleInput = page.getByTestId("jobTitle-input");

  await page.getByTestId("request-modification-button").click();

  await expect(page.getByTestId("modify-position-modal")).toBeVisible();

  await expect(jobTitleInput).toHaveValue("Software Engineer");

  await fillDate({ page, id: "effectiveAt", date: "09/05/2023" });

  await page.getByTestId("paymentAmount-currency-input").fill("135000.00");

  await page.getByTestId("changeDescription-input").fill("He needs a raise");

  const saveButton = await page.getByTestId("continue-create-position");

  const futureMod = await page.getByText("Future Position Modification");

  const backToHead = await page.getByTestId("sidemenu-headcount");

  await saveButton.click();

  await expect(futureMod).toBeVisible();

  await backToHead.click();

  await page.getByText("Quarterstaff Engineer").click();

  await page.getByTestId("request-modification-button").click();

  await expect(page.getByTestId("modify-position-modal")).toBeVisible();

  await expect(page.getByLabel("Title")).toHaveValue("Quarterstaff Engineer");

  await fillDate({ page, id: "effectiveAt", date: "09/05/2023" });

  await page.getByTestId("paymentAmount-currency-input").fill("185000.00");

  await page
    .getByTestId("changeDescription-input")
    .fill("He does not needs a big raise");

  await saveButton.click();

  await expect(futureMod).toBeVisible();

  await backToHead.click();

  await page.getByText("DevOps King").click();

  await page.getByTestId("request-modification-button").click();

  await expect(page.getByTestId("modify-position-modal")).toBeVisible();

  await expect(page.getByLabel("Title")).toHaveValue("DevOps King");

  await fillDate({ page, id: "effectiveAt", date: "09/05/2023" });

  await page.getByTestId("paymentAmount-currency-input").fill("125000.00");

  await page
    .getByTestId("changeDescription-input")
    .fill("Oof this is not right");

  await saveButton.click();

  await expect(futureMod).toBeVisible();

  await backToHead.click();

  await page.getByTestId("sidemenu-dashboard").click();

  await expect(page.getByTestId("position-change-request-total")).toHaveText(
    "Position Change Requests5",
  );

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByText("Jason Voorhees").nth(0)).toBeVisible();

  await expect(
    page.locator('[data-testid^="modification-request-metadata-"]').nth(0),
  ).toHaveText(
    "TitleQuarterstaff EngineerEmployeeMichael MyersEffective Date05/01/2024View Changes",
  );

  await expect(
    page.locator('[data-testid^="modification-request-metadata-"]').nth(1),
  ).toHaveText(
    "TitleSoftware EngineerEmployeeJason VoorheesEffective Date05/01/2024View Changes",
  );

  await expect(
    page.locator('[data-testid^="modification-request-metadata-"]').nth(2),
  ).toHaveText(
    "TitleSoftware EngineerEmployeeJason VoorheesEffective Date09/05/2023View Changes",
  );

  await expect(
    page.locator('[data-testid^="modification-request-metadata-"]').nth(3),
  ).toHaveText(
    "TitleQuarterstaff EngineerEmployeeMichael MyersEffective Date09/05/2023View Changes",
  );

  await expect(
    page.locator('[data-testid^="modification-request-metadata-"]').nth(4),
  ).toHaveText(
    "TitleDevOps KingEmployeeFreddy KruegerEffective Date09/05/2023View Changes",
  );

  await page
    .locator('[data-testid^="dismiss-modification-request-"]')
    .nth(0)
    .click();

  await expect(page.getByText("Request Withdrawn Successfully")).toBeVisible();

  await page
    .locator('[data-testid^="view-modification-rejection-"]')
    .nth(0)
    .click();

  await expect(page.getByTestId("reject-request-modal")).toBeVisible();

  await page.getByTestId("dismiss-rejected-position-request").click();

  await expect(page.getByTestId("reject-request-modal")).not.toBeVisible();

  await expect(
    page.getByText("Request Withdrawn Successfully").first(),
  ).toBeVisible();

  await page
    .locator('[data-testid^="withdraw-modification-request-"]')
    .nth(2)
    .click();

  await expect(page.getByTestId("withdraw-request-modal")).toBeVisible();

  await page.getByTestId("submit-withdraw-request").click();

  await expect(page.getByTestId("withdraw-request-modal")).not.toBeVisible();

  await expect(
    page.getByText("Request Withdrawn Successfully").first(),
  ).toBeVisible();

  await page.getByTestId("sidemenu-logout").click();

  await login(page, "test+modificationrequest@getparallel.com");

  await page.goto("/dashboard");

  await expect(page.getByTestId("department-leader-requests-total")).toHaveText(
    "Department Leader Requests3",
  );

  await page.getByTestId("view-all-tasks-link").click();

  await expect(page.getByText("Jason Voorhees")).toBeVisible();
  await expect(page.getByText("Michael Myers")).toBeVisible();
  await expect(page.getByText("Freddy Krueger")).toBeVisible();

  const taskToDismiss = await page
    .locator('[data-testid^="modification-request-metadata-"]')
    .nth(0);
  await expect(taskToDismiss).toHaveText(
    "TitleSoftware EngineerEmployeeJason VoorheesEffective Date09/05/2023View Changes",
  );

  const taskForRejection = page
    .locator('[data-testid^="modification-request-metadata-"]')
    .nth(1);
  await expect(taskForRejection).toHaveText(
    "TitleQuarterstaff EngineerEmployeeMichael MyersEffective Date09/05/2023View Changes",
  );

  await expect(
    page.locator('[data-testid^="modification-request-metadata-"]').nth(2),
  ).toHaveText(
    "TitleDevOps KingEmployeeFreddy KruegerEffective Date09/05/2023View Changes",
  );

  await page
    .locator('[data-testid^="dismiss-modification-request-"]')
    .nth(0)
    .click();

  await expect(page.getByText("Request Dismissed Successfully")).toBeVisible();

  await page
    .getByText(
      "TitleDevOps KingEmployeeFreddy KruegerEffective Date09/05/2023View Changes",
    )
    .waitFor({ state: "detached" });

  await page
    .locator('[data-testid^="view-modification-request-"]')
    .nth(0)
    .click();

  await expect(
    page.getByTestId("view-modification-request-modal"),
  ).toBeVisible();

  await expect(page.getByTestId("view-modification-request-modal")).toHaveText(
    'Requested byModification Request UserDate09/05/2023StatusPENDINGCurrent PositionRole DetailsJob TitleSoftware EngineerManager-DepartmentFinanceHire Date02/01/2023CompensationEmployment TypeFull TimePayment TypeSalarySalary$125,000Bonus-Commission-Attainment-Request ChangesSalary$135,000Change Reason"He needs a raise"CancelRejectApprove',
  );

  await page.getByTestId("submit-reject-modification").click();

  await expect(page.getByTestId("reject-request-modal")).toBeVisible();

  const textarea = page.getByTestId("rejection-reasoning-textarea");
  await expect(textarea).toBeVisible();
  await textarea.fill("No he doesn't");

  await page.getByTestId("submit-reject-request").click();

  await expect(page.getByText("Request Rejected Successfully")).toBeVisible();

  await page
    .getByText(
      "TitleSoftware EngineerEmployeeJason VoorheesEffective Date09/05/2023View Changes",
    )
    .waitFor({ state: "detached" });

  await page.locator('[data-testid^="view-modification-request-"]').click();

  await expect(
    page.getByTestId("view-modification-request-modal"),
  ).toBeVisible();

  await expect(page.getByTestId("view-modification-request-modal")).toHaveText(
    'Requested byModification Request UserDate09/05/2023StatusPENDINGCurrent PositionRole DetailsJob TitleQuarterstaff EngineerManager-DepartmentFinanceHire Date05/01/2023CompensationEmployment TypeFull TimePayment TypeSalarySalary$150,000Bonus-Commission-Attainment-Request ChangesSalary$185,000Change Reason"He does not needs a big raise"CancelRejectApprove',
  );

  await page.getByTestId("submit-approve-modification").click();

  await expect(
    page.getByTestId("view-modification-request-modal"),
  ).not.toBeVisible();

  await expect(page.getByText("Request Approved Successfully")).toBeVisible();

  await page
    .getByText(
      "TitleQuarterstaff EngineerEmployeeMichael MyersEffective Date09/05/2023View Changes",
    )
    .waitFor({ state: "detached" });

  await page.goto("/headcount");
  await page.getByText("Loading...").waitFor({ state: "detached" });

  await page.getByText("Quarterstaff Engineer").click();

  await expect(
    page.locator('[data-testid^="position-version-forecast-card-"]').nth(0),
  ).toHaveText(
    "Compensation09/05/2023Approved requestModification Request on 09/05/2023Compensation$150,000$185,000",
  );
});
