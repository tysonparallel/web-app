import { expect, test } from "@playwright/test";
import login from "../utils/login";
import compareTableData from "../utils/compareTableData";
import assertBudgetCard from "../utils/assertBudgetCard";
import date from "../utils/date";

test.describe.configure({ mode: "parallel" });

test("Variances - within budget", async ({ page }) => {
  await login(page, "test+default@getparallel.com");
  await page.goto(`/dashboard`);

  await page.getByTestId("view-variances-link").click();

  await page.waitForURL(/\/variances\?planUuid=[a-fA-F0-9-]+&mode=month/);
  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await assertBudgetCard(page, {
    total: "$17,115",
    planTitle: `${date().format("MMMM")} Salaries & Expense Models`,
    planValue: "$36,045",
    actualValue: "$18,930",
    varianceValue: "$17,115",
    budgetTag: "Within Budget",
  });

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "ID Number",
    "Name",
    "Job Title",
    "Department",
    "Modification",
    "Effective Date",
    "Impact",
  ]);

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    [
      "-",
      "-",
      "Interim CFO",
      "Finance",
      "Salary Change",
      "08/01/23",
      "$10,000",
    ],
    [
      "-",
      "Thomas Edison",
      "Analyst",
      "Finance",
      "Commission Change",
      "09/01/23",
      "($750)",
    ],
    [
      "-",
      "Thomas Edison",
      "Analyst",
      "Finance",
      "Salary Change",
      "09/01/23",
      "($5,000)",
    ],
    ["-", "Burden", "-", "Finance", "Headcount Change", "09/30/23", "$850"],
    [
      "-",
      "-",
      "Senior Engineer",
      "Engineering",
      "Salary Change",
      "04/01/23",
      "$10,000",
    ],
    [
      "-",
      "Burden",
      "-",
      "Engineering",
      "Headcount Change",
      "09/30/23",
      "$2,000",
    ],
    ["-", "Swag", "-", "Engineering", "Headcount Change", "09/30/23", "$15"],
    ["Total", "", "", "", "", "", "$17,115"],
  ];

  await compareTableData(rows, expectedData);

  await page.getByTestId("segment-department").click();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "Department",
    "Modifications",
    "Total Impact",
  ]);

  const departmentTable = page.locator("table");
  const departmentRows = departmentTable.locator("tbody tr");

  const expectedDepartmentData = [
    ["Finance", "3", "$5,100"],
    ["Engineering", "3", "$12,015"],
    ["Total", "6", "$17,115"],
  ];

  await compareTableData(departmentRows, expectedDepartmentData);
});

test("Variances - department select", async ({ page }) => {
  await login(page, "test+default@getparallel.com");
  await page.goto(`/dashboard`);

  await page.getByTestId("view-variances-link").click();

  await page.waitForURL(/\/variances\?planUuid=[a-fA-F0-9-]+&mode=month/);

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await page
    .getByRole("heading", { name: "Budget", exact: true })
    .waitFor({ state: "visible" });

  await page.getByTestId("select-department-button").click();

  const departmentOptions = page.locator(
    `xpath=//*[starts-with(@data-testid, 'select-department-option-')]`,
  );
  await departmentOptions.locator(`text=Finance`).click();

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await assertBudgetCard(page, {
    total: "$5,100",
    planTitle: `${date().format("MMMM")} Salaries & Expense Models`,
    planValue: "$24,030",
    actualValue: "$18,930",
    varianceValue: "$5,100",
    budgetTag: "Within Budget",
  });

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "ID Number",
    "Name",
    "Job Title",
    "Department",
    "Modification",
    "Effective Date",
    "Impact",
  ]);

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    [
      "-",
      "-",
      "Interim CFO",
      "Finance",
      "Salary Change",
      "08/01/23",
      "$10,000",
    ],
    [
      "-",
      "Thomas Edison",
      "Analyst",
      "Finance",
      "Commission Change",
      "09/01/23",
      "($750)",
    ],
    [
      "-",
      "Thomas Edison",
      "Analyst",
      "Finance",
      "Salary Change",
      "09/01/23",
      "($5,000)",
    ],
    ["-", "Burden", "-", "Finance", "Headcount Change", "09/30/23", "$850"],
    ["Total", "", "", "", "", "", "$5,100"],
  ];

  await compareTableData(rows, expectedData);

  await page.getByTestId("segment-department").click();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "Department",
    "Modifications",
    "Total Impact",
  ]);

  const departmentTable = page.locator("table");
  const departmentRows = departmentTable.locator("tbody tr");

  const expectedDepartmentData = [
    ["Finance", "3", "$5,100"],
    ["Total", "3", "$5,100"],
  ];

  await compareTableData(departmentRows, expectedDepartmentData);
});

test("Variances - over budget", async ({ page }) => {
  await login(page, "test+default@getparallel.com");
  await page.goto(`/dashboard`);

  await page.getByTestId("view-variances-link").click();

  await page.waitForURL(/\/variances\?planUuid=[a-fA-F0-9-]+&mode=month/);

  await page
    .getByRole("heading", { name: "Budget", exact: true })
    .waitFor({ state: "visible" });
  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await page.getByTestId("select-date-range").click();

  await page.getByTestId("select-date-range-option-year").click();

  await page
    .locator(".react-loading-skeleton")
    .nth(0)
    .waitFor({ state: "detached" });

  await assertBudgetCard(page, {
    total: "($1,381)",
    planTitle: `${date().format("YYYY")} Salaries & Expense Models`,
    planValue: "$266,563",
    actualValue: "$267,943",
    varianceValue: "($1,381)",
    budgetTag: "Over Budget",
  });

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "ID Number",
    "Name",
    "Job Title",
    "Department",
    "Modification",
    "Effective Date",
    "Impact",
  ]);

  const table = page.locator("table");
  const rows = table.locator("tbody tr");

  const expectedData = [
    [
      "-",
      "-",
      "Interim CFO",
      "Finance",
      "Salary Change",
      "08/01/23",
      "$20,323",
    ],
    ["-", "Burden", "-", "Finance", "Headcount Change", "08/31/23", "($600)"],
    ["-", "Swag", "-", "Finance", "Headcount Change", "08/31/23", "($30)"],
    [
      "-",
      "Thomas Edison",
      "Analyst",
      "Finance",
      "Commission Change",
      "09/01/23",
      "($3,000)",
    ],
    [
      "-",
      "Thomas Edison",
      "Analyst",
      "Finance",
      "Salary Change",
      "09/01/23",
      "($20,000)",
    ],
    [
      "-",
      "-",
      "Senior Engineer",
      "Engineering",
      "Salary Change",
      "04/01/23",
      "$89,667",
    ],
    [
      "-",
      "Burden",
      "-",
      "Engineering",
      "Headcount Change",
      "05/31/23",
      "($1,300)",
    ],
    ["-", "Swag", "-", "Engineering", "Headcount Change", "06/30/23", "$60"],
    [
      "-",
      "-",
      "Distinguished Engineer",
      "Engineering",
      "Bonus Change",
      "10/01/23",
      "($2,500)",
    ],
    [
      "-",
      "-",
      "Distinguished Engineer",
      "Engineering",
      "Salary Change",
      "10/01/23",
      "($84,000)",
    ],

    ["Total", "", "", "", "", "", "($1,381)"],
  ];

  await compareTableData(rows, expectedData);

  await page.getByTestId("segment-department").click();

  await expect(page.getByTestId("table-header-column")).toHaveText([
    "Department",
    "Modifications",
    "Total Impact",
  ]);

  const departmentTable = page.locator("table");
  const departmentRows = departmentTable.locator("tbody tr");

  const expectedDepartmentData = [
    ["Finance", "4", "($3,307)"],
    ["Engineering", "4", "$1,927"],
    ["Total", "8", "($1,381)"],
  ];

  await compareTableData(departmentRows, expectedDepartmentData);
});
