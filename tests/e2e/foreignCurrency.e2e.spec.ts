import { test } from "@playwright/test";
import dayjs from "dayjs";
import quarterOfYear from "dayjs/plugin/quarterOfYear.js";
import login from "../utils/login";
import assertBudgetCard from "../utils/assertBudgetCard";
import date from "../utils/date";
dayjs.extend(quarterOfYear);

test.describe.configure({ mode: "parallel" });

test("Foreign Currency - Check conversion", async ({ page }) => {
  await login(page, "test+foreigncurrency@getparallel.com");

  await page.goto(`/dashboard`);

  await assertBudgetCard(page, {
    total: "$1,147",
    planTitle: `${date().format("MMMM")} Salaries & Expense Models`,
    planValue: "$36,045",
    actualValue: "$34,898",
    varianceValue: "$1,147",
    budgetTag: "Within Budget",
  });
});
