declare namespace Types {
  interface InputState {
    value: string;
    valid: boolean;
    touched: boolean;
    pristine: boolean;
    validation: RegExp;
    errorMessage: string;
    disabled?: boolean;
  }
  interface Integration {
    uuid: string;
    name: string;
    type: string;
    connectionStatus: "CONNECTED" | "FAILED" | "PENDING";
    isManual: boolean;
    createdAt: string;
    lastSync: string;
  }
  interface Group {
    uuid: string;
    name: string;
    type: string;
  }
  interface Employee {
    uuid: string;
    firstName: string;
    lastName: string;
    groups?: Group[];
  }

  type DataSubject = "headcount" | "expenses";
}

declare module "*.json";
