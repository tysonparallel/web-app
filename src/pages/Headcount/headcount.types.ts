export interface Employee {
  uuid: string;
  firstName: string;
  lastName: string;
  employeeNumber: string | null;
  startDate: string;
  workEmail: string | null;
  terminationDate: string | null;
}

export interface Task {
  type: string;
  organizationUuid: string;
  completedBy: string;
  completedAt: Date;
  metadata: {
    employeeUuid?: string | null;
    positionUuid?: string | null;
    employeeName?: string | null;
    compensationRate?: number | null;
    positionVersionUuid?: string | null;
    title?: string | null;
    employeeStartDate?: Date | null;
    lastFilled?: string | null;
    department?: string | null;
    paymentUnit?: string | null;
    modification?: string | null;
    originalChangeDate?: Date | null;
    manager?: string | null;
    terminationDate?: Date | null;
    effectiveAt?: string | null;
    changeDescription?: string | null;
    expectedWeeklyHours?: number | null;
    employmentType?: string | null;
    startDate?: string | null;
  };
}

export interface ILocation {
  uuid?: string;
  createdAt: string;
  updatedAt: string;
  country: string;
  locationType: string;
  streetOne?: string | null;
  streetTwo?: string | null;
  city?: string | null;
  state?: string | null;
  zipCode?: string | null;
}

export interface ICurrentPosition {
  uuid: string;
  name: string;
  positionUuid: string;
  employeeUuid: string;
  managerUuid: string;
  groupUuid: string;
  employmentType: string;
  compensationRate: number;
  commission: number | null;
  bonus: number | null;
  attainment: number | null;
  expectedWeeklyHours: number | null;
  paymentUnit: string;
  updatedAt: string;
  effectiveAt: string;
  title: string;
  isActive: boolean;
  isActual: boolean;
  currency: string;
  location: ILocation;
  changeDescription?: string | null;
}

export interface Position {
  current: ICurrentPosition;
  currentDepartment?: {
    name: string;
    uuid: string;
  };
  currentEmployee?: Employee;
  currentManager?: Employee;
  auditLog: {
    uuid: string;
    changeDescription: string;
    changedBy: { firstName: string; lastName: string } | null;
    effectiveAt: string;
    changedOn: string;
    changes: {
      label: string;
      changedFrom: number | boolean | string | null;
      changedTo: number | boolean | string | null;
    }[];
    isActual: boolean;
  }[];
  hasFutureChanges: boolean;
  hasOutdatedForecast: boolean;
  history: {
    uuid: string;
    name: string;
    positionUuid: string;
    managerUuid: string;
    groupUuid: string;
    employeeUuid: string;
    employmentType: string;
    compensationRate: number;
    commission: number | null;
    bonus: number | null;
    attainment: number | null;
    expectedWeeklyHours: number;
    paymentUnit: string;
    updatedAt: string;
    effectiveAt: string;
    title: string;
    isActive: boolean;
    isActual: boolean;
    currency: string;
  }[];
}

export interface IPositionHistoryVersion {
  uuid: string;
  changeDescription: string;
  changedBy: {
    firstName: string;
    lastName: string;
  } | null;
  effectiveAt: string;
  changedOn: string;
  changes: {
    label: string;
    changedFrom: string | number | boolean | null;
    changedTo: string | number | boolean | null;
  }[];
  isActual: boolean;
}

export interface PositionsImport {
  uuid: string;
  createdAt: string;
  updatedAt: string;
  organizationUuid: string;
  createdBy: string;
  fileName: string;
  positionsCreated: number;
  createdByUser: {
    name: string;
    email: string;
  };
}
