export interface GroupsResponse {
  data: {
    data: Types.Group[];
  };
}

export interface Projection {
  title: string;
  format?: string;
  groups: {
    groupUuid: string;
    groupName: string;
    values: {
      monthEnd: Date;
      value: number;
    }[];
    groupTotal: number;
  }[];
  projectionTotal: number;
  expenseModelUuid?: string;
}

export interface ProjectionsResponse {
  data: {
    data: {
      aggregatedSalaries: number;
      aggregatedExpenses: number;
      headcountProjections: Projection[];
      currencyProjections: Projection[];
    };
  };
  status: number;
}
export type HeadcountOrSalary = "headcount" | "salaries";
