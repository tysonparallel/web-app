export interface ICondition {
  field: string;
  condition: string;
  value: string[];
}

export interface IExpenseModelSegment {
  name: string;
  value: number;
  conditions: ICondition[];
  uuid: string;
  createdAt: string;
  updatedAt: string;
}

export interface ICurrentExpense {
  versionUuid: string;
  expenseUuid: string;
  parentUuid: string | null;
  organizationUuid: string;
  scenarioUuid: string | null;
  name: string;
  effectiveAt: string;
  type: "FIXED_PER_EMPLOYEE" | "PERCENT_EMPLOYEE_SALARY" | "MANUAL_ADJUSTMENT";
  changeDescription: string;
  createdAt: string;
  createdBy: string | null;
  updatedAt: string;
  deletedAt: string | null;
  deletedBy: string | null;
  expenseModelSegments: IExpenseModelSegment[];
}

export interface User {
  firstName: string;
  lastName: string;
}

export interface ExpenseAuditEvent {
  versionUuid: string;
  changeDescription: string;
  changedBy: string | User | null;
  effectiveAt: string;
  changedOn: string;
  changes: Change[];
}

export interface Change {
  label: string;
  changedFrom:
    | string
    | number
    | boolean
    | Date
    | string[]
    | null
    | undefined
    | IExpenseModelSegment[];
  changedFromFormat: string;
  changedTo:
    | string
    | number
    | boolean
    | Date
    | string[]
    | null
    | undefined
    | IExpenseModelSegment[];
  changedToFormat: string;
}

export interface IExpense {
  expenseUuid: string;
  current: ICurrentExpense;
  lastModified: string;
  auditLog?: ExpenseAuditEvent[];
  canBeDeleted: boolean;
}
