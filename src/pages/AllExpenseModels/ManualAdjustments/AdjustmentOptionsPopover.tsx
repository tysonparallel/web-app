import React, { Fragment, useState } from "react";
import { Popover, Transition } from "@headlessui/react";
import { usePopper } from "react-popper";
import Typography from "~/components/Typography";
import List, { ListItem } from "~/components/List";
import { EllipsisVerticalIcon } from "@heroicons/react/24/outline";

interface Props {
  id?: string;
  deleteFunction: () => void;
}

const AdjustmentOptionsPopover = ({ id, deleteFunction }: Props) => {
  const [referenceElement, setReferenceElement] = useState(undefined);
  const [popperElement, setPopperElement] = useState(undefined);
  const { styles, attributes } = usePopper(referenceElement, popperElement);

  return (
    <Popover className="relative h-6 w-6">
      <Popover.Button
        ref={setReferenceElement}
        data-testid="deactivatePositionDropdown"
      >
        <EllipsisVerticalIcon
          className="w-6 h-6"
          data-testid={`${id}-adjustment-button`}
        />
      </Popover.Button>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-200"
        enterFrom="opacity-0 translate-y-1"
        enterTo="opacity-100 translate-y-0"
        leave="transition ease-in duration-150"
        leaveFrom="opacity-100 translate-y-0"
        leaveTo="opacity-0 translate-y-1"
      >
        <Popover.Panel
          ref={setPopperElement}
          style={{
            ...styles.popper,
            zIndex: 20,
          }}
          {...attributes.popper}
        >
          <div className="overflow-hidden rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white w-[180px]">
            <div className="p-2">
              <List>
                <ListItem
                  noBorder
                  showChevron={false}
                  onClick={deleteFunction}
                  className="hover:bg-neutral-50 rounded px-3 py-1 z-20"
                  id={`${id}-option-delete`}
                >
                  <Typography color="warning">Delete Adjustment</Typography>
                </ListItem>
              </List>
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  );
};

export default AdjustmentOptionsPopover;
