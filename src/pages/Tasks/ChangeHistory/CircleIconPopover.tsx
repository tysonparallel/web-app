import React, { Fragment, useState, useEffect } from "react";
import { Popover, Transition } from "@headlessui/react";
import { usePopper } from "react-popper";
import {
  CheckCircleIcon,
  MinusCircleIcon,
  XCircleIcon,
} from "@heroicons/react/24/solid";
import Typography from "~/components/Typography";
import useHover from "~/utils/hooks/useHover";

interface Props {
  iconType: "minus" | "check" | "x";
}

const CircleIconPopover = ({ iconType }: Props) => {
  const [referenceElement, setReferenceElement] = useState(undefined);
  const [popperElement, setPopperElement] = useState(undefined);
  const { styles, attributes } = usePopper(referenceElement, popperElement);
  const [popoverState, setPopoverState] = useState<{
    circleIconType?: React.ReactNode;
    forecastType?:
      | "Incorrectly Forecasted"
      | "Correctly Forecasted"
      | "Not Forecasted";
    popoverBody?:
      | "This change was forecasted but the HRIS data didn't match the predicted modification"
      | "This change was forecasted and the HRIS data matched the predicted change"
      | "This change wasn't forecasted but there was a change in the HRIS data";
  }>({});
  const [ref, hovering] = useHover();

  useEffect(() => {
    if (iconType === "minus") {
      setPopoverState({
        circleIconType: <MinusCircleIcon className="h-6 w-6 text-yellow-500" />,
        forecastType: "Incorrectly Forecasted",
        popoverBody:
          "This change was forecasted but the HRIS data didn't match the predicted modification",
      });
    } else if (iconType === "check") {
      setPopoverState({
        circleIconType: <CheckCircleIcon className="h-6 w-6 text-green-500" />,
        forecastType: "Correctly Forecasted",
        popoverBody:
          "This change was forecasted and the HRIS data matched the predicted change",
      });
    } else {
      setPopoverState({
        circleIconType: <XCircleIcon className="h-6 w-6 text-red-500" />,
        forecastType: "Not Forecasted",
        popoverBody:
          "This change wasn't forecasted but there was a change in the HRIS data",
      });
    }
  }, []);

  return (
    <div data-testid="circle-popover" ref={ref}>
      <Popover className="relative">
        <Popover.Button
          ref={setReferenceElement}
          style={{ pointerEvents: "none" }}
        >
          {popoverState.circleIconType}
        </Popover.Button>
        <Transition
          show={Boolean(hovering)}
          as={Fragment}
          enter="transition-opacity ease-out duration-200"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition-opacity ease-in duration-50"
          leaveTo="opacity-0 translate-y-1"
        >
          <Popover.Panel
            ref={setPopperElement}
            style={{
              ...styles.popper,
              zIndex: 20,
              position: "absolute",
              width: "268px",
              textWrap: "wrap",
            }}
            {...attributes.popper}
          >
            <div
              data-testid="import-error-popover"
              className="flex flex-col z-20 bg-white py-4 px-5 rounded-lg border-2 border-green-100 gap-2 mt-2"
            >
              <Typography size="xs" weight="semibold">
                {popoverState.forecastType}
              </Typography>
              <Typography size="xs" className="break-words">
                {popoverState.popoverBody}
              </Typography>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>
    </div>
  );
};

export default CircleIconPopover;
