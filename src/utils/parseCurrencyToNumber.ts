export default (currencyStr: string) => {
  const isNegative = currencyStr.includes("(") && currencyStr.includes(")");

  let numericStr = currencyStr.replace(/[^0-9.-]+/g, "");
  if (isNegative) {
    numericStr = `-${numericStr}`;
  }
  return parseFloat(numericStr);
};
