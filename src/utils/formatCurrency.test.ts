import { describe, it } from "vitest";
import formatCurrency from "./formatCurrency"; // Assuming the function is in a file named formatCurrency.ts

describe("formatCurrency", () => {
  it("should format string value with cents", ({ expect }) => {
    expect(formatCurrency("12345")).toBe("$123.45");
  });

  it("should format string value without cents", ({ expect }) => {
    expect(formatCurrency("12345", false)).toBe("$123");
  });

  it("should format numeric value with cents", ({ expect }) => {
    expect(formatCurrency(12345)).toBe("$123.45");
  });

  it("should format numeric value without cents", ({ expect }) => {
    expect(formatCurrency(12345, false)).toBe("$123");
  });
});
