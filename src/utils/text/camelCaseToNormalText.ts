export const camelCaseToNormalText = (camelCaseString: string): string => {
  const spaceSeparatedString = camelCaseString.replace(
    /([a-z])([A-Z])/g,
    "$1 $2",
  );
  return spaceSeparatedString.replace(/\b\w/g, (char) => char.toUpperCase());
};
