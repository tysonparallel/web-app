import { describe, it } from "vitest";
import {
  emailValidator,
  passwordValidator,
  fullNameValidator,
} from "./validators";

describe("emailValidator", () => {
  it("should validate a valid email address", ({ expect }) => {
    const validEmail = "test@example.com";
    expect(emailValidator.test(validEmail)).toBe(true);
  });

  it("should not validate an invalid email address", ({ expect }) => {
    const invalidEmail = "invalid_email";
    expect(emailValidator.test(invalidEmail)).toBe(false);
  });

  it("should not validate an empty email address", ({ expect }) => {
    const emptyEmail = "";
    expect(emailValidator.test(emptyEmail)).toBe(false);
  });
});

describe("passwordValidator", () => {
  it("should validate a valid password", ({ expect }) => {
    const validPassword = "Password1!";
    expect(passwordValidator.test(validPassword)).toBe(true);
  });

  it("should not validate a password without a lowercase letter", ({
    expect,
  }) => {
    const passwordWithoutLowercase = "PASSWORD1!";
    expect(passwordValidator.test(passwordWithoutLowercase)).toBe(false);
  });

  it("should not validate a password without an uppercase letter", ({
    expect,
  }) => {
    const passwordWithoutUppercase = "password1!";
    expect(passwordValidator.test(passwordWithoutUppercase)).toBe(false);
  });

  it("should not validate a password without a digit", ({ expect }) => {
    const passwordWithoutDigit = "Password!";
    expect(passwordValidator.test(passwordWithoutDigit)).toBe(false);
  });

  it("should not validate a password without a special character", ({
    expect,
  }) => {
    const passwordWithoutSpecialChar = "Password1";
    expect(passwordValidator.test(passwordWithoutSpecialChar)).toBe(false);
  });

  it("should not validate a password with fewer than 8 characters", ({
    expect,
  }) => {
    const shortPassword = "Pass1!";
    expect(passwordValidator.test(shortPassword)).toBe(false);
  });
});

describe("fullNameValidator", () => {
  it("should validate a valid full name", ({ expect }) => {
    const validFullName = "John Doe";
    expect(fullNameValidator.test(validFullName)).toBe(true);
  });

  it("should not validate a full name with invalid characters", ({
    expect,
  }) => {
    const fullNameWithInvalidCharacters = "John 123";
    expect(fullNameValidator.test(fullNameWithInvalidCharacters)).toBe(false);
  });

  it("should not validate a full name with leading or trailing whitespace", ({
    expect,
  }) => {
    const fullNameWithWhitespace = "  John Doe  ";
    expect(fullNameValidator.test(fullNameWithWhitespace)).toBe(false);
  });

  it("should not validate a full name with only one name", ({ expect }) => {
    const singleName = "John";
    expect(fullNameValidator.test(singleName)).toBe(false);
  });

  it("should not validate a full name without a space separator", ({
    expect,
  }) => {
    const fullNameWithoutSpace = "John-Doe";
    expect(fullNameValidator.test(fullNameWithoutSpace)).toBe(false);
  });

  it("should validate a full name with non-standard characters", ({
    expect,
  }) => {
    const fullNameWithNonStandardCharacters = "José María González-Ruíz";
    expect(fullNameValidator.test(fullNameWithNonStandardCharacters)).toBe(
      true,
    );
  });

  it("should validate a full name with accented characters", ({ expect }) => {
    const fullNameWithAccentedCharacters = "André Müller";
    expect(fullNameValidator.test(fullNameWithAccentedCharacters)).toBe(true);
  });

  it("should validate a full name with hyphenated last name", ({ expect }) => {
    const fullNameWithHyphenatedLastName = "Mary Thompson-Smith";
    expect(fullNameValidator.test(fullNameWithHyphenatedLastName)).toBe(true);
  });
});
