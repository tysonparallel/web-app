import formatCurrency from "~/utils/formatCurrency";

export default (data: any, isCurrency = false): string[] => {
  const valuesSum = data.reduce(
    (output: number[], { values }: { values: number[] }) => {
      const result = output;
      values.forEach((val: number, i: number) => {
        if (output[i]) {
          result[i] += val;
        } else {
          result[i] = val;
        }
      });
      return result;
    },
    [],
  );

  return [
    "Total",
    ...valuesSum.map((val: number) => {
      if (isCurrency) {
        return formatCurrency(val, false);
      }
      return val.toString();
    }),
  ];
};
