export default (val: string): string =>
  val.charAt(0).toUpperCase() + val.slice(1);

export const capitalizeWords = (val: string): string => {
  const re = /(\b[a-z](?!\s))/g;
  return val.replace(re, (x) => x.toUpperCase());
};
