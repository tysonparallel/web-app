export enum EmploymentTypes {
  FULL_TIME = "Full Time",
  PART_TIME = "Part Time",
  INTERN = "Intern",
  CONTRACTOR = "Contractor",
}

export default EmploymentTypes;
