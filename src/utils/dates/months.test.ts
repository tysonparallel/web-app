import { describe, it } from "vitest";
import months from "./months";

describe("months", () => {
  it("should contain all 12 months", ({ expect }) => {
    expect(months).toHaveLength(12);
    expect(months).toEqual([
      "january",
      "february",
      "march",
      "april",
      "may",
      "june",
      "july",
      "august",
      "september",
      "october",
      "november",
      "december",
    ]);
  });
});
