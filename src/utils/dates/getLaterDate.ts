import { Dayjs } from "dayjs";

export default (dateA: Dayjs, dateB: Dayjs) =>
  dateA.isAfter(dateB) ? dateA : dateB;
