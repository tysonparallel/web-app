import { describe, it, beforeEach } from "vitest";
import dayjs from "dayjs";
import date from "./date";

describe("date", () => {
  beforeEach(() => {
    process.env = {};
  });

  it("should create a dayjs instance with the provided date", ({ expect }) => {
    const mockDate = "2023-05-23";
    const expectedDate = dayjs(mockDate).utc().startOf("second");
    const result = date(mockDate).startOf("second");

    expect(result).toEqual(expectedDate);
  });

  it("should create a dayjs instance with the current date if no date or VITE_MOCK_DATE is provided", ({
    expect,
  }) => {
    const expectedDate = dayjs().utc().startOf("minute");
    const result = date().startOf("minute");

    expect(result).toEqual(expectedDate);
  });
});
