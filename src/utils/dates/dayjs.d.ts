import dayjs from "dayjs";

declare module "dayjs" {
  interface Dayjs {
    lastOccurrence(unit: "month", value: number): Dayjs;
  }
}

export = dayjs;
