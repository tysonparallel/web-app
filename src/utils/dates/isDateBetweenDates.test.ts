import { describe, it } from "vitest";
import isDateBetweenDatesInclusive from "./isDateBetweenDatesInclusive";

describe("isDateBetweenDates", () => {
  const dateBefore = new Date("2021-01-01T00:00:00.000");
  const dateBeforeTimestamp = new Date("2021-01-01T23:59:59.999");
  const dateAfter = new Date("2021-01-03T00:00:00.000");
  const dateAfterTimestamp = new Date("2021-01-03T23:59:59.999");
  const dateForComparison = new Date("2021-01-02T12:00:00.000");

  it("returns true if between days", ({ expect }) => {
    const result = isDateBetweenDatesInclusive({
      comparisonDate: dateForComparison,
      dateForBefore: dateBefore,
      dateForAfter: dateAfter,
    });
    expect(result).toEqual(true);
  });

  it("returns true when comparison date is on the same day as before date, timestamp after", ({
    expect,
  }) => {
    const result = isDateBetweenDatesInclusive({
      comparisonDate: dateBeforeTimestamp,
      dateForBefore: dateBefore,
      dateForAfter: dateAfter,
    });
    expect(result).toEqual(true);
  });

  it("returns true when comparison date is on the same day as before date, timestamp before", ({
    expect,
  }) => {
    const result = isDateBetweenDatesInclusive({
      comparisonDate: dateBefore,
      dateForBefore: dateBeforeTimestamp,
      dateForAfter: dateAfter,
    });
    expect(result).toEqual(true);
  });

  it("returns true when comparison date is on the same day as after date, timestamp before", ({
    expect,
  }) => {
    const result = isDateBetweenDatesInclusive({
      comparisonDate: dateAfter,
      dateForBefore: dateBefore,
      dateForAfter: dateAfterTimestamp,
    });
    expect(result).toEqual(true);
  });

  it("returns true when comparison date is on the same day as after date, timestamp after", ({
    expect,
  }) => {
    const result = isDateBetweenDatesInclusive({
      comparisonDate: dateAfterTimestamp,
      dateForBefore: dateBefore,
      dateForAfter: dateAfter,
    });
    expect(result).toEqual(true);
  });

  it("returns false when comparison date is on the day before beforeDate", ({
    expect,
  }) => {
    const result = isDateBetweenDatesInclusive({
      comparisonDate: dateBefore,
      dateForBefore: dateForComparison,
      dateForAfter: dateAfter,
    });
    expect(result).toEqual(false);
  });

  it("returns false when comparison date is on the day after afterDate", ({
    expect,
  }) => {
    const result = isDateBetweenDatesInclusive({
      comparisonDate: dateAfter,
      dateForBefore: dateBefore,
      dateForAfter: dateForComparison,
    });
    expect(result).toEqual(false);
  });
});
