export default (role: string, departments?: string[]): boolean =>
  role === "admin" && !!departments && departments[0] === "*";
