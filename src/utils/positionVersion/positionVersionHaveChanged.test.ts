import { describe, it, expect } from "vitest";
import positionVersionsHaveChanged from "./positionVersionsHaveChanged";

describe("comparePositionVersions", () => {
  it("should return true when objects are different", () => {
    const a = {
      prop1: "value1",
      prop2: "value2",
      changeDescription: "ignoreThis",
    };
    const b = {
      prop1: "value1",
      prop2: "differentValue", // Different value
      changeDescription: "ignoreThis",
    };
    expect(positionVersionsHaveChanged(a, b)).toBe(true);
  });

  it("should return false when objects are the same", () => {
    const a = {
      prop1: "value1",
      prop2: "value2",
      changeDescription: "ignoreThis",
    };
    const b = {
      prop1: "value1",
      prop2: "value2",
      changeDescription: "ignoreThis",
    };
    expect(positionVersionsHaveChanged(a, b)).toBe(false);
  });

  it("should return true when nested objects are different", () => {
    const a = {
      prop1: "value1",
      prop2: {
        nested1: "nestedValue1",
        nested2: "nestedValue2",
        changeDescription: "ignoreThis",
      },
    };
    const b = {
      prop1: "value1",
      prop2: {
        nested1: "nestedValue1",
        nested2: "differentNestedValue", // Different value
        changeDescription: "ignoreThis",
      },
    };
    expect(positionVersionsHaveChanged(a, b)).toBe(true);
  });

  it("should return false when objects are empty", () => {
    const a = {};
    const b = {};
    expect(positionVersionsHaveChanged(a, b)).toBe(false);
  });

  it("should ignore specified keys", () => {
    const a = {
      prop1: "value1",
      prop2: "value2",
      changeDescription: "ignoreThis",
    };
    const b = {
      prop1: "value1",
      prop2: "value2",
      changeDescription: "ignoreThis 2",
    };
    expect(positionVersionsHaveChanged(a, b)).toBe(false); // Ignored key
  });

  it("should compare dates", () => {
    const a = {
      effectiveAt: "2023-12-04T00:00:00.000Z",
    };
    const b = {
      effectiveAt: "2023-12-04",
    };
    expect(positionVersionsHaveChanged(a, b)).toBe(false);
  });

  it("should compare compensation rates as numbers", () => {
    const a = {
      compensationRate: "1000",
    };
    const b = {
      compensationRate: "2000",
    };
    expect(positionVersionsHaveChanged(a, b)).toBe(true);
  });
});
