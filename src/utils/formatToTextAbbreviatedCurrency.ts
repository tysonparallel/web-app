export default ({
  value,
  abbreviation,
  decimal,
  currency,
}: {
  value: number | string;
  abbreviation?: "K" | "M" | "B" | "T";
  decimal?: number;
  currency?: string;
}): string => {
  if (!currency) {
    currency = "$";
  }

  let numValue: number;
  if (typeof value === "string") {
    numValue = parseFloat(value);
  } else {
    numValue = value;
  }

  if (abbreviation === undefined) {
    if (numValue >= 100000000000000) {
      abbreviation = "T";
    } else if (numValue >= 100000000000) {
      abbreviation = "B";
    } else if (numValue >= 100000000) {
      abbreviation = "M";
    } else {
      abbreviation = "K";
    }
  }

  let result: string;

  if (abbreviation === "T") {
    result =
      currency + (numValue / 100000000000000).toFixed(decimal ?? 0) + "T";
  } else if (abbreviation === "B") {
    result = currency + (numValue / 100000000000).toFixed(decimal ?? 0) + "B";
  } else if (abbreviation === "M") {
    result = currency + (numValue / 100000000).toFixed(decimal ?? 0) + "M";
  } else {
    result = currency + (numValue / 100000).toFixed(decimal ?? 0) + "K";
  }

  return result;
};
