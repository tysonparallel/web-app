import { describe, it } from "vitest";
import parseCurrencyToNumber from "./parseCurrencyToNumber";

describe("parseCurrencyToNumber", () => {
  it("should parse string value with cents", ({ expect }) => {
    expect(parseCurrencyToNumber("$123.45")).toBe(123.45);
  });

  it("should parse string value without cents", ({ expect }) => {
    expect(parseCurrencyToNumber("$123")).toBe(123);
  });
});
