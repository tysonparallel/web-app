import axios, { Method } from "axios";
import { beforeEach, describe, expect, it, vi } from "vitest";
import request from "./request";

vi.mock("axios");

describe("request", () => {
  beforeEach(() => {
    vi.stubEnv("VITE_PARALLEL_API_URL", "http://localhost:9000");
    localStorage.removeItem("accessToken");
    axios.get.mockReset();
  });

  it("should make a request with empty access token", async () => {
    const mockResponse = {
      status: 403,
      data: { error: "Unauthorized" },
    };
    axios.request.mockResolvedValueOnce(mockResponse);
    const requestData = {
      url: "/users",
      method: "GET" as Method,
    };

    const response = await request(requestData);

    expect(response).toEqual(mockResponse);
    expect(axios.request).toHaveBeenCalledWith(
      expect.objectContaining({
        url: "/users",
        method: "GET",
        baseURL: expect.any(String),
        headers: {
          "Content-Type": "application/json",
          Authorization: undefined,
        },
      }),
    );
  });
});
