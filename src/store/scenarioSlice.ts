import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export interface ScenarioState {
  inEditMode: boolean;
  activeScenarioUuid?: string;
}

const initialState = {
  inEditMode: false,
} as ScenarioState;

export const scenarioSlice = createSlice({
  name: "scenario",
  initialState,
  reducers: {
    update: (state, action: PayloadAction<ScenarioState>) => action.payload,
    reset: () => initialState,
  },
});

const { update, reset } = scenarioSlice.actions;
export { update, reset };
export default scenarioSlice.reducer;
