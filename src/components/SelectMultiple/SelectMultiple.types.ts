import { ReactNode } from "react";

export interface SelectMultipleType {
  value?: string | null;
  label: string | null | ReactNode;
  node?: ReactNode | null;
  disabled?: boolean;
}

export interface SelectMultipleState {
  options: SelectMultipleType[];
  selected?: SelectMultipleType[];
  valid: boolean;
  touched: boolean;
  pristine: boolean;
  disabled?: boolean;
  multiple?: boolean;
  isLoading?: boolean;
}
