import React from "react";
import Typography from "../Typography";
import { IDataArrayDictionary } from ".";
import { ChevronRightIcon } from "@heroicons/react/24/outline";

interface IProps {
  xFormatter: (value: number | null) => string;
  yFormatter: (value: number | null) => string;
  active?: boolean;
  payload?: { payload: IDataArrayDictionary }[];
  label?: number;
  dataKeys: string[];
}

const LineGraphTooltip = ({
  xFormatter,
  yFormatter,
  active,
  payload,
  label,
  dataKeys,
}: IProps): React.ReactElement | null => {
  if (active && payload?.length) {
    return (
      <div className="bg-white ml-2 rounded-xl shadow-md py-1 px-4">
        <Typography color="secondary" size="xs">
          {xFormatter(label ?? null)}
        </Typography>
        <div className="flex items-center justify-between">
          <Typography size="sm" weight="bold">
            {yFormatter(payload[0].payload[dataKeys[1]] ?? null)}
          </Typography>
          {dataKeys.length >= 3 && (
            <>
              <ChevronRightIcon
                height={"16px"}
                width={"16px"}
                className="text-neutral-200 mx-1"
              />
              <Typography size="sm" weight="bold" color="graphBlue">
                {yFormatter(payload[0].payload[dataKeys[2]] ?? null)}
              </Typography>
            </>
          )}
        </div>
      </div>
    );
  }
  return null;
};

export default LineGraphTooltip;
