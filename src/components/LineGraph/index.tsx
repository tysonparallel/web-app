import React from "react";
import { LineChart, XAxis, YAxis, Line, Tooltip } from "recharts";
import { CategoricalChartState } from "recharts/types/chart/types";
import Typography from "../Typography";
import Card from "../Card";
import LineGraphTooltip from "./LineGraphTooltip";

export type IDataArrayDictionary = Record<string, number>;

interface ILineProps {
  dataKey: string;
  stroke: string;
  dot: boolean;
  isDashed?: boolean;
}

interface IProps {
  xFormatter: (value: number | null) => string;
  yFormatter: (value: number | null) => string;
  setActiveIndex: (value: number) => void;
  data: IDataArrayDictionary[];
  dataKeys: string[]; // Array of all keys that may appear in a data object, should be ordered XAxis, first line, second line, etc.
  width: number;
  height: number;
  lines: ILineProps[];
  tooltipDefaultIndex?: number; // leave undefined for standard tooltip behavior, setting it allows the tooltip to be displayed and synced across multiple graphs
  card?: {
    title: string;
    month?: string;
    figure?: string;
  };
}

const LineGraph = ({
  xFormatter,
  yFormatter,
  setActiveIndex,
  data,
  dataKeys,
  width,
  height,
  lines,
  card,
  tooltipDefaultIndex,
}: IProps): React.ReactElement => {
  const xKey = dataKeys[0];
  const minX = Math.min(...data.map((item) => item[xKey]));
  const maxX = Math.max(...data.map((item) => item[xKey]));

  const handleMouseChange = (value: CategoricalChartState): void => {
    if (value.activeLabel) {
      const activeLabelInt = parseInt(value.activeLabel);
      const activeLabelIndex = data
        .map((item) => item[xKey])
        .indexOf(activeLabelInt);
      setActiveIndex(activeLabelIndex);
    } else {
      setActiveIndex(-1);
    }
  };

  const graph = (
    <div>
      <LineChart
        height={height}
        width={width}
        margin={{ top: 8, right: 8 }}
        onMouseMove={(value) => handleMouseChange(value)}
        onMouseLeave={() => setActiveIndex(-1)}
      >
        <YAxis
          axisLine={false}
          domain={[0, "auto"]}
          tickFormatter={(value) => yFormatter(value)}
          tickLine={false}
          tickCount={4}
          tickMargin={0}
          tick={{
            fontWeight: 400,
            fill: "#999999",
            fontFamily: "inter",
            fontSize: 16,
          }}
        />
        <XAxis
          axisLine={false}
          domain={[minX, maxX]}
          dataKey={"month"}
          type={"number"}
          tick={false}
        />
        <Tooltip
          content={
            <LineGraphTooltip
              xFormatter={xFormatter}
              yFormatter={yFormatter}
              dataKeys={dataKeys}
            />
          }
          allowEscapeViewBox={{ x: true, y: true }}
          cursor={{ stroke: "#BCBCBC", strokeWidth: 2, strokeDasharray: 8 }}
          defaultIndex={tooltipDefaultIndex !== -1 ? tooltipDefaultIndex : -1}
          active={
            tooltipDefaultIndex === undefined
              ? undefined
              : tooltipDefaultIndex !== -1
                ? true
                : false
          }
        />
        {lines.map((line) => (
          <Line
            key={line.dataKey}
            type="linear"
            data={data}
            dataKey={line.dataKey}
            stroke={line.stroke}
            strokeWidth={1.5}
            dot={line.dot}
            strokeDasharray={line.isDashed ? "10 10" : ""}
            activeDot={
              tooltipDefaultIndex !== -1 && { r: 7, style: { stroke: "none" } }
            }
          />
        ))}
      </LineChart>
      <div className="flex justify-between items-center pl-14 w-full mt-[-20px]">
        <Typography size="sm" color="empty">
          {xFormatter(minX)}
        </Typography>
        <Typography size="sm" color="empty">
          {xFormatter(maxX)}
        </Typography>
      </div>
    </div>
  );

  if (card) {
    return (
      <Card>
        <div className="flex items-center justify-between mb-3 w-full">
          <Typography size="sm">{card.title}</Typography>
          {card.month && card.figure && (
            <div className="flex items-center">
              <Typography
                size="xs"
                color="disabled"
                weight="thin"
                className="mr-2"
              >
                {card.month}
              </Typography>
              <Typography size="sm" weight="bold">
                {card.figure}
              </Typography>
            </div>
          )}
        </div>
        {graph}
      </Card>
    );
  }

  return graph;
};

export default LineGraph;
