import React from "react";

interface Props {
  text: string;
  className?: string;
  iconFill?: "default" | "green" | "red" | "orange";
}

const Tag = ({ text, className, iconFill = "default" }: Props) => {
  const fillStyles = {
    default: "bg-blue-500",
    green: "bg-green-500",
    red: "bg-red-400",
    orange: "bg-orange",
  };
  return (
    <div className="rounded-full border-2 border-neutral-50 py-2 px-4 w-auto inline-block">
      <div className="flex items-center gap-2 justify-start w-full">
        <div
          className={`h-3 w-3 rounded-full ${fillStyles[iconFill]} ${className}`}
        />
        {text}
      </div>
    </div>
  );
};

export default Tag;
