import FieldMapping from "~/components/FieldMapping/FieldMapping";
import useFieldMapping from "~/components/FieldMapping/useFieldMapping";

export { useFieldMapping };
export default FieldMapping;
