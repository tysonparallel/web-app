import { ReactNode } from "react";

export interface SelectType {
  value?: string | null;
  label: string | null;
  node?: ReactNode;
  disabled?: boolean;
}

export interface SelectState {
  options: SelectType[];
  selected?: SelectType;
  valid: boolean;
  touched: boolean;
  pristine: boolean;
  disabled?: boolean;
  multiple?: boolean;
  isLoading?: boolean;
}
