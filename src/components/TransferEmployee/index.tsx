import React from "react";
import Modal from "~/components/Modal";
import Typography from "~/components/Typography";
import { useEffect, useState } from "react";
import date from "~/utils/dates/date";
import formatCurrency from "~/utils/formatCurrency";
import { useSelector } from "react-redux";
import { State } from "~/store";
import request from "~/utils/request";
import Button from "~/components/Button";
import Skeleton from "~/components/Skeleton";
import toast from "react-hot-toast";
import { Position } from "pages/Headcount/headcount.types";
import useNonHrisFieldValidationState from "~/components/Position/ParallelFieldValidation/useNonHrisFieldValidationState";
import { useSelect } from "~/components/Select";
import ParallelFieldValidation from "~/components/Position/ParallelFieldValidation";
import MiniPositionCard from "../../pages/Headcount/AssignEmployee/PositionCard";

interface Props {
  isOpen: boolean;
  setModal: () => void;
  successCallback?: () => void;
  isLoading: boolean;
  setIsLoading: (isLoading: boolean) => void;
  taskUuid?: string;
  employeeData: {
    employeeUuid?: string | null;
    title?: string | null;
    effectiveAt?: string | null;
    manager?: string | null;
    department?: string | null;
    compensationRate?: number | null;
    currency?: string | null;
    paymentUnit?: string | null;
    startDate?: string | null;
    employmentType?: string | null;
    employeeName?: string | null;
  } | null;
}

interface PositionResponse {
  request: unknown;
  data: {
    data: Position[];
  } | null;
  status: number;
}

const TransferEmployeeContainer = ({
  isOpen,
  setModal,
  employeeData,
  successCallback,
  isLoading,
  setIsLoading,
  taskUuid,
}: Props): React.ReactNode => {
  const organizationUuid = useSelector(
    (state: State) => state.organization.uuid,
  );
  const [positions, setPositions] = useState<Position[]>([]);
  const [selectedPosition, setSelectedPosition] = useState<Position | null>(
    null,
  );
  const [isLoadingPositions, setIsLoadingPositions] = useState(true);
  const [showParallelFieldValidation, setShowParallelFieldValidation] =
    useState(false);
  const [currencyTypeState, setCurrencyTypeState] = useSelect({
    selected: {
      value: employeeData?.currency ?? "USD",
      label: employeeData?.currency ?? "USD",
    },
  });

  const {
    expectedHoursState,
    setExpectedHoursState,
    bonusAmountState,
    setBonusAmountState,
    commissionAmountState,
    setCommissionAmountState,
    attainmentState,
    setAttainmentState,
  } = useNonHrisFieldValidationState();

  const getUnfilledPositionsData = async (): Promise<void> => {
    try {
      const positionsResponse = (await request({
        url: `/organizations/${organizationUuid}/positions`,
        method: "GET",
        params: {
          isActive: true,
          isFilled: false,
        },
      })) as PositionResponse;

      if (positionsResponse.data?.data)
        setPositions(positionsResponse.data.data);
    } catch {
      throw Error("Error fetching positions");
    } finally {
      setIsLoadingPositions(false);
    }
  };

  const attemptTransfer = async (): Promise<void> => {
    setIsLoading(true);
    const transferResponse = await request({
      url: `/organizations/${organizationUuid}/tasks/${taskUuid}/process-potential-transfer`,
      method: "POST",
      body: {
        parallelTrackedFields: {
          bonus: bonusAmountState.value
            ? Number(bonusAmountState.value) * 100
            : null,
          commission: commissionAmountState.value
            ? Number(commissionAmountState.value) * 100
            : null,
          attainment: attainmentState.value
            ? Number(attainmentState.value) / 100
            : null,
          expectedWeeklyHours: expectedHoursState.value
            ? expectedHoursState.value
            : undefined,
        },
        positionVersionUuid: selectedPosition?.current.uuid,
      },
    });
    setIsLoading(false);
    if (transferResponse.status === 201) {
      setSelectedPosition(null);
      toast.success("Transferred employee");
      if (successCallback) {
        successCallback();
      }
    } else {
      toast.error("Error transferring employee");
    }
  };

  const validateNonHrisFields = (): void => {
    if (selectedPosition !== null) {
      if (employeeData?.employmentType === "HOURLY") {
        setExpectedHoursState({
          ...expectedHoursState,
          value:
            selectedPosition.current.expectedWeeklyHours?.toString() ?? "20",
          valid: true,
        });
      }

      setBonusAmountState({
        ...bonusAmountState,
        value:
          selectedPosition.current.bonus !== null
            ? (selectedPosition.current.bonus / 100).toString()
            : "",
        valid: true,
      });

      setCommissionAmountState({
        ...commissionAmountState,
        value:
          selectedPosition.current.commission !== null
            ? (selectedPosition.current.commission / 100).toString()
            : "",
        valid: true,
      });

      setAttainmentState({
        ...attainmentState,
        value:
          selectedPosition.current.attainment !== null
            ? (selectedPosition.current.attainment * 100).toString()
            : "",
        valid: true,
      });
    }

    setShowParallelFieldValidation(true);
  };

  useEffect(() => {
    if (isOpen) {
      getUnfilledPositionsData();
    }
  }, [isOpen]);

  const formatEmploymentType = (value: string): string =>
    value
      .replace("_", " ")
      .toLowerCase()
      .split(" ")
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(" ");

  let paymentUnit = "-";
  if (employeeData?.paymentUnit) {
    paymentUnit = employeeData.paymentUnit === "HOURLY" ? "Hourly" : "Salary";
  }

  if (!employeeData) return null;

  const modalDisplayState = ((): React.ReactNode => {
    if (isLoadingPositions) {
      // Render the loading state
      return Array.from({ length: 5 }).map((_, index) => (
        <Skeleton
          key={
            /* eslint-disable-next-line react/no-array-index-key */
            `y-axis-skeleton-${index}`
          }
          height={52}
          width={1070}
          baseColor="white"
        />
      ));
    }
    if (positions.length) {
      // Render the positions if available
      return positions.map((position) => (
        <MiniPositionCard
          isSelected={selectedPosition?.current.uuid === position.current.uuid}
          onSelect={setSelectedPosition}
          position={position}
          id={`transfer-employee-position-${position.current.positionUuid}`}
          key={position.current.uuid}
        />
      ));
    }

    // Render the message if there are no positions
    return (
      <Typography color="empty" className="m-auto">
        No Forecasted Positions
      </Typography>
    );
  })();

  const handleModalState = ((): {
    title: string;
    size: "lg" | "xxl";
  } => ({
    title: showParallelFieldValidation
      ? "Validate New Position Data"
      : "Transfer Employee",
    size: showParallelFieldValidation ? "lg" : "xxl",
  }))();

  return (
    <Modal
      title={handleModalState.title}
      isOpen={isOpen}
      size={handleModalState.size}
      onClose={() => {
        setSelectedPosition(null);
        setModal();
      }}
    >
      {!showParallelFieldValidation ? (
        <div
          data-testid="transfer-employee-modal"
          className="col-span-7 flex flex-col items-start w-full"
        >
          <div className="pl-6 pr-10 w-full mt-6 mb-4">
            <div className="flex flex-row gap-3 items-baseline">
              <Typography tag="h3" size="md" weight="medium">
                {employeeData.employeeName}
              </Typography>
              <Typography
                tag="p"
                size="xs"
                weight="medium"
                color="special"
                className="text-orange"
              >
                HRIS DATA
              </Typography>
            </div>
            <div
              data-testid="task-employee-metadata"
              className="w-full flex flex-row justify-between mt-2"
            >
              <div>
                <Typography color="secondary">Title</Typography>
                <p
                  className="text-sm w-64 truncate"
                  title={employeeData.title ?? "-"}
                >
                  {employeeData.title ?? "-"}
                </p>
              </div>
              <div>
                <Typography color="secondary">Department</Typography>
                <p
                  className="text-sm w-32 truncate"
                  title={employeeData.department ?? "-"}
                >
                  {employeeData.department ?? "-"}
                </p>
              </div>
              <div>
                <Typography color="secondary">Pay Type</Typography>
                <p
                  className="text-sm w-32 truncate"
                  title={employeeData.employmentType ?? "-"}
                >
                  {employeeData.employmentType
                    ? formatEmploymentType(employeeData.employmentType)
                    : "-"}
                </p>
              </div>
              <div>
                <Typography color="secondary">Payment Unit</Typography>
                <p className="text-sm w-32 truncate" title={paymentUnit}>
                  {paymentUnit}
                </p>
              </div>
              <div>
                <Typography color="secondary">Compensation</Typography>
                <p
                  className="text-sm w-32 truncate"
                  title={
                    employeeData.compensationRate
                      ? formatCurrency(
                          employeeData.compensationRate,
                          employeeData.paymentUnit === "HOURLY" ? true : false,
                          employeeData.currency ?? undefined,
                        )
                      : "-"
                  }
                >
                  {employeeData.compensationRate
                    ? formatCurrency(
                        employeeData.compensationRate,
                        employeeData.paymentUnit === "HOURLY" ? true : false,
                        employeeData.currency ?? undefined,
                      )
                    : "-"}
                </p>
              </div>
              <div>
                <Typography color="secondary">Start Date</Typography>
                <p className="text-sm w-32 truncate">
                  {employeeData.startDate
                    ? date(employeeData.startDate).format("MM/DD/YYYY")
                    : "-"}
                </p>
              </div>
            </div>
          </div>
          <div className="w-full h-[320px] bg-neutral-15 flex flex-col gap-3 my-4 p-2 rounded-xl overflow-auto border border-neutral-50">
            {modalDisplayState}
          </div>
          <div className="w-full flex justify-between">
            <Button
              fill="clear"
              className="!w-auto !px-0"
              id="transfer-employee-button-cancel"
              onClick={() => {
                setSelectedPosition(null);
                setModal();
              }}
            >
              Cancel
            </Button>
            <div className="flex gap-4">
              <Button
                id="generate-position-button"
                fill="outline"
                className="!w-auto"
                disabled={selectedPosition !== null || isLoading}
                onClick={validateNonHrisFields}
              >
                Generate Position
              </Button>
              <Button
                className="!w-auto"
                id="transfer-employee-button"
                disabled={selectedPosition === null || isLoading}
                onClick={validateNonHrisFields}
              >
                Transfer
              </Button>
            </div>
          </div>
        </div>
      ) : (
        <ParallelFieldValidation
          onSubmit={attemptTransfer}
          onBack={() => {
            setShowParallelFieldValidation(false);
          }}
          currencyTypeState={currencyTypeState}
          setCurrencyTypeState={setCurrencyTypeState}
          expectedHoursState={expectedHoursState}
          setExpectedHoursState={setExpectedHoursState}
          bonusAmountState={bonusAmountState}
          setBonusAmountState={setBonusAmountState}
          commissionAmountState={commissionAmountState}
          setCommissionAmountState={setCommissionAmountState}
          attainmentState={attainmentState}
          setAttainmentState={setAttainmentState}
          paymentUnit={paymentUnit}
          isLoading={isLoading}
        />
      )}
    </Modal>
  );
};

export default TransferEmployeeContainer;
