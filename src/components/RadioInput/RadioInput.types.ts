export interface RadioInputType {
  value?: string;
  label: string;
  description?: string;
  disabled?: boolean;
}

export interface RadioInputState {
  options: RadioInputType[];
  selected?: RadioInputType;
  valid: boolean;
  touched: boolean;
  pristine: boolean;
  disabled?: boolean;
  errorMessage?: string;
}
