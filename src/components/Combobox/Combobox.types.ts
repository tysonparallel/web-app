import React from "react";

interface BaseProps {
  className?: string;
  id: string;
  state: State;
  setState: React.Dispatch<React.SetStateAction<State>>;
  errorMessage?: string;
  label: string;
  placeholder: string;
  emptyStateMessage?: string;
  leftIcon?: React.ReactNode;
  inputClassName?: string;
  disabled?: boolean;
  required?: boolean;
}

type OptionalLabel = Omit<BaseProps, "label"> & {
  label?: string;
};

type OptionalPlaceholder = Omit<BaseProps, "placeholder"> & {
  placeholder?: string;
};

export type Props = BaseProps | OptionalLabel | OptionalPlaceholder;

export interface Option {
  label: string | null;
  value: string | null;
  component?: React.ReactNode;
}

export interface State {
  query: string;
  options: Option[];
  selected?: Option;
  valid: boolean;
  touched: boolean;
  pristine: boolean;
  disabled?: boolean;
}
