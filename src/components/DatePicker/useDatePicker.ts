import React, { useState } from "react";

export interface DatePickerState {
  value: {
    startDate: string | null;
    endDate: string | null;
  };
  valid: boolean;
  touched: boolean;
  pristine: boolean;
  minDate?: string | null;
  errorMessage?: string;
  disabled?: boolean;
}

const useDatePicker = (
  initialValues?: Partial<DatePickerState>,
): [
  value: DatePickerState,
  setState: React.Dispatch<React.SetStateAction<DatePickerState>>,
] => {
  const { value, minDate, valid } = initialValues || {};
  const initialState: DatePickerState = {
    value: value || {
      startDate: null,
      endDate: null,
    },
    valid: valid || false,
    touched: false,
    pristine: true,
    minDate: minDate || null,
    errorMessage: "Start date is required",
  };

  const [input, setDatePicker] = useState(initialState);

  return [input, setDatePicker];
};

export default useDatePicker;
