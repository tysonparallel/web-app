import React from "react";

interface Props {
  children: React.ReactNode | string;
}

const Chip = ({ children }: Props) => (
  <div className="p-1 px-4 text-base rounded-full w-auto mt-1 mb-1 text-white bg-green">
    {children}
  </div>
);

export default Chip;
