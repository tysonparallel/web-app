import * as dotenv from "dotenv";
dotenv.config();

import { defineConfig, devices } from "@playwright/test";

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export default defineConfig({
  /* Look for test files in the "tests" directory, relative to this configuration file. */
  testDir: "tests",
  /* Override fully parallel because our tests commonly fail (someone fix them) when fully parallel stresses the system */
  workers: 3,
  /* Fail the build on CI if you accidentally left tests.only in the source code. */
  forbidOnly: !!process.env.CI,
  retries: 2,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: "html",
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
  use: {
    acceptDownloads: true,
    /* Base URL to use in actions like `await page.goto('/')`. */
    baseURL: process.env.BASE_URL ?? "http://localhost:8000",

    /* Collect trace when retrying the failed tests. See https://playwright.dev/docs/trace-viewer */
    trace: "on-first-retry",

    /* Configure the timeout to 10 seconds. Anything longer and what the hell are we doing calling ourselves engineers? */
    actionTimeout: 10000,

    screenshot: "only-on-failure",
  },

  /* Configure projects for major browsers */
  projects: [
    {
      name: "chromium",
      use: devices["Desktop Chrome"],
    },
    {
      name: "firefox",
      use: devices["Desktop Firefox"],
    },

    {
      name: "webkit",
      use: devices["Desktop Safari"],
    },

    /* Test against mobile viewports. */
    // {
    //   name: 'Mobile Chrome',
    //   use: { ...devices['Pixel 5'] },
    // },
    // {
    //   name: 'Mobile Safari',
    //   use: { ...devices['iPhone 12'] },
    // },

    /* Test against branded browsers. */
    // {
    //   name: 'Microsoft Edge',
    //   use: { ...devices['Desktop Edge'], channel: 'msedge' },
    // },
    // {
    //   name: 'Google Chrome',
    //   use: { ...devices['Desktop Chrome'], channel: 'chrome' },
    // },
  ],

  /* Run your local dev server before starting the e2e */
  // webServer: {
  //   command: "npm run start",
  //   url: "http://127.0.0.1:5173",
  //   reuseExistingServer: !process.env.CI,
  // },
});
